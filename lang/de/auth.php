<?php

return [

    'name' => 'Name',
    'email' => 'E-Mail-Adresse',
    'password' => 'Passwort',
    'new-password' => 'Neues Passwort',
    'new-password-confirmed' => 'Neues Passwort wiederholen',
    'remember' => 'Angemeldet bleiben',
    'accept' => 'Ich akzeptiere die Nutzungsbedingungen.',
    'code' => 'Code',

    'placeholder' => [
        'name' => 'Max Mustermann',
        'email' => 'max.mustermann@example.com',
        'code' => '123987',
    ],

    'signin' => [
        'title' => 'Anmelden',
        'description' => 'Melde dich mit deinen Zugangsdaten an.',
        'submit' => 'Anmelden',
        'validation' => [
            'email' => 'Bitte gib deine E-Mail-Adresse ein.',
            'password' => 'Bitte gib dein Passwort ein.',
            'failed' => 'Die E-Mail-Adresse oder das Passwort ist nicht korrekt.'
        ]
    ],

    'challenge' => [
        'title' => 'Zwei-Faktor-Eingabe',
        'description' => 'Öffne deine Authentifizierungs-App und gib den Code ein.',
        'submit' => 'Code verifizieren',
        'submit-await' => 'Verifizieren…',
        'validation' => 'Der eingegebene Code ist ungültig.',
        'alert' => 'Der eingegebene Code ist nicht gültig. Versuche es erneut oder gib einen deiner Wiederherstellungscodes ein.'
    ],

    'signup' => [
        'title' => 'Registrieren',
        'description' => 'Erstelle ein neuen Benutzeraccount.',
        'submit' => 'Registrieren',
        'validation' => [
            'name' => [
                'min' => 'Der Name muss mindestens 2 Zeichen enthalten.',
                'max' => 'Der Name darf nicht mehr als 100 Zeichen enthalten.',
                'required' => 'Bitte gib einen Namen ein.'
            ],
            'email' => [
                'email' => 'Bitte gib eine gültige E-Mail-Adresse ein.',
                'unique' => 'Diese E-Mail-Adresse wird bereits verwendet.',
                'required' => 'Bitte gib eine E-Mail-Adresse ein.'
            ],
            'password' => [
                'required' => 'Bitte gib ein Passwort ein.'
            ]
        ]
    ],

    'forgot-password' => [
        'hint' => 'Passwort vergessen?',
        'title' => 'Passwort vergessen',
        'description' => 'Gib deine E-Mail-Adresse ein, um dein Passwort zurückzusetzen.',
        'submit' => 'Passwort zurücksetzen',
        'validation' => [
            'email' => [
                'email' => 'Bitte gib eine gültige E-Mail-Adresse ein.',
                'exists' => 'Diese E-Mail-Adresse ist bei uns nicht registriert.',
                'required' => 'Bitte gib eine E-Mail-Adresse ein.'
            ]
        ],
        'info' => 'Wir haben dir eine E-Mail an <strong>:email</strong> gesendet, mit der du dir ein neues Passwort geben kannst.'
    ],

    'reset-password' => [
        'title' => 'Passwort zurücksetzen',
        'description' => 'Gib dein neues Passwort ein.',
        'submit' => 'Neues Passwort setzen',
        'validation' => [
            'password' => [
                'required' => 'Bitte gib ein Passwort ein.'
            ]
        ],
        'info' => 'Dein Passwort wurde geändert. Du kannst dich nun mit deinem neuen Passwort anmelden.'
    ],

    'confirm-password' => [
        'title' => 'Passwort bestätigen.',
        'description' => 'Bitte gib dein Passwort zur Bestätigung ein, damit du fortfahren kannst.',
        'submit' => 'Passwort bestätigen',
        'validation' => 'Das eingegebene Passwort ist nicht korrekt.'
    ],

    'signout' => 'Abmelden',

    'validation' => [
        'password' => [
            'string' => 'Bitte gib ein gültiges Passwort ein.',
            'min' => 'Das Passwort muss mindestens :length Zeichen lang sein.',
            'max' => 'Das Passwort darf maximal :length Zeichen lang sein.',
            'mixed' => 'Das Passwort muss aus Groß- und Kleinbuchstaben bestehen.',
            'digits' => 'Das Passwort muss mindestens eine Zahl enthalten.',
            'special' => 'Das Passwort muss mindestens ein Sonderzeichen enthalten.',
        ]
    ]
];
