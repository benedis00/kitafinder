<?php

return [

    'privateCenter' => 'private Kita ohne Abschluss der Rahmenvereinbarung',
    'workersWelfare' => 'Arbeiterwohlfahrt',
    'caritas' => 'Deutscher Caritasverband',
    'ekt' => 'EKT',
    'deaconry' => 'Diakonisches Werk',
    'ownOperation' => 'Eigenbetrieb',
    'companyCenter' => 'Betriebskita',
    'other' => 'Sonstiger freier Träger',

];
