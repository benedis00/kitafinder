<?php

return [

    'forest' => 'Waldkindergarten',
    'parentChild' => 'Eltern-Kind-Gruppe',
    'parentInitiative' => 'Eltern-Initiativ-Kindertagesstätte',
    'dayCare' => 'Kindertagesstätte',

];
