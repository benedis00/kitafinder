<?php

return [

    'dropzone' => [
        'description' => 'Hier Dateien hochladen',
        'upload' => 'Datei hochladen',
        'drop' => 'Dateien hier ablegen',
    ]
];
