<x-layout.gate>
    <x-panel custom class="-mx-8 xs:mx-auto px-5 xs:px-12 py-8 xs:py-12 max-w-lg">
        <form method="post" action="{{ route('signup.store') }}" class="space-y-6" x-data="{}" x-on:submit="$refs.spinner.classList.remove('hidden'); $refs.submit.setAttribute('disabled', 'disabled')">

            {{-- Intro --}}
            <div>
                <h1 class="text-3xl mb-2">{{ trans('auth.signup.title') }}</h1>
                <p>{{ trans('auth.signup.description') }}</p>
                @csrf
            </div>

            @if (session()->has('message'))
                <div class="text-primary bg-primary-50 rounded px-3 py-2">
                    <i class="fas fa-info-circle inline mr-2"></i>
                    <span>{{ session()->pull('message') }}</span>
                </div>
            @endif

            {{-- Name --}}
            <div>
                <x-form.label required for="name">{{ trans('auth.name') }}</x-form.label>
                <x-form.input required
                    id="name"
                    name="name"
                    placeholder="{{ trans('auth.placeholder.name') }}"
                    maxlength=100
                    :invalid="$errors->has('name')" />
                <x-form.error field="name" />
            </div>

            {{-- Email --}}
            <div>
                <x-form.label required for="email">{{ trans('auth.email') }}</x-form.label>
                <x-form.input required
                    id="email"
                    name="email"
                    type="email"
                    maxlength=100
                    :invalid="$errors->has('email')" />
                <x-form.error field="email" />
            </div>

            {{-- Password --}}
            <div>
                <x-form.label required for="password">
                    {{ trans('auth.password') }}
                </x-form.label>
                <x-form.input required
                    id="password"
                    name="password"
                    type="password"
                    maxlength=255
                    :invalid="$errors->has('password')" />
                <x-form.error field="password" />
            </div>

            {{-- Accept --}}
            <x-form.checkbox required id="accept" name="accept" :invalid="$errors->has('accept')">
                {{ trans('auth.accept') }}
            </x-form.checkbox>

            {{-- Submit --}}
            <x-button class="w-full flex items-center justify-center" x-ref="submit">
                <x-animation.submit-spinner class="hidden" x-ref="spinner" />
                {{ trans('auth.signup.submit') }}
            </x-button>
        </form>
    </x-panel>
</x-layout.gate>
