<x-layout.gate>
    <x-panel custom class="-mx-8 xs:mx-auto px-5 xs:px-12 py-8 xs:py-12 max-w-lg">
        <form method="post" action="{{ route('password.reset.store') }}" class="space-y-6" x-data="{}" x-on:submit="$refs.spinner.classList.remove('hidden'); $refs.submit.setAttribute('disabled', 'disabled')">

            {{-- Intro --}}
            <div>
                <h1 class="text-3xl mb-2">{{ trans('auth.reset-password.title') }}</h1>
                <p>{{ trans('auth.reset-password.description') }}</p>
                @csrf
            </div>

            {{-- Password --}}
            <div>
                <input type="hidden" name="token" value="{{ request('token') }}" />
                <x-form.label required for="password">{{ trans('auth.password') }}</x-form.label>
                <x-form.input required
                    id="password"
                    type="password"
                    name="password"
                    autocomplete="new-password"
                    :invalid="$errors->has('password')" />
                <x-form.error field="password" />
            </div>

            {{-- Submit --}}
            <x-button class="w-full flex items-center justify-center" x-ref="submit">
                <x-animation.submit-spinner class="hidden" x-ref="spinner" />
                {{ trans('auth.reset-password.submit') }}
            </x-button>
        </form>
    </x-panel>
</x-layout.gate>
