<x-layout.gate>
    <x-panel custom class="-mx-8 xs:mx-auto px-5 xs:px-12 py-8 xs:py-12 max-w-lg">
        <form method="post" action="{{ route('password.forgot.store') }}" class="space-y-6" x-data="{}" x-on:submit="$refs.spinner.classList.remove('hidden'); $refs.submit.setAttribute('disabled', 'disabled')">

            {{-- Intro --}}
            <div>
                <h1 class="text-3xl mb-2">{{ trans('auth.forgot-password.title') }}</h1>
                <p>{{ trans('auth.forgot-password.description') }}</p>
                @csrf
            </div>

            {{-- Email --}}
            <div>
                <x-form.label required for="email">{{ trans('auth.email') }}</x-form.label>
                <x-form.input required
                    id="email"
                    type="email"
                    name="email"
                    placeholder="{{ trans('auth.placeholder.email') }}"
                    :invalid="$errors->has('email')" />
                <x-form.error field="email" />
            </div>

            {{-- Submit --}}
            <x-button class="w-full flex items-center justify-center" x-ref="submit">
                <x-animation.submit-spinner class="hidden" x-ref="spinner" />
                {{ trans('auth.forgot-password.submit') }}
            </x-button>
        </form>
    </x-panel>
</x-layout.gate>
