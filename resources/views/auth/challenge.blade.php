<x-layout.gate>
    <x-panel custom class="-mx-8 xs:mx-auto px-5 xs:px-12 py-8 xs:py-12 max-w-lg">
        <form method="post" action="{{ route('two-factor-authentication.challenge') }}" class="space-y-6" x-data="{ submit: (button, spinner, buttonText) => { button.disabled = true; buttonText.innerText = button.dataset.textVerifying; spinner.classList.remove('hidden'); $el.submit(); } }">

            {{-- Intro --}}
            <div>
                <h1 class="text-3xl mb-2">{{ trans('auth.challenge.title') }}</h1>
                <p>{{ trans('auth.challenge.description') }}</p>
                @csrf
            </div>

            @if ($message = session()->pull('message-danger'))
                <div class="mt-8 py-2 px-3 bg-red-100 text-red-500 rounded-md">
                    {!! $message !!}
                </div>
            @endif

            {{-- Password --}}
            <div>
                <x-form.label required for="code">{{ trans('auth.code') }}</x-form.label>
                <x-form.input required
                    id="code"
                    type="text"
                    name="code"
                    placeholder="{{ trans('auth.placeholder.code') }}"
                    autofocus
                    autocomplete="one-time-code"
                    x-on:input="!event.isTrusted && submit($refs.button, $refs.spinner, $refs.buttonText);"
                    class="font-mono"
                    :invalid="$errors->has('code')" />
                <x-form.error field="code" />
            </div>

            {{-- Submit --}}

            <x-button class="w-full flex items-center justify-center" x-ref="button" data-text-verifying="{{ trans('auth.challenge.submit-await') }}">
                <x-animation.submit-spinner x-ref="spinner" class="hidden" />
                <span x-ref="buttonText">{{ trans('auth.challenge.submit') }}</span>
            </x-button>
        </form>
    </x-panel>
</x-layout.gate>
