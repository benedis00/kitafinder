<x-layout.main>

    <div class="max-w-5xl mx-auto mt-16 mb-32 space-y-8">
        <x-title>Welcome to the playground!</x-title>

        <div class="flex space-x-4">
            <x-button>Primary</x-button>
            <x-button secondary>Secondary</x-button>
            <x-button danger>Danger</x-button>
            <x-button class="flex items-center space-x-2">
                <x-animation.submit-spinner />
                Primary
            </x-button>
            <x-button secondary class="flex items-center space-x-2">
                <x-animation.submit-spinner secondary />
                Secondary
            </x-button>
            <x-button danger class="flex items-center space-x-2">
                <x-animation.submit-spinner danger />
                Danger
            </x-button>
            <div class="flex items-center justify-center text-gray-300 relative">
                <i class="fas fa-bell text-3xl relative">
                    <x-animation.ping />
                </i>
            </div>
        </div>

        <x-section title="Section" description="Lorem ipsum dolor sit amet" class="space-y-2">
            <x-slot:info>
                <x-logo class="w-16" />
            </x-slot:info>
            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ipsum consectetur culpa soluta a doloribus consequatur, debitis voluptate eaque tenetur placeat quae totam ut suscipit ea porro adipisci! Blanditiis, ipsa veritatis?</p>
            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ipsum consectetur culpa soluta a doloribus consequatur, debitis voluptate eaque tenetur placeat quae totam ut suscipit ea porro adipisci! Blanditiis, ipsa veritatis?</p>
            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ipsum consectetur culpa soluta a doloribus consequatur, debitis voluptate eaque tenetur placeat quae totam ut suscipit ea porro adipisci! Blanditiis, ipsa veritatis?</p>
        </x-section>

        <div class="grid grid-cols-3 gap-8 p-8 bg-gray-50">
            <x-panel class="h-32">Panel A</x-panel>
            <x-panel>Panel B</x-panel>
            <x-panel>Panel C</x-panel>
        </div>

        <div>
            <x-form.label>Label</x-form.label>
            <x-form.input placeholder="Input" />
        </div>

        <div>
            <x-form.checkbox>Checkbox A</x-form.checkbox>
            <x-form.checkbox>Checkbox B</x-form.checkbox>
            <x-form.checkbox>Checkbox C</x-form.checkbox>
        </div>

        <div class="space-y-1">
            <x-form.radio.button id="rbg_1" name="radio_button_group">Radio Button A</x-form.radio.button>
            <x-form.radio.button id="rbg_2" name="radio_button_group">Radio Button B</x-form.radio.button>
            <x-form.radio.button id="rbg_3" name="radio_button_group">Radio Button C</x-form.radio.button>
        </div>

        <div class="grid grid-cols-3 gap-4">
            <x-form.radio.panel id="rbg_1_panel" name="radio_button_group_panel">Radio Panel A</x-form.radio.panel>
            <x-form.radio.panel id="rbg_2_panel" name="radio_button_group_panel">Radio Panel B</x-form.radio.panel>
            <x-form.radio.panel id="rbg_3_panel" name="radio_button_group_panel">Radio Panel C</x-form.radio.panel>
        </div>

        <x-form.dropzone />

        <x-form.select>
            <option>Option A</option>
            <option>Option B</option>
            <option>Option C</option>
        </x-form.select>

        <x-form.textarea rows="3">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Quod molestias cupiditate ratione soluta eligendi, suscipit, autem, placeat iste expedita odit ducimus impedit rem odio ad temporibus sint quo officiis vero?
        </x-form.textarea>

        <x-form.toggle id="toggle" />
    </div>

</x-layout.main>
