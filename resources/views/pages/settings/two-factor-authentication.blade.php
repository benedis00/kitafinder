<x-section title="Two Factor Authentication" description="Sichere dein Benutzerkonto weiter ab, in dem du die Zwei-Faktor-Authentifizierung aktiviert.">
    <x-slot:info>
        @if (auth()->user()->twoFactorEnabled())
            <div>{!! auth()->user()->twoFactorQrCodeSvg() !!}</div>
        @endif
    </x-slot:info>

    @if (! auth()->user()->twoFactorEnabled())
        <p class="mb-6">
            Wenn du die Zwei-Faktor-Authentifizierung nutzt, musst du bei der Anmeldung neben deinem Passwort noch ein Authentifizierungscode eingeben. Diesen musst du sicher verfahren und kannst ihn in einem Passwortmanager deiner Wahl speichern.
        </p>
    @endif
    <p class="mb-6">
        Empfohlene Apps für die Zwei-Faktor-Authentifizierung sind unter anderem Google Authenticator, Authy oder Passwort-Manager wie 1Password.
    </p>

    @if (auth()->user()->twoFactorEnabled())
        <h5 class="mt-5">QR-Code</h5>
        <p>Kopiere oder scanne den QR-Code und füge ihn deiner Authentifizierungs-App hinzu. Diese wird dir Zugangscodes generieren, mit denen du dich anmelden kannst.</p>

        <h5 class="mt-5">Wiederherstellungscodes</h5>
        <p>Wenn du keinen Zugang zu deiner Authentifizierungs-App hast, kannst du dich auch mit den unten stehenden Wiederherstellungscodes authentifizieren. <strong>Bewahre die Codes deshalb sicher auf und verliere sie nicht.</strong> Wenn du einen Code benutzt, wird er im nachhinein automatisch durch einen neuen ersetzt.</p>

        <pre class="mt-5 px-3 py-2 text-sm bg-gray-100 rounded-md inline-block leading-6 tracking-widest">{{ collect(auth()->user()->recoveryCodes())->join(PHP_EOL) }}</pre>

        <div class="mt-5 flex flex-col xs:flex-row justify-end">
            <x-button secondary class="flex items-center justify-center" type="submit" wire:loading.attr="disabled" wire:click="regenerate">
                <x-animation.submit-spinner secondary wire:loading wire:target="regenerate" />
                Neue Codes generieren
            </x-button>
            <x-button danger class="flex items-center justify-center mt-4 xs:mt-0 xs:ml-4" type="submit" wire:loading.attr="disabled" wire:click="deactivate">
                <x-animation.submit-spinner danger wire:loading wire:target="deactivate" />
                Deaktivieren
            </x-button>
        </div>
    @else
        <x-button class="flex items-center justify-center w-full xs:w-auto xs:ml-auto" type="submit" wire:loading.attr="disabled" wire:click="activate">
            <x-animation.submit-spinner wire:loading wire:target="activate" />
            Aktivieren
        </x-button>
    @endif
</x-section>
