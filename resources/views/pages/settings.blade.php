<div>

    {{-- Title --}}
    <x-title>{{ trans('settings.title') }}</x-title>

    {{-- Two Factor Authentication --}}
    <livewire:pages.settings.two-factor-authentication />
</div>
