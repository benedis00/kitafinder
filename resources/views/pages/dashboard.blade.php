@push('styles')
    <style>
        .dot {
            height: 10px;
            width: 10px;
            border-radius: 50%;
            display: inline-block;
        }

        .blue {
            background-color: rgba(42,129,203,0.6);
        }
        .blue div {
            background-color: rgba(42,129,203,1);
        }

        .green {
            background-color: rgba(42,173,39,0.6);
        }
        .green div {
            background-color: rgba(42,173,39,1);
        }

        .gold {
            background-color: rgba(255,211,38,0.6);
        }
        .gold div {
            background-color: rgba(255,211,38,1);
        }

        .red {
            background-color: rgba(203,43,62,0.6);
        }
        .red div {
            background-color: rgba(203,43,62,1);
        }

        .grey {
            background-color: rgba(123,123,123,0.3);
        }
        .grey div {
            background-color: rgba(123,123,123,0.7);
        }

        .marker-cluster {
            background-clip: padding-box;
            border-radius: 20px;
        }
        .marker-cluster div {
            width: 30px;
            height: 30px;
            margin-left: 5px;
            margin-top: 5px;

            text-align: center;
            border-radius: 15px;
            font: 12px "Helvetica Neue", Arial, Helvetica, sans-serif;
        }
        .marker-cluster span {
            line-height: 30px;
        }

        .legend {
            line-height: 18px;
            background-color: rgba(255,255,255,0.8);
            padding: 6px 8px;
            box-shadow: 0 0 15px rgba(0,0,0,0.2);
            border-radius: 5px;
            z-index: 40;
        }
        .legend i {
            width: 14px;
            height: 14px;
            float: left;
            margin-top: 2px;
            margin-bottom: 2px;
            margin-right: 8px;
            opacity: 0.7;
        }
        .info {
            padding: 6px 8px;
            font: 14px/16px Arial, Helvetica, sans-serif;
            background: white;
            background: rgba(255,255,255,0.8);
        }

        @media (max-width: 639px) {
            .legend {
                margin-bottom: 20px !important;
            }
        }


        @media (max-width: 400px) {
            .legend {
                margin-bottom: 40px !important;
            }
        }

        .leaflet-right {
            z-index: auto;
        }

    </style>
@endpush
<div>
    <x-sidebar.index>
        <x-slot:contentLeft>

            @livewire('sidebar-content', ['left' => true])

        </x-slot>

        <x-slot:contentBottom>

            @livewire('sidebar-content', ['left' => false])

        </x-slot>

        <div id="map" class="h-full z-10" wire:ignore></div>
    </x-sidebar.index>
</div>

@push('scripts')
    <script src="https://api.mapbox.com/mapbox.js/v3.3.1/mapbox.js"></script>
    <script src="https://unpkg.com/leaflet-control-geocoder/dist/Control.Geocoder.js"></script>
    <script src="https://unpkg.com/leaflet.markercluster@1.4.1/dist/leaflet.markercluster.js"></script>
    <script> document.addEventListener("DOMContentLoaded", (event) => {
        Modal("intro-modal");
    }) </script>
    <script> const api = <?php echo json_encode(config('app.mapbox_token')); ?> </script>
    <script> var kitas = <?php echo json_encode($this->getJsonKitas()); ?>; </script>
    <script> const kitaTypes = <?php echo json_encode($kitaTypes); ?>; </script>
    <script>
        var resized = false;
        window.addEventListener('resize', function () {
            if (resized) {
                setTimeout(() => {
                    resized = false;
                }, 500);

            } else {
                setTimeout(() => {
                    resized = true;
                    window.dispatchEvent(new Event('resize'));
                }, 500);

            }
        });
    </script>
    <script>
        window.addEventListener('updated', event => {
            kitas = event.detail.kitas;
            recalculateKitas();
        })
    </script>
    <script>
        window.addEventListener('filterTypes', event => {
            removeOverlay(event.detail.types);
        })
    </script>
    <script>
        window.addEventListener('greyKitas', event => {
            removeGreyKitas(event.detail.show);
        })
    </script>
    <script>
        window.addEventListener('listClick', event => {
            switch (event.detail.color) {
                case 'blue':
                    openBluePopup(event.detail.lng, event.detail.lat);
                    break;
                case 'red':
                    openRedPopup(event.detail.lng, event.detail.lat);
                    break;
                case 'green':
                    openGreenPopup(event.detail.lng, event.detail.lat);
                    break;
                default:
                    openGoldPopup(event.detail.lng, event.detail.lat);
                    break;
            }
        })
    </script>
    <script>
        window.addEventListener('circleClick', event => {
            calculateCircle(event.detail.lng, event.detail.lat, event.detail.radius, event.detail.markerPopup);
            window.dispatchEvent(new Event('closeSidebar'));
        })
    </script>
    <script>
        window.addEventListener('circleDelete', event => {
            deleteCircle();
        })
    </script>
    <script>
        window.addEventListener('deleteFilter', event => {
            deleteFilter();
        })
    </script>
    <script type="text/javascript" src="{{ asset('map/map.js') }}"></script>
@endpush
