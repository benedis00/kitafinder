@php
    $caretLeft = '
        <svg class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20">
            <path fill-rule="evenodd" d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z" clip-rule="evenodd" />
        </svg>';

    $caretRight = '
        <svg class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20">
            <path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd" />
        </svg>';

    $elementClasses = 'relative inline-flex items-center px-4 py-2 text-sm font-medium leading-5 rounded';
@endphp

<div>
    @if ($paginator->hasPages())
        @php(isset($this->numberOfPaginatorsRendered[$paginator->getPageName()]) ? $this->numberOfPaginatorsRendered[$paginator->getPageName()]++ : $this->numberOfPaginatorsRendered[$paginator->getPageName()] = 1)

        <nav role="navigation" aria-label="Pagination Navigation" class="flex items-center justify-between">
            <div class="flex items-center justify-between flex-1 sm:hidden">
                <span>
                    @if ($paginator->onFirstPage())
                        <span class="{{ $elementClasses }} text-primary-300 bg-white cursor-default">
                            <div class="-ml-2">
                                {!! $caretLeft !!}
                            </div>
                            <span class="hidden xs:inline">{{ trans('data-table.pagination.previous') }}</span>
                            <span class="xs:hidden">{{ trans('data-table.pagination.previous-short') }}</span>
                        </span>
                    @else
                        <button
                            wire:click="previousPage('{{ $paginator->getPageName() }}')"
                            wire:loading.attr="disabled"
                            dusk="previousPage{{ $paginator->getPageName() == 'page' ? '' : '.' . $paginator->getPageName() }}.before"
                            class="{{ $elementClasses }} text-primary bg-primary-50 hover:text-white hover:bg-primary focus:z-10 focus:outline-none active:bg-primary active:text-white transition ease-in-out duration-150">
                            <div class="-ml-2">
                                {!! $caretLeft !!}
                            </div>
                            <span class="hidden xs:inline">{{ trans('data-table.pagination.previous') }}</span>
                            <span class="xs:hidden">{{ trans('data-table.pagination.previous-short') }}</span>
                        </button>
                    @endif
                </span>

                <span class="text-sm text-gray-700 leading-5">
                    {{ $paginator->currentPage() }}/{{ $paginator->lastPage() }}
                </span>

                <span>
                    @if ($paginator->hasMorePages())
                        <button
                            wire:click="nextPage('{{ $paginator->getPageName() }}')"
                            wire:loading.attr="disabled"
                            dusk="nextPage{{ $paginator->getPageName() == 'page' ? '' : '.' . $paginator->getPageName() }}.before"
                            class="{{ $elementClasses }} text-primary bg-primary-50 hover:text-white hover:bg-primary focus:z-10 focus:outline-none active:bg-primary active:text-white transition ease-in-out duration-150">
                            <span class="hidden xs:inline">{{ trans('data-table.pagination.next') }}</span>
                            <span class="xs:hidden">{{ trans('data-table.pagination.next-short') }}</span>
                            <div class="-mr-2">
                                {!! $caretRight !!}
                            </div>
                        </button>
                    @else
                        <span class="{{ $elementClasses }} text-primary-300 bg-white cursor-default">
                            <span class="hidden xs:inline">{{ trans('data-table.pagination.next') }}</span>
                            <span class="xs:hidden">{{ trans('data-table.pagination.next-short') }}</span>
                            <div class="-mr-2">
                                {!! $caretRight !!}
                            </div>
                        </span>
                    @endif
                </span>
            </div>

            <div class="hidden sm:flex-1 sm:flex sm:items-center sm:justify-between">

                {{-- Item Summary --}}
                <div>
                    <p class="text-sm text-gray-700 leading-5">
                        {!! trans('data-table.pagination.summary', [
                            'first' => $paginator->firstItem(),
                            'last' => $paginator->lastItem(),
                            'total' => $paginator->total()
                        ]) !!}
                    </p>
                </div>

                {{-- Page Selection --}}
                <div>
                    <span class="relative z-0 inline-flex space-x-2">

                        {{-- Previous Page Link --}}
                        <span>
                            @if ($paginator->onFirstPage())
                                <span class="{{ $elementClasses }} !px-2 text-primary-300 cursor-default">
                                    {!! $caretLeft !!}
                                </span>
                            @else
                                <button wire:click="previousPage('{{ $paginator->getPageName() }}')" dusk="previousPage{{ $paginator->getPageName() == 'page' ? '' : '.' . $paginator->getPageName() }}.after" rel="prev" class="{{ $elementClasses }} !px-2 text-primary bg-white hover:text-white hover:bg-primary focus:z-10 focus:outline-none active:bg-primary active:text-white transition ease-in-out duration-150">
                                    {!! $caretLeft !!}
                                </button>
                            @endif
                        </span>

                        {{-- Pagination Elements --}}
                        @foreach ($elements as $element)

                            {{-- "Three Dots" Separator --}}
                            @if (is_string($element))
                                <span class="{{ $elementClasses }} text-primary bg-primary-50 cursor-default">
                                    {{ $element }}
                                </span>
                            @endif

                            {{-- Array Of Links --}}
                            @if (is_array($element))
                                @foreach ($element as $page => $url)
                                    <span wire:key="paginator-{{ $paginator->getPageName() }}-{{ $this->numberOfPaginatorsRendered[$paginator->getPageName()] }}-page{{ $page }}">
                                        @if ($page == $paginator->currentPage())
                                            <span class="{{ $elementClasses }} text-white bg-primary cursor-default">
                                                {{ $page }}
                                            </span>
                                        @else
                                            <button wire:click="gotoPage({{ $page }}, '{{ $paginator->getPageName() }}')" class="{{ $elementClasses }} text-primary bg-primary-50 hover:text-white hover:bg-primary focus:z-10 focus:outline-none active:bg-primary active:text-white transition ease-in-out duration-150">
                                                {{ $page }}
                                            </button>
                                        @endif
                                    </span>
                                @endforeach
                            @endif
                        @endforeach

                        {{-- Next Page Link --}}
                        <span>
                            @if ($paginator->hasMorePages())
                                <button wire:click="nextPage('{{ $paginator->getPageName() }}')" dusk="nextPage{{ $paginator->getPageName() == 'page' ? '' : '.' . $paginator->getPageName() }}.after" rel="next" class="{{ $elementClasses }} !px-2 text-primary bg-white hover:text-white hover:bg-primary focus:z-10 focus:outline-none active:bg-primary active:text-white transition ease-in-out duration-150">
                                    {!! $caretRight !!}
                                </button>
                            @else
                                <span class="{{ $elementClasses }} !px-2 text-primary-300 cursor-default">
                                    {!! $caretRight !!}
                                </span>
                            @endif
                        </span>
                    </span>
                </div>
            </div>
        </nav>
    @endif
</div>
