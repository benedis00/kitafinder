<x-modal title="Intro" maxWidth="3xl">

    {{-- Content --}}
    <p class="text-lg font-semibold">Was ist der Kitafinder?</p>
    <p class="pb-2">Der Kitafinder ist ein Projekt das im Rahmen einer Bachelorarbeit in Zusammenarbeit mit der Freien Universität und der Webagentur Pangoon Development entstanden ist, um Nutzern die Suche nach Informationen mit Hilfe von öffentlichen Daten und einer Kartendarstellung zu erleichtern.</p>
    <p class="text-lg font-semibold"> Systemeinführung </p>
    <p> Kitas werden einzeln als Marker oder zusammengefasst in einem Cluster dargestellt. Die Zahl im Cluster steht für die Anzahl der zusammengefassten Kitas. </p>
    <p> Kitas sind in 4 Einrichtungsarten unterteilt, welche die Farbe des Markers oder Clusters ausmacht: <a href="https://www.socialnet.de/lexikon/Kindertageseinrichtung" class="!text-[#0078A8]">Kindertagesstätte</a>, <a href="https://www.rausimwalde.de/definition-waldkindergarten/" class="!text-[#0078A8]">Waldkindergarten</a>, <a href="https://elternbildung-portal.at/artikel/eltern-kind-gruppe-was-ist-das" class="!text-[#0078A8]">Eltern-Kind-Gruppe</a> und <a href="https://bage.de/was-sind-elterninitiativen/" class="!text-[#0078A8]">Eltern-Initiativ-Kindertagesstätte</a>. </p>
    <p> Für die speziellere Suche gibt es auf der linken Seite (oder auf der Mobilansicht unten) eine Sidebar, unter welcher die Möglichkeiten des Filterns nach Informationen, der Suche mit Hilfe eines Umkreises und der Darstellung der angezeigten Kitas als Liste angeboten werden. </p>

    <x-slot name="actions">
        <x-form.close>
            Schließen
        </x-form.close>
    </x-slot>

</x-modal>
