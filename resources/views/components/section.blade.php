@props([
    'title',
    'description' => null,
    'info' => null
])

<section class="flex flex-col sm:flex-row space-y-4 sm:space-y-0 sm:space-x-8 py-8 first:pt-0 last:pb-0">

    {{-- Title --}}
    <div class="sm:w-60 md:w-72 lg:w-96 shrink-0">
        <h2 class="text-lg sm:text-xl">{{ $title }}</h2>
        @isset ($description)
            <p class="text-gray-500 text-sm mt-1">{{ $description }}</p>
        @endisset
        @isset ($info)
            <div {{ $info->attributes->class('mt-6') }}>
                {{ $info }}
            </div>
        @endisset
    </div>

    {{-- Content --}}
    <div {{ $attributes->class('xs:flex-grow') }}>
        {{ $slot }}
    </div>
</section>
