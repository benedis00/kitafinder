@props([
    'custom' => false
])

@php
    if (! $custom) {
        $attributes = $attributes->class('p-3');
    }
@endphp

<div {{ $attributes->class('bg-white shadow rounded') }}>
    {{ $slot }}
</div>
