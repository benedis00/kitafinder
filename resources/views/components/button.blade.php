@props([
    'href' => null,
    'danger' => false,
    'secondary' => false,
    'large' => false,
    'small' => false
])

@php
    $attributes = $attributes->class(join(' ', [
        'whitespace-nowrap',
        'rounded font-medium block appearance-none',
        'transition-colors duration-200',
        'border-2 focus:outline-none focus:ring-2 focus:ring-offset-2',
        'disabled:opacity-70 disabled:transition-none disabled:cursor-not-allowed'
    ]));

    if ($danger) {
        $attributes = $attributes->class('text-white bg-red-600 border-red-600 hover:bg-red-700 hover:border-red-700 focus:ring-red-600 hover:focus:ring-red-600');
    } else if ($secondary) {
        $attributes = $attributes->class('text-gray-500 bg-gray-200 border-gray-200 hover:text-gray-900 hover:disabled:text-gray-500 focus:ring-gray-200 hover:focus:ring-gray-200');
    } else {
        $attributes = $attributes->class('text-white bg-primary border-primary hover:bg-primary-800 hover:border-primary-800 focus:ring-primary hover:focus:ring-primary-800');
    }

    if ($large) {
        $attributes = $attributes->class('py-3 px-5 text-base');
    } else if ($small) {
        $attributes = $attributes->class('px-3 py-1 xl:px-4 xl:py-1.5 text-sm');
    } else {
        $attributes = $attributes->class('px-4 py-2 text-base');
    }
@endphp

@isset($href)
    <a {{ $attributes->merge([ 'href' => $href ]) }}>{{ $slot }}</a>
@else
    <button {{ $attributes }}>{{ $slot }}</button>
@endisset
