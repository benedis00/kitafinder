@props([
    'invalid' => false
])

@php
    $attributes = $attributes->class(join(' ', [
        'h-5 w-5 appearance-none p-0 inline-block',
        'border-solid border-2 focus:outline-none',
        'align-middle select-none flex-shrink-0 focus:ring-2',
        'transition duration-200 bg-origin-border',

        'bg-white text-primary border-gray-200',
        'focus:ring-offset-2 focus:ring-primary focus:ring-offset-white',

        'rounded bg-cover bg-center bg-no-repeat',
        'checked:bg-primary checked:border-primary'
    ]));
@endphp

<div class="flex">
    <input {{ $attributes->class([ 'peer' => $invalid ])([ 'type' => 'checkbox']) }} />
    @isset($slot)
        <x-form.label for="{{ $attributes->get('id') }}" label-class="!mb-0 !text-sm leading-5" class="{{ $slot->isEmpty() ? '' : 'ml-3' }}">
            {{ $slot }}
        </x-form.label>
    @endisset
</div>
