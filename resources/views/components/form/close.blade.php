<x-button secondary type="cancel" wire:click="$emit('closeModal')">
    @if ($slot->isNotEmpty())
        {{ $slot }}
    @else
        {{ trans('product.general.close') }}
    @endif
</x-button>
