@props([
'error' => false
])

<small {{ $attributes->class('!mt-1 text-xs inline-block'.($error ? ' !text-red-500' : ' !text-gray-400')) }}>{{ $slot }}</small>
