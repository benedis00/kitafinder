@props([
    'required' => false,
    'tooltip' => null,
    'actions' => null
])

@php
    $attributes = $attributes->class('inline-block text-sm text-gray-500 font-semibold');
    if ($required) {
        $attributes = $attributes->class('after:content-[attr(required-mark)] after:text-red-500');
    }
@endphp
@isset($tooltip)
    @isset($actions)
        <div class="flex items-center justify-between mb-2">
            <div class="flex items-center">
                <label {{ $attributes }} @if ($required) required-mark="*" @endif>{{ $slot }}</label>
                <i class="fas fa-question-circle text-xs ml-1 text-gray-500 cursor-pointer" data-tooltip="{{ $tooltip }}" data-tooltip-placement="right"></i>
            </div>
            <span class="leading-5 text-sm">{{ $actions }}</span>
        </div>
    @else
        <div class="flex items-center mb-2">
            <label {{ $attributes }} @if ($required) required-mark="*" @endif>{{ $slot }}</label>
            <i class="fas fa-question-circle text-xs ml-1 text-gray-500 cursor-pointer" data-tooltip="{{ $tooltip }}" data-tooltip-placement="right"></i>
        </div>
    @endif
@else
    @isset($actions)
        <div class="flex items-center justify-between mb-2">
            <label {{ $attributes }} @if ($required) required-mark="*" @endif>{{ $slot }}</label>
            <span class="leading-5 text-sm">{{ $actions }}</span>
        </div>
    @else
        <div class="flex items-center mb-2">
            <label {{ $attributes }} @if ($required) required-mark="*" @endif>{{ $slot }}</label>
        </div>
    @endisset
@endisset
