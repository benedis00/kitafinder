@props([
    'label',
    'description',
    'leading' => false
])

@php
    $containerClasses = join(' ', [
        'relative w-12 h-7 rounded-full bg-gray-200 overflow-hidden transition duration-300',
        'focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-primary',
        'focus-within:ring-offset-white',
        'flex-shrink-0',
        isset($label) || isset($description) ? ($leading ? 'mr-4' : 'ml-4') : ''
    ]);

    $attributes = $attributes->class(join(' ', [
        'absolute block w-7 h-7 rounded-full bg-white border-2',
        'appearance-none outline-none cursor-pointer transition duration-300',
        'checked:transform checked:translate-x-5 checked:border-primary',
        'peer'
    ]));
@endphp

@if (isset($label) || isset($description))
    <div class="flex items-start {{ $leading ? 'flex-row-reverse justify-end' : 'justify-between' }}">
        <div class="flex flex-col text-sm">
            @isset ($label)
                <strong class="leading-7">{{ $label }}</strong>
            @endisset
            @isset ($description)
                <span class="text-gray-400">{{ $description }}</span>
            @endisset
        </div>
@endif
<div class="{{ $containerClasses }}">
    <input {{ $attributes->merge([ 'type' => 'checkbox' ]) }} />
    <label
        for="{{ $attributes->get('id') }}"
        class="block overflow-hidden h-full transition duration-300 bg-gray-200 cursor-pointer peer-checked:bg-primary"
    ></label>
</div>
@if (isset($label) || isset($description))
    </div>
@endif
