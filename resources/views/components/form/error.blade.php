@props([
    'field' => false,
    'compositeField' => false
])
@if ($compositeField && collect($errors->keys())->intersect($compositeField)->count())
    @php
        $message = null;
        do {
            $message = $errors->first(array_shift($compositeField));
        } while (! isset($message) || strlen($message) === 0);
    @endphp
    <x-form.info error>{{ $message }}</x-form.info>
@elseif ($field)
    @error ($field)
        <x-form.info error>{{ $message }}</x-form.info>
    @else
        @if ($slot->isNotEmpty())
            <x-form.info>{{ $slot }}</x-form.info>
        @endif
    @enderror
@endif
