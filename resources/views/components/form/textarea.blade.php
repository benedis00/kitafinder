@props([
    'invalid' => false
])

@php
    if ($invalid) {
        $attributes = $attributes->class('!border-red-500');
    }

    $attributes = $attributes->class(join(' ', [
        'px-3 py-2',
        'appearance-none focus:outline-none',
        'border-solid border-2',
        'rounded block w-full',
        'transition duration-200',
        'disabled:opacity-50',
        'bg-white',
        'border-gray-200',
        'focus:border-primary',
        'placeholder-gray-400'
    ]));
@endphp

<textarea {{ $attributes}}>{{ $slot }}</textarea>
