@props([
    'invalid' => false
])

@php
    $attributes = $attributes->class(join(' ', [
        'h-5 w-5 appearance-none p-0 inline-block',
        'border-solid border-2 focus:outline-none box-border',
        'align-middle select-none flex-shrink-0 focus:ring-2',
        'transition duration-300 bg-origin-border',

        'bg-gray-200',
        'text-primary-500 border-transparent',
        'focus:ring-offset-2 focus:ring-primary-500 focus:ring-offset-white',

        'rounded-full bg-cover bg-center bg-no-repeat',
        'checked:bg-primary-500'
    ]));
@endphp

<div class="flex">
    <input {{ $attributes->class([ 'peer' => $invalid ])->merge([ 'type' => 'radio']) }} />
    @isset($slot)
        <x-form.label for="{{ $attributes->get('id') }}" class="mb-0 ml-3 {{ $invalid ? '!text-red-500' : '' }}">
            {{ $slot }}
        </x-form.label>
    @endisset
</div>
