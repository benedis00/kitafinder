@props([
    'invalid' => false
])

@php
    $attributes = $attributes->class('shadow block rounded-md transition border-2 hover:border-primary-500 hover:text-primary-500 peer-checked:border-primary-500 peer-checked:bg-primary-100 peer-checked:text-primary-500 cursor-pointer select-none p-4');
    if ($invalid) {
        $attributes = $attributes->class('border-red-500 hover:border-red-500 text-red-500 hover:text-red-500 bg-red-100');
    } else {
        $attributes = $attributes->class('border-gray-100 text-gray-500');
    }
@endphp

<div class="font-bold">
    <input {{ $attributes->merge([ 'type' => 'radio' ])->except('class') }} class="hidden peer">
    <label for="{{ $attributes->get('id') }}" {{ $attributes->only([ 'class' ]) }}>
        {{ $slot }}
    </label>
</div>
