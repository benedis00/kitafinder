@props([
    'invalid' => false,
    'leadingAddOn' => false,
    'trailingAddOn' => false,
    'rightAligned' => false
])

@php
    if ($invalid) {
        $attributes = $attributes->class('!border-red-600');
    }

    $attributes = $attributes->class('flex overflow-hidden placeholder-gray-400 bg-white border-solid border-2 border-gray-200 focus-within:border-primary rounded block w-full transition duration-200');

    if ($attributes->has('disabled')) {
        $attributes = $attributes->class('opacity-50');
    }
@endphp

<div {{ $attributes->only('class') }}>
    @if ($leadingAddOn)
        <div @if (!$leadingAddOn->attributes->has('inline')) class="bg-gray-100 text-gray-500 px-3 py-2 border-r-2 border-gray-200 flex-shrink-0" @else class="-m-0.5 flex-shrink-0" @endif>
            {{ $leadingAddOn }}
        </div>
    @endif
    <input {{ $attributes->except('class')->class('focus:outline-none px-3 py-2 w-full'.($rightAligned ? ' text-right' : '')) }} />
    @if ($trailingAddOn)
        <div @if (!$trailingAddOn->attributes->has('inline')) class="pr-3 py-2 flex-shrink-0" @else class="-m-0.5 flex-shrink-0" @endif>
            {{ $trailingAddOn }}
        </div>
    @endif
</div>
