@props([
    'description' => null
])

@php
    $attributes = $attributes->class('overflow-hidden border-2 relative rounded-md group text-gray-500');
@endphp

<div {{ $attributes->only('class') }}>
    <input {{ $attributes->except('class') }} type="file" class="absolute inset-0 opacity-0" role="button" />
    <label for="{{ $attributes->get('id') }}" class="px-8 py-8 flex flex-col items-center text-center space-y-4">
        <i class="fas fa-copy text-4xl pointer-events-none"></i>
        <span>{{ $description ?? ($slot->isNotEmpty() ? $slot : trans('app.dropzone.description')) }}</span>
    </label>
</div>
