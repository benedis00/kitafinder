@props([
    'target' => 'submit'
])

<form {{ $attributes->merge([ 'wire:submit.prevent' => $target ])->class('space-y-4 xs:space-y-6') }}>
    {{ $slot }}
</form>
