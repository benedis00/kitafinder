@props([
    'invalid' => false
])

@php
    $attributes = $attributes->merge([
        'style' => 'background-position: right 0.7rem center; background-size: 1.5rem 1.5rem;',
        'class' => 'px-3 py-2 pr-10'
    ]);

    if ($invalid) {
        $attributes = $attributes->class('!border-red-500');
    }

    $attributes = $attributes->class(join(' ', [
        'appearance-none focus:outline-none',
        'border-solid border-2',
        'rounded block w-full',
        'transition duration-200',
        'disabled:opacity-50',
        'bg-white',
        'border-gray-200',
        'focus:border-primary',
        'placeholder-gray-400',

        'bg-no-repeat'
    ]));
@endphp

<select {{ $attributes}}>{{ $slot }}</select>
