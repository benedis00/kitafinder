@aware([
    'target'
])

@props([
    'target' => 'submit',
    'form' => null
])

<x-button {{ $attributes->class('flex items-center') }} :form="$form" type="submit" wire:loading.attr="disabled" wire:target="{{ $target }}">
    <x-animation.submit-spinner :secondary="$attributes->has('secondary') || $attributes->has('white')" :danger="$attributes->has('danger')" wire:loading wire:target="{{ $target }}" />
    {{ $slot }}
</x-button>
