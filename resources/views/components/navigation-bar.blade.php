<nav class="fixed z-40 text-primary h-16 sm:h-20 w-full flex items-center justify-between gap-4 px-4 sm:px-8 shadow-md">

    {{-- Application Info --}}
    <a href="{{ route('dashboard') }}" class="{{ auth()->check() ? 'hidden xl:flex' : 'flex' }} items-center space-x-4 font-medium">
        <i class="fas fa-child text-2xl"></i>
        <span class="text-lg font-serif whitespace-nowrap">{{ config('app.name') }}</span>
    </a>

    @auth
        <div class="flex xl:hidden" x-data="{ disclosed: false }">
            <i class="fas fa-bars text-2xl cursor-pointer" x-bind:class="{ 'fa-times': disclosed, 'fa-bars': !disclosed }" x-on:click="disclosed = !disclosed; document.body.classList[ disclosed ? 'add' : 'remove' ]('overflow-hidden')"></i>
            <div class="fixed inset-x-0 top-16 sm:top-20 bottom-0 bg-black bg-opacity-20"
                x-show="disclosed"
                x-on:click="disclosed = false"
                x-transition:enter="transition ease-out duration-100"
                x-transition:enter-start="bg-opacity-0"
                x-transition:enter-end="bg-opacity-20"
                x-transition:leave="transition ease-in duration-100 delay-200"
                x-transition:leave-start="bg-opacity-20"
                x-transition:leave-end="bg-opacity-0"
                style="display: none">
                <div class="h-full max-w-xs bg-gray-50 border-r overflow-y-scroll px-4 xs:px-8"
                    x-show="disclosed"
                    x-on:click="$event.stopPropagation();"
                    x-transition:enter="transform transition ease-out duration-200 delay-100"
                    x-transition:enter-start="-translate-x-12 opacity-0"
                    x-transition:enter-end="translate-x-0 opacity-100"
                    x-transition:leave="transform transition ease-in duration-200"
                    x-transition:leave-start="translate-x-0 opacity-100"
                    x-transition:leave-end="-translate-x-12 opacity-0">
                    <x-sidebar with-logo />
                </div>
            </div>
        </div>
    @endauth

    {{-- Actions --}}
    <div class="flex items-center space-x-4" x-data="{}">
        @auth
            {{-- Name --}}
            <span>{{ auth()->user()->email }}</span>

            {{-- User --}}
            <x-dropdown>
                <x-slot name="trigger">
                    <div class="cursor-pointer flex items-center select-none">
                        <div class="rounded-full flex items-center justify-center overflow-hidden font-bold uppercase text-primary bg-primary-50 h-9 w-9 xl:h-10 xl:w-10 group-focus:ring-2 ring-primary-50 ring-offset-primary ring-offset-2">
                            <img src="{{ auth()->user()->avatar_url }}" alt="{{ auth()->user()->name }}">
                        </div>
                    </div>
                </x-slot>
                <x-dropdown.item icon="fas fa-cog" href="{{ route('settings') }}">{{ trans('settings.title') }}</x-dropdown.item>
                <x-dropdown.item form icon="fas fa-sign-out-alt" href="{{ route('signout') }}">{{ trans('auth.signout') }}</x-dropdown.item>
            </x-dropdown>
        @endauth
    </div>
</nav>
