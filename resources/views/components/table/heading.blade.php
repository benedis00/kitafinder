@props([
    'sortable' => null,
    'onlyIf' => true,
    'centered' => false
])

@if ($onlyIf)
    <th {{ $attributes->class('px-3 xs:px-4 py-2 xs:py-3 whitespace-nowrap '.($centered ? 'text-center' : 'text-left'))->only('class') }}>
        @unless ($sortable)
            <span class="leading-4 font-bold">{{ $slot }}</span>
        @else
            <button {{ $attributes->except('class') }} class="flex items-center space-x-2 text-left leading-4 font-bold group" wire:click="sortBy('{{ $sortable }}')">
                <span>{{ $slot }}</span>

                {{-- Sort Indicators --}}
                <span class="w-2">
                    @php $direction = $this->getFieldSortDirection($sortable) @endphp
                    @if ($direction === 'asc')
                        <i class="fas fa-sort-up group-hover:!hidden"></i>
                        <i class="fas fa-sort-down opacity-40 !hidden group-hover:!inline"></i>
                    @elseif ($direction === 'desc')
                        <i class="fas fa-sort-down group-hover:!hidden"></i>
                        <i class="fas fa-sort opacity-40 !hidden group-hover:!inline"></i>
                    @else
                        <i class="fas fa-sort-up opacity-40 !hidden group-hover:!inline"></i>
                    @endif
                </span>
            </button>
        @endunless
    </th>
@endif
