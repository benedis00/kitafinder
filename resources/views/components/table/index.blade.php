@props([
    'selectable' => null
])

<div class="min-w-full overflow-x-auto overflow-hidden -mx-4 xs:mx-0"
    x-data="{ shadowLeft: false, shadowRight: false, checkShadow() { this.shadowLeft = $el.scrollLeft > 0; this.shadowRight = $el.scrollWidth - $el.scrollLeft !== $el.clientWidth; } }"
    x-bind:class="{ 'shadow-table-left': shadowLeft && !shadowRight, 'shadow-table-right': shadowRight && !shadowLeft, 'shadow-table-inset': shadowLeft && shadowRight }"
    x-init="checkShadow()"
    x-on:scroll="checkShadow();">
    <div class="flex">
        <div class="w-4 xs:w-0 flex-shrink-0"></div>
        <table {{ $attributes->class('w-full') }}>
            <thead>
                <tr>
                    @if ($selectable)
                        <x-table.heading class="w-5">
                            <x-form.checkbox class="cursor-pointer" wire:model="bulkSelectedPage" />
                        </x-table.heading>
                    @endif
                    {{ $head }}
                </tr>
            </thead>

            <tbody>
                {{ $body }}
            </tbody>
        </table>
        <div class="w-4 xs:w-0 flex-shrink-0"></div>
    </div>
</div>
