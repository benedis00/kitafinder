@props([
    'searchPlaceholder',
    'withFilters' => false,
    'activeFilters' => 0
])

<div class="mb-3 xl:mb-5">
    <div class="grid grid-cols-6 gap-2 md:flex mb-2">

        {{-- Search --}}
        <x-form.input wire:model="search" placeholder="{{ $searchPlaceholder }}" class="col-span-4 md:col-span-3 xl:mr-4 2xl:mr-12" />

        {{-- Items Per Page --}}
        <div class="flex items-center whitespace-nowrap col-span-2 md:col-span-1 flex-shrink-0">
            <span class="hidden sm:inline mr-2">{{ trans('data-table.interaction.per-page') }}</span>
            <x-form.select wire:model="itemsPerPage">
                @foreach (\App\Livewire\DataTable\Component::ITEMS_PER_PAGE as $items)
                    <option value="{{ $items }}">{{ $items }}</option>
                @endforeach
            </x-form.select>
        </div>

        {{-- Actions --}}
        <div class="flex col-span-6 space-x-2">
            <div class="flex-grow flex justify-evenly space-x-2">
                @if ($withFilters)
                    <x-button class="w-full" secondary wire:click="$toggle('discloseFilters')">
                        <i class="far fa-filter"></i>
                        @if ($activeFilters > 0)
                            ({{ $activeFilters }})
                        @endif
                    </x-button>
                @endif

                @if (isset($actions) && $actions instanceof \Illuminate\View\ComponentSlot)
                    <x-dropdown full-width>
                        <x-slot name="trigger">
                            <x-button secondary class="w-full" :disabled="!$actions->attributes->get('active')">{{ trans('data-table.interaction.actions') }}</x-button>
                        </x-slot>
                        {{ $actions }}
                    </x-dropdown>
                @endif
            </div>

            @isset($append)
                <div>
                    {{ $append }}
                </div>
            @endisset
        </div>
    </div>

    {{-- Disclosing filters --}}
    @if ($withFilters && $filters->attributes->get('disclosed'))
        <div class="space-y-2 bg-gray-50 p-4 rounded">
            <div class="grid gap-2 grid-cols-1 xs:grid-cols-2 md:grid-cols-3 lg:grid-cols-4">
                {{ $filters }}
            </div>
            <div class="flex justify-end">
                <a class="link inline-block cursor-pointer" wire:click="resetFilters">{{ trans('data-table.interaction.filter.reset') }}</a>
            </div>
        </div>
    @endif
</div>
