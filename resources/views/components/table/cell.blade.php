@props([
    'onlyIf' => true
])

@if ($onlyIf)
    <td {{ $attributes->class('px-3 xs:px-4 py-2 xs:py-3 whitespace-nowrap text-sm leading-5 group-hover:bg-primary-50 group-hover:text-primary first:rounded-l last:rounded-r') }}>
        {{ $slot }}
    </td>
@endif
