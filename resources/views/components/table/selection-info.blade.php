@props([
    'table',
    'pageSelected' => false,
    'allSelected' => false,
    'paginator'
])

@if ($table->hasBulkSelection())
    <x-table.row select-message>
        @unless ($allSelected)
            <div>
                <span>
                    {!! trans_choice('data-table.selection.single', $table->bulkSelectionCount(), [ 'count' => $table->bulkSelectionCount() ]) !!}
                </span>
                <span>
                    @if ($pageSelected)
                        {!! trans('data-table.selection.confirm-all', [ 'count' => $paginator->total() ]) !!}
                        <a class="link cursor-pointer" wire:click="$set('bulkSelectedAll', true)">
                            {{ trans('data-table.selection.select-all') }}
                        </a>
                    @endif
                </span>
            </div>
        @else
            {!! trans('data-table.selection.all', [ 'count' => $paginator->total() ]) !!}
        @endunless
    </x-table.row>
@endif
