@props([
    'filters' => 0,
    'loading' => false
])
<x-table.row empty-state>
    <div>
        <span>
            {{ $loading ? trans('data-table.loading') : trans('data-table.interaction.filter.results.message') }}
        </span>
        @if ($filters > 0)
            <a class="link cursor-pointer" wire:click="resetFilters">{{ trans('data-table.interaction.filter.results.reset') }}</a>
        @endif
    </div>
</x-table.row>
