@aware([
    'selectable' => null,
    'cols' => null
])
@props([
    'selectable' => null,
    'emptyState' => false,
    'selectMessage' => false,
    'cols' => 1
])

@php
    if ($emptyState) {

    } else if ($selectMessage) {
        $attributes = $attributes->class('bg-gray-100');
    } else {
        $attributes = $attributes->class('group')->merge([ 'wire:loading.class.delay' => 'opacity-50' ]);
    }
@endphp

<tr {{ $attributes }}>
    @if ($emptyState || $selectMessage)
        @if ($emptyState)
            <x-table.cell colspan="{{ $cols }}">
                {{ $slot }}
            </x-table.cell>
        @elseif ($selectMessage)
            <x-table.cell colspan="{{ $cols }}">
                {{ $slot }}
            </x-table.cell>
        @endif
    @else
        @if ($selectable)
            <x-table.cell>
                <x-form.checkbox class="cursor-pointer" wire:model="bulkSelected" value="{{ $selectable }}" />
            </x-table.cell>
        @endif
        {{ $slot }}
    @endif
</tr>
