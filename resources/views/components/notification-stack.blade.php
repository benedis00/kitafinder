@push ('scripts')
    @if (session()->has('notification'))
        <script>
            window.dispatchEvent(new CustomEvent('notify', { detail: @json(session()->pull('notification')) }));
            document.currentScript.remove();
        </script>
    @endif
@endpush

<div x-data="notifications" x-on:notify.window="collect($event.detail)" class="fixed inset-0 z-50 px-4 sm:p-6 pointer-events-none flex flex-col items-end space-y-2">
    <template x-for="notification in notifications" :key="notification.uuid">
        <div
            x-transition:enter="transform ease-out duration-300 transition"
            x-transition:enter-start="translate-y-4 opacity-0 sm:translate-y-0 sm:translate-x-4"
            x-transition:enter-end="translate-y-0 opacity-100 sm:translate-x-0"
            x-transition:leave="transition ease-in duration-200"
            x-transition:leave-start="opacity-100"
            x-transition:leave-end="opacity-0"
            class="max-w-sm w-full bg-white shadow-tooltip rounded-md pointer-events-auto drop-shadow-sm"
            x-show="notification.visible">
            <div class="rounded-md overflow-hidden border-l-4" x-bind:class="{ 'border-l-primary': !notification.message.isError, 'border-l-red-500': notification.message.isError }">
                <div class="px-4 py-4">
                    <div class="flex items-start">

                        {{-- Icon --}}
                        <div x-show="notification.message.icon" class="flex-shrink-0 mr-4" x-bind:class="{ 'text-primary-500': !notification.message.isError, 'text-red-500': notification.message.isError }">
                            <i class="text-2xl" x-bind:class="notification.message.icon"></i>
                        </div>

                        {{-- Content --}}
                        <div class="w-0 flex-1 flex flex-col space-y-1.5">
                            <div class="text-sm leading-none font-bold text-gray-900" x-text="notification.message.headline"></div>
                            <div class="text-sm leading-tight text-gray-500" x-text="notification.message.content"></div>
                        </div>

                        {{-- Close button --}}
                        <i class="far fa-times cursor-pointer text-base leading-none" x-on:click="remove(notification.uuid)"></i>
                    </div>
                </div>
            </div>
        </div>
    </template>
</div>
