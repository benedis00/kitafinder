@props(['day', 'times'])

<div class="col-span-1">
    {{ $day }}:
</div>
<div class="col-span-2 times">
    {{ $times }}
</div>
