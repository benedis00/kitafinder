<x-layout.main class="bg-gray-50 !px-8 !pt-28 !pb-8 xs:!pt-40 xs:!pb-28 md:!pt-52">
    {{ $slot }}
</x-layout.main>
