@php
    $title          = config('app.name');
    $description    = '';
    $keywords       = '';
@endphp

<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" dir="ltr">
    <head>
        <meta charset="utf-8">
        <meta name="viewport"       content="width=device-width, initial-scale=1, shrink-to-fit=no, viewport-fit=cover, user-scalable=no">
        <meta name="keywords"       content="@yield('keywords', $keywords)">
        <meta name="description"    content="@yield('description', $description)">
        <meta name="copyright"      content="Copyright &copy; {{ now()->year }} {{ $title }}">
        <meta name="language"       content="{{ app()->getLocale() }}">
        <meta name="robots"         content="none">

        @hasSection('title')
            <title>@yield('title', $title) | {{ $title }}</title>
        @else
            <title>{{ $title }} | {{ $description }}</title>
        @endif

        {{-- Include the site webmanifest --}}
        <link rel="manifest" href="{{ asset('site.webmanifest') }}" />

        <meta name="mobile-web-app-capable"                 content="yes">
        <meta name="apple-mobile-web-app-capable"           content="yes" />
        <meta name="apple-mobile-web-app-title"             content="{{ $title }}">
        <meta name="application-name"                       content="{{ $title }}">
        <meta name="apple-mobile-web-app-status-bar-style"  content="default" />

        {{-- Include main styles --}}
        <link rel="stylesheet" href="{{ mix('css/app.css') }}">
        <link rel="stylesheet" href="https://unpkg.com/leaflet@1.8.0/dist/leaflet.css"
            integrity="sha512-hoalWLoI8r4UszCkZ5kL8vayOGVae1oxXe/2A4AO6J9+580uKHDO3JdHb7NzwwzK5xr/Fs0W40kiNHxM9vyTtQ==" crossorigin=""/>
        <link rel="stylesheet" href="https://unpkg.com/leaflet-control-geocoder/dist/Control.Geocoder.css" />
        <link rel="stylesheet" href="https://unpkg.com/leaflet.markercluster@1.4.1/dist/MarkerCluster.css" />
        <link rel="stylesheet" href="https://unpkg.com/leaflet.markercluster@1.4.1/dist/MarkerCluster.Default.css" />

        <script src="https://kit.fontawesome.com/{{ config('services.font-awesome') }}.js" crossorigin="anonymous"></script>


        @yield('head')
        @stack('styles')

        {{-- Include Livewire styles --}}
        <livewire:styles />
    </head>
    <body>

        {{-- Main application container --}}
        <div id="app">
            <x-navigation-bar />

            <main {{ $attributes->class('pt-16 sm:pt-20 min-h-screen') }}>
                {{ $slot }}
            </main>
        </div>

        {{-- Include Livewire scripts --}}
        <livewire:scripts />
        <livewire:modal-container />

        {{-- Include main scripts --}}
        <!-- Make sure you put this AFTER Leaflet's CSS -->
        <script src="https://unpkg.com/leaflet@1.8.0/dist/leaflet.js"
            integrity="sha512-BB3hKbKWOc9Ez/TAwyWxNXeoV9c1v6FIeYiBieIWkpLjauysF18NzgR1MBNBXf8/KABdlkX68nAhlwcDFLGPCQ==" crossorigin=""></script>
        <script src="{{ mix('js/app.js') }}" charset="utf-8"></script>
        @stack('scripts')
    </body>
</html>
