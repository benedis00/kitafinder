<x-layout.main>

    {{ $slot }}

    <x-notification-stack />
</x-layout.main>
