@props(['danger', 'secondary'])

@php
    $color = 'text-white';
    if (isset($danger)) {
        $color = 'text-white';
    } else if (isset($secondary)) {
        $color = 'text-gray-500';
    }
@endphp
<x-animation.spinner {{ $attributes->class('-ml-1 mr-2 h-4 w-4 '.$color) }} />
