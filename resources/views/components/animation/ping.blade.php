<span class="absolute top-0 right-0 flex h-2.5 w-2.5">
    <span class="animate-ping absolute inline-flex h-full w-full rounded-full bg-primary-500 opacity-75"></span>
    <span class="absolute inline-flex rounded-full h-2.5 w-2.5 bg-primary-500"></span>
</span>
