@props([
    'kita'
])

<h1 class="text-lg"> {{ $kita['name'] }} </h1>
<p class="mb-1">
    <b class="font-semibold">Einrichtungsart</b>: {{ $kita->facility_type->name() }}
</p>
<p class="mb-1">
    {{ $kita->address_street }} {{ $kita->address_house_number }}{{ $kita->address_house_number_addition }} <br>
    {{ $kita->address_postal_code }} {{$kita->address_place}} {{ $kita->supervisoryDistrict->name }} <br>
</p>
<p>
    @if (isset($kita->mobile))
        Tel.: {{ $kita->mobile }} <br>
    @endif
    @if (isset($kita->email))
        Email: <a href="mailto:{{ $kita->email }}" class="!text-[#0078A8]"> {{ $kita->email }} </a> <br>
    @endif
    @if (isset($kita->website))
        Homepage: <a href="{{  $kita->website }}" class="!text-[#0078A8]"> {{  $kita->website }} </a> <br>
    @endif
</p>
