@push('styles')

    <style>

        #bottomControl {
            left: 0px;
            right: 0px;
        }

        #sideControl {
            left: 0px;
            width: calc((100vw * 0.35) - 32px);
            padding-top: 16px;
        }

        @media (max-width: 743px) {
            #sideControl {
                font-size: 16px;
            }
        }

        @media (max-width: 675px) {
            #sideControl {
                font-size: 15px;
            }
        }

        @media (max-width: 860px) {
            #openingHours {
                grid-template-columns: repeat(2, minmax(0, 1fr));
            }
            .times {
                grid-column: span 1 / span 1;
            }
        }

    </style>

@endpush

<div class="mb-4 pb-4 mx-4">

    {{-- Controlling the showed content --}}
    <div class="flex items-center text-lg border-b-2 mx-4 border-gray-300 bg-white select-none fixed z-30 {{ ($left && !$sidebarOpen) ? 'hidden' : '' }}" id="{{ $left ? 'sideControl' : 'bottomControl' }}">
        <div class="border-b-2 pb-1 cursor-pointer -mb-0.5 flex-grow {{ ($contentType === 'filter') ?  'border-primary text-primary' : 'border-gray-300' }}" wire:click="$emit('changeContent', 'filter')">
            <i class="fas fa-filter"></i> Filter
        </div>
        <div class="border-b-2 pb-1 cursor-pointer -mb-0.5 flex-grow {{ ($contentType === 'umkreis') ?  'border-primary text-primary' : 'border-gray-300' }}" wire:click="$emit('changeContent', 'umkreis')">
            <i class="fas fa-street-view"></i> Umkreis
        </div>
        <div class="border-b-2 pb-1 cursor-pointer -mb-0.5 flex-grow {{ ($contentType === 'liste') ?  'border-primary text-primary' : 'border-gray-300' }}" wire:click="$emit('changeContent', 'liste')">
            <i class="fas fa-list"></i> Liste
        </div>
    </div>

    {{-- Content --}}
    <div class="overflow-x-hidden {{ $left ? 'pt-14' : 'pt-10' }}">
        @if ($contentType === 'filter')

            <button class="underline text-gray-500 pt-1 text-sm text-right w-full pb-3" wire:click="resetting">
                Zurücksetzen
            </button>

            {{-- Checkbox if grey markers should be shown --}}
            <div class="p-2 pb-4">
                <x-form.checkbox wire:model="greyKitas" id="greyKitas" :invalid="$errors->has('greyKitas')">
                    Ausgeblendete Kitas als graue Marker anzeigen
                </x-form.checkbox>
                <x-form.error field="greyKitas" />
            </div>

            {{-- Supervisory district and postal code --}}
            <div class="{{ $left ? 'lg:flex' : 'flex' }}">
                <div class="pb-6 {{ $left ? 'lg:pr-2' : 'pr-2' }}">
                    <x-form.label for="district">
                        Bezirk
                    </x-form.label>
                    <x-form.select wire:model="district" id="district" :invalid="$errors->has('district')">
                        <option value="all"> Alle </option>
                        @foreach (App\Models\SupervisoryDistrict::all() as $district)
                            <option value="{{ $district->id }}"> {{ $district->name }} </option>
                        @endforeach
                    </x-form.select>
                    <x-form.error field="district" />
                </div>

                <div class="flex-grow pb-6  {{ $left ? 'lg:pl-2' : 'pl-2' }}">
                    <x-form.label for="postalCode">
                        Postleitzahl
                    </x-form.label>
                    <x-form.input wire:model.lazy="postalCode" placeholder="13597" id="postalCode" :invalid="$errors->has('postalCode')"/>
                    <x-form.error field="postalCode" />
                </div>
            </div>

            {{-- Facility types --}}
            <x-form.label>
                Einrichtungsarten
            </x-form.label>
            <div class="p-6 legend !rounded !shadow-none mb-3">
                @foreach ($facilityTypes as $type)
                    <x-form.checkbox wire:model="facilityTypes.{{ $loop->index }}.checked" id="facilityTypes.{{ $loop->index }}.checked" :invalid="$errors->has('facilityTypes.{{ $loop->index }}.checked')">
                        <i class="{{ $type['class'] }}"></i> {{ $type['name'] }}
                    </x-form.checkbox>
                    <x-form.error field="facilityTypes.{{ $loop->index }}.checked" />
                @endforeach
            </div>

            {{-- Opening Hours --}}
            <x-form.label>
                Öffnungszeiten (an mindestens einem Tag in der Woche)
            </x-form.label>
            <div class="pb-6 flex">
                <div class="pr-2 flex-1">
                    <x-form.label class="text-xs" for="opens">
                        von spätestens
                    </x-form.label>
                    <x-form.select wire:model="opens" id="opens" :invalid="$errors->has('opens')">
                        <option value="all"> Alle </option>
                        @foreach ($openingHours as $hours)
                            <option value="{{ $hours }}"> {{ $hours }} </option>
                        @endforeach
                    </x-form.select>
                    <x-form.error field="opens" />
                </div>

                <div class="pl-2 flex-1">
                    <x-form.label class="text-xs" for="closes">
                        bis frühestens
                    </x-form.label>
                    <x-form.select wire:model="closes" id="closes" :invalid="$errors->has('closes')">
                        <option value="all"> Alle </option>
                        @foreach ($closingHours as $hours)
                            <option value="{{ $hours }}"> {{ $hours }} </option>
                        @endforeach
                    </x-form.select>
                    <x-form.error field="closes" />
                </div>
            </div>

            {{-- Number of places --}}
            <x-form.label>
                Platzanzahl
            </x-form.label>
            <div class="flex pb-6">
                <div class="pr-2">
                    <x-form.label class="text-xs" for="minPlaces">
                        minimum
                    </x-form.label>
                    <x-form.input type="number" wire:model.lazy="minPlaces" placeholder="0" id="minPlaces" :invalid="$errors->has('minPlaces')"/>
                    <x-form.error field="minPlaces" />
                </div>

                <div class="pl-2">
                    <x-form.label class="text-xs" for="maxPlaces">
                        maximum
                    </x-form.label>
                    <x-form.input type="number" wire:model.lazy="maxPlaces" placeholder="358" id="maxPlaces" :invalid="$errors->has('maxPlaces')"/>
                    <x-form.error field="maxPlaces" />
                </div>
            </div>

            {{-- Minimum and maximum age --}}
            <x-form.label>
                Betreuungsalter
            </x-form.label>
            <div class="flex pb-6">
                <div class="pr-2">
                    <x-form.label class="text-xs" for="minAge">
                        minimum
                    </x-form.label>
                    <x-form.input type="number" wire:model.lazy="minAge" placeholder="0" id="minAge" :invalid="$errors->has('minAge')"/>
                    <x-form.error field="minAge" />
                </div>

                <div class="pl-2">
                    <x-form.label class="text-xs" for="maxAge">
                        maximum
                    </x-form.label>
                    <x-form.input type="number" wire:model.lazy="maxAge" placeholder="10" id="maxAge" :invalid="$errors->has('maxAge')"/>
                    <x-form.error field="maxAge" />
                </div>
            </div>

            {{-- Educational focus and features --}}
            <div class="{{ $left ? 'lg:flex' : 'flex' }}">
                <div class="{{ $left ? 'lg:pr-2 lg:pb-0 pb-6' : 'pr-2' }} flex-1 self-end">
                    <x-form.label for="educationalFocus">
                        Pädagogischer Schwerpunkt
                    </x-form.label>
                    <x-form.input wire:model.lazy="educationalFocus" placeholder="Situationsansatz" id="educationalFocus" :invalid="$errors->has('educationalFocus')"/>
                    <div class="{{ $left ? 'lg:hidden' : 'hidden' }}">
                        <x-form.error field="educationalFocus" />
                    </div>
                </div>

                <div class="{{ $left ? 'lg:pl-2 lg:pb-0 pb-6' : 'pl-2' }} flex-1 self-end">
                    <x-form.label for="educationalFeatures">
                        Pädagogische Merkmale
                    </x-form.label>
                    <x-form.input wire:model.lazy="educationalFeatures" placeholder="altersgemischte Betreuung" id="educationalFeatures" :invalid="$errors->has('educationalFeatures')"/>
                    <div class="{{ $left ? 'lg:hidden' : 'hidden' }}">
                        <x-form.error field="educationalFeatures" />
                    </div>
                </div>

            </div>

            {{-- Errors next to eachother --}}
            <div class="hidden pb-6 {{ $left ? 'lg:flex' : '!flex' }}">
                <div class="flex-1 {{ $left ? 'lg:pr-2' : 'pr-2' }}">
                    <x-form.error field="educationalFocus" />
                </div>
                <div class="flex-1 {{ $left ? 'lg:pl-2' : 'pl-2' }}">
                    <x-form.error field="educationalFeatures" />
                </div>
            </div>


            {{-- Provider name and type --}}
            <div class="{{ $left ? 'lg:flex' : 'flex' }}">
                <div class="{{ $left ? 'lg:pr-2 lg:pb-0 pb-6' : 'pr-2' }} flex-1 self-end">
                    <x-form.label for="providerName">
                        Name des Trägers
                    </x-form.label>
                    <x-form.input wire:model.lazy="providerName" placeholder="Kindergärten NordOst" id="providerName" :invalid="$errors->has('providerName')"/>
                    <div class="{{ $left ? 'lg:hidden' : 'hidden' }}">
                        <x-form.error field="providerName" />
                    </div>
                </div>

                <div class="{{ $left ? 'lg:pl-2 lg:pb-0 pb-6' : 'pl-2' }} flex-1 self-end">
                    <x-form.label for="providerType">
                        Trägerart
                    </x-form.label>
                    <x-form.select wire:model="providerType" id="providerType" :invalid="$errors->has('providerType')">
                        <option value="all"> Alle </option>
                        @foreach (App\Enums\ProviderType::cases() as $case)
                            <option value="{{ $case->name }}"> {{ $case->name() }} </option>
                        @endforeach
                    </x-form.select>
                    <div class="{{ $left ? 'lg:hidden' : 'hidden' }}">
                        <x-form.error field="providerType" />
                    </div>
                </div>
            </div>

            {{-- Errors next to eachother --}}
            <div class="hidden pb-6 {{ $left ? 'lg:flex' : '!flex' }}">
                <div class="flex-1 {{ $left ? 'lg:pr-2' : 'pr-2' }}">
                    <x-form.error field="providerName" />
                </div>
                <div class="flex-1 {{ $left ? 'lg:pl-2' : 'pl-2' }}">
                    <x-form.error field="providerType" />
                </div>
            </div>


            {{-- Checkboxes if information is provided --}}
            <x-form.label>
                Angaben müssen vorhanden sein
            </x-form.label>
            <div class="p-2">
                <x-form.checkbox wire:model="email" id="email" :invalid="$errors->has('email')">
                    Email
                </x-form.checkbox>
                <x-form.error field="email" />
            </div>
            <div class="p-2">
                <x-form.checkbox wire:model="website" id="website" :invalid="$errors->has('website')">
                    Webseite
                </x-form.checkbox>
                <x-form.error field="website" />
            </div>
            <div class="p-2">
                <x-form.checkbox wire:model="mobile" id="mobile" :invalid="$errors->has('mobile')">
                    Telefonnummer
                </x-form.checkbox>
                <x-form.error field="mobile" />
            </div>
            <div class="p-2">
                <x-form.checkbox wire:model="fax" id="fax" :invalid="$errors->has('fax')">
                    Faxnummer
                </x-form.checkbox>
                <x-form.error field="fax" />
            </div>



        @elseif ($contentType === 'umkreis')

            <div class="ml-1 mt-2">
                <div class="pb-6">
                    <x-form.label for="address">
                        Adresse
                    </x-form.label>
                    <x-form.input wire:model.lazy="address" placeholder="Albrechtstraße 20, 10117 Berlin Mitte" id="address" :invalid="$errors->has('address')"/>
                    <x-form.error field="address" />
                </div>

                <div class="pb-8">
                    <x-form.label for="circle">
                        Umkreis in km
                    </x-form.label>
                    <x-form.input wire:model.lazy="circle" type="number" id="circle" :invalid="$errors->has('circle')" class="w-24"/>
                    <x-form.error field="circle" />
                </div>

                <x-button class="mb-1" wire:click="searchCircle">
                    Umkreissuche starten
                </x-button>

                <button class="underline text-gray-500 pt-1 text-sm" wire:click="deleteCircle">
                    Umkreis entfernen
                </button>
            </div>


        @elseif ($contentType === 'liste')

            <div class="pb-2 mt-2">
                <x-form.label for="kitaName">
                    Name der Kita
                </x-form.label>
                <x-form.input wire:model="kitaName" placeholder="Suche nach gefilterten Kitas" id="kitaName" :invalid="$errors->has('kitaName')"/>
                <x-form.error field="kitaName" />
            </div>

            @foreach ($this->getList() as $listItem)
                <div class="p-4 border-2 border-gray-400 rounded-lg my-4 bg-gray-100">
                    <button wire:click="showPopup({{ $listItem }})" class="text-left w-full">
                        <x-list-item :kita="$listItem"/>
                    </button>
                </div>
            @endforeach

            {{ $this->getList()->links() }}

        @else

            {{-- Name --}}
            <h1 class="text-xl mb-3 mt-2">
                {{ $kita->name }}
            </h1>

            {{-- Contactdetails --}}
            <div class="mb-3">
                <h2 class="text-lg mb-1">
                    Kontakt
                </h2>
                <p> {{ $kita->address_street }} {{ $kita->address_house_number }}{{ $kita->address_house_number_addition }} </p>
                <p class="mb-2"> {{ $kita->address_postal_code }} {{$kita->address_place}} {{ $kita->supervisoryDistrict->name }} </p>

                @if (isset($kita->mobile))
                    <p> Tel.: {{ $kita->mobile }} </p>
                @endif
                @if (isset($kita->fax))
                    <p> Fax.: {{ $kita->fax }} </p>
                @endif
                @if (isset($kita->email))
                    <p> Email: <a href="mailto:{{ $kita->email }}" class="!text-[#0078A8]"> {{ $kita->email }} </a> </p>
                @endif
                @if (isset($kita->website))
                    <p> Homepage: <a href="{{  $kita->website}}" class="!text-[#0078A8]"> {{  $kita->website}} </a> </p>
                @endif
            </div>

            {{-- OpeningHours --}}
            @if (isset($kita->opening_hours))
                <div class="mb-3">
                    <h2 class="text-lg mb-1">
                        Öffnungszeiten:
                    </h2>
                    <div class="grid grid-cols-3 gap-1" id="openingHours">

                        <x-opening-hours day="Montag" :times="$times['Mo']"/>
                        <x-opening-hours day="Dienstag" :times="$times['Tu']"/>
                        <x-opening-hours day="Mittwoch" :times="$times['We']"/>
                        <x-opening-hours day="Donnerstag" :times="$times['Th']"/>
                        <x-opening-hours day="Freitag" :times="$times['Fr']"/>
                        <x-opening-hours day="Samstag" :times="$times['Sa']"/>
                        <x-opening-hours day="Sonntag" :times="$times['Su']"/>

                    </div>
                </div>
            @endif

            {{-- Additional Information --}}
            <div class="mb-3">
                <h2 class="text-lg mb-1">
                    Zusätzliche Informationen
                </h2>
                <p>
                    <b class="font-semibold">Einrichtungsart</b>: {{ $kita->facility_type->name() }}
                </p>
                @if (isset($kita->number_of_places))
                    <p class="mt-1">
                        <b class="font-semibold">Platzanzahl</b>: {{ $kita->number_of_places }}
                    </p>
                @endif
                @if (isset($kita->min_age))
                    <p class="mt-1">
                        <b class="font-semibold">Mindestalter</b>: {{ $kita->min_age }}
                    </p>
                @endif
                @if (isset($kita->max_age))
                    <p class="mt-1">
                        <b class="font-semibold">Maximalalter</b>: {{ $kita->max_age }}
                    </p>
                @endif
                @if (isset($kita->educational_focus))
                    <p class="mt-1">
                        <b class="font-semibold">Pädagogische Schwerpunkte</b>: {{ $kita->educational_focus }}
                    </p>
                @endif
                @if (isset($kita->educational_features))
                    <p class="mt-1">
                        <b class="font-semibold">Pädagogische Merkmale</b>: {{ $kita->educational_features }}
                    </p>
                @endif
            </div>

            {{-- Information about the provider --}}
            <div>
                <h2 class="text-lg mb-1">
                    Freier Träger
                </h2>
                <p>
                    {{ $kita->provider->name }}
                </p>
                <p>
                    {{ $kita->provider->type->name() }}
                </p>
                @if (isset($kita->provider->email))
                    <a href="mailto:{{ $kita->email }}" class="!text-[#0078A8]">
                        {{ $kita->provider->email }}
                    </a>
                @endif
                @if (isset($kita->provider->website))
                    <a href="{{ $kita->provider->website }}" class="!text-[#0078A8]">
                        {{ $kita->provider->website }}
                    </a>
                @endif
            </div>

        @endif
    </div>
</div>
