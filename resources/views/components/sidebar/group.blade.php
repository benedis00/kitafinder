@props([
    'title'
])

<div {{ $attributes }}>
    @isset($title)
        <h4 class="font-bold mb-2 uppercase text-sm text-gray-400">
            {{ $title }}
        </h4>
    @endisset
    <ul class="space-y-1">
        {{ $slot }}
    </ul>
</div>
