@push('styles')
    <style>

        #sidebar {
            transition: width 0.3s;
            height: calc(100vh - 80px);
            margin-top: 80px;
        }

        #sidebar a {
            transition: width 0.3s;
        }

        #bottombar {
            transition: height 0.3s;
        }

        #bottmbar a {
            transition: 0.3s;
        }

        #bottombarButton {
            transition: height 0.3s;
            display: none;
        }

        #main {
            transition: margin-left 0.3s;
        }

        #fully {
            transition: 0.3s;
            width: calc(100vw - 32px);
            height: calc(100vh - 80px);
        }

        @media (max-width: 639px) {
            #main {
                margin-left: 0px !important;
                transition: margin-bottom 0.3s;
            }

            #sidebar {
                display: none;
            }

            #bottombar {
                display: block;
            }

            #fully {
                width: calc(100vw);
                height: calc(100vh - 96px);
            }

            #sidebarButton {
                display: none;
            }

            #bottombarButton {
                display: block;
            }
        }

        #leftIcon {
            position: relative;
            top: calc(50% - 8px);
        }

        #bottomIcon {
            vertical-align: top;
        }

    </style>
@endpush

<div wire:ignore>

    <div class="w-0 fixed top-0 left-0 overflow-x-hidden" id="sidebar" wire:ignore.self>
        {{ $contentLeft }}
    </div>

    <div class="flex" id="main">
        <button onclick="toggleSidebar()" class="flex z-40" id="sidebarButton">
            <div class="w-7 h-full">
                <i class="fas fa-chevron-right text-xl" id="leftIcon"></i>
            </div>
            <div class="w-1 h-full bg-primary"></div>
        </button>

        <div id="fully">
            {{$slot}}
        </div>
    </div>

    <button id="bottombarButton" onclick="toggleBottombar()">
        <div class="h-1 w-screen bg-primary"></div>
        <div class="h-7 w-screen">
            <i class="fas fa-chevron-up text-xl" id="bottomIcon"></i>
        </div>
    </button>

    <div class="h-0 w-screen fixed bottom-0 right-0 overflow-x-hidden hidden" id="bottombar">
        {{ $contentBottom }}
    </div>

</div>

@push('scripts')
    <script>
        var sidebarOpen = false;
        var bottombarOpen = false;
        var breakpoint;

        if (window.innerWidth > 639) {
            breakpoint = 'side';
        } else {
            breakpoint = 'bottom';
        }

        function toggleSidebar() {
            if (sidebarOpen) {
                document.getElementById('sidebar').style.width = '0';
                document.getElementById('main').style.marginLeft = '0';
                document.getElementById('leftIcon').className = 'fas fa-chevron-right text-xl';
                sidebarOpen = false;
            } else {
                document.getElementById('sidebar').style.width = (window.innerWidth * 0.35) + 'px';
                document.getElementById('main').style.marginLeft = (window.innerWidth * 0.35) + 'px';
                document.getElementById('leftIcon').className = 'fas fa-chevron-left text-xl';
                sidebarOpen = true;
            }

            window.livewire.emit('updateSidebar', sidebarOpen);

            setTimeout(() => {
                resized = true;
                window.dispatchEvent(new Event('resize'));
            }, 200);
        }

        function toggleBottombar() {
            if (bottombarOpen) {
                document.getElementById('bottombar').style.height = '0';
                document.getElementById('fully').style.height = 'calc(100vh - 96px)';
                document.getElementById('bottomIcon').className = 'fas fa-chevron-up text-xl';
                bottombarOpen = false;
            } else {
                document.getElementById('bottombar').style.height = (window.innerHeight * 0.35) + 'px';
                document.getElementById('fully').style.height = 'calc(100vh - 96px - ' + (window.innerHeight * 0.35) + 'px)';
                document.getElementById('bottomIcon').className = 'fas fa-chevron-down text-xl';
                bottombarOpen = true;
            }

            setTimeout(() => {
                resized = true;
                window.dispatchEvent(new Event('resize'));
            }, 200);
        }

        function resizeSidebar() {
            if (window.innerWidth > 639) {
                if (sidebarOpen) {
                    document.getElementById('sidebar').style.width = (window.innerWidth * 0.35) + 'px';
                    document.getElementById('main').style.marginLeft = (window.innerWidth * 0.35) + 'px';
                    document.getElementById('leftIcon').className = 'fas fa-chevron-left text-xl';
                }

                document.getElementById('sideControl').style.width = ((window.innerWidth * 0.35) - 32) + 'px';
                document.getElementById('fully').style.height = 'calc(100vh - 80px)';

                if ((bottombarOpen !== sidebarOpen) && (breakpoint === 'bottom')) {
                    toggleSidebar();
                }

                breakpoint = 'side';
            } else {
                if (bottombarOpen) {
                    document.getElementById('bottombar').style.height = (window.innerHeight * 0.35) + 'px';
                    document.getElementById('fully').style.height = 'calc(100vh - 96px - ' + (window.innerHeight * 0.35) + 'px)';
                    document.getElementById('bottomIcon').className = 'fas fa-chevron-down text-xl';
                }

                if ((bottombarOpen !== sidebarOpen) && (breakpoint === 'side')) {
                    toggleBottombar();
                }

                breakpoint = 'bottom';
            }
        }

        window.addEventListener('details', function () {
            bottombarOpen = true;
            sidebarOpen = true;
            window.livewire.emit('updateSidebar', sidebarOpen);
            resizeSidebar();

            setTimeout(() => {
                resized = true;
                window.dispatchEvent(new Event('resize'));
            }, 200);
        });
        window.addEventListener('closeSidebar', function () {
            if (bottombarOpen) {
                toggleBottombar();
            }
            if (sidebarOpen) {
                toggleSidebar();
            }
        });
        window.onresize = resizeSidebar;
    </script>
@endpush
