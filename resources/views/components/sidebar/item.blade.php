@props([
    'icon',
    'href',
    'active' => false
])

@php
    $attributes = $attributes->class('-mx-2 px-2 py-1 rounded group');

    $attributes = $active
        ? $attributes->class('bg-gray-200 text-gray-900')
        : $attributes->class('hover:bg-gray-200 text-gray-500');
@endphp

<div @empty($href) {{ $attributes }} @endempty>

    @isset ($href)
        <a {{ $attributes->merge([ 'href' => $href ])->class('block') }}>
    @endisset

    <div class="flex items-center">
        @isset ($icon)
            <div class="w-7 h-7 flex justify-center items-center mr-2">
                <i class="{{ $icon }} text-base group-hover:text-gray-900"></i>
            </div>
        @endisset
        <span class="group-hover:text-gray-900">{{ $slot }}</span>
    </div>

    @isset ($href)
        </a>
    @endisset
</div>
