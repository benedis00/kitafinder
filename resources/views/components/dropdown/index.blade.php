@props([
'size' => 'min-w-[200px]',
'fullWidth' => false
])

<div x-data="{ open: false }" x-on:keydown.window.escape="open = false" x-on:click.away="open = false" x-on:keyup.away="open = false" {{ $attributes->merge(['class' => 'xs:relative'.($fullWidth ? ' w-full' : '') ]) }}>
    <div x-on:click="open = !open" x-on:keydown.enter="open = !open" tabindex="0" class="group outline-none">
        {{ $trigger }}
    </div>
    <div class="origin-top-right absolute inset-x-4 xs:left-auto xs:right-0 rounded shadow-md max-w-xl bg-white mt-2 overflow-hidden text-sm p-3 {{ $size }}" role="menu" aria-orientation="{{ $orientation ?? 'vertical'}}" style="display: none" x-show="open" x-transition:enter="transition duration-75" x-transition:enter-start="transform opacity-0 scale-75" x-transition:enter-end="transform opacity-100 scale-100" x-transition:leave="transition duration-75" x-transition:leave-start="transform opacity-100 scale-100" x-transition:leave-end="transform opacity-0 scale-75" {{ $attributes }}>
        {{ $slot }}
    </div>
</div>
