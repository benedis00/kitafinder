@props([
'button' => false,
'href' => null,
'form' => false,
'icon' => null
])

@php
$attributes = $attributes->merge([
'class' => 'px-4 py-2 block'
]);

if (isset($href) || $button) {
$attributes = $attributes->class('outline-none no-underline rounded select-none transition duration-100 text-gray-500 hover:bg-primary-100 hover:text-primary-500 focus:bg-primary-100 focus:text-primary-500');

if (isset($form)) {
$attributes = $attributes->class('cursor-pointer');
}
}

if (isset($divider)) {
$attributes = $attributes->merge([
'class' => 'border-b border-gray-100'
]);
}
@endphp
@isset ($href)
@if ($form)
<form action="{{ $href }}" method="post" x-data x-on:click="$el.submit()" x-on:keydown.enter="$el.submit()">
    @csrf
    @endif
    <a @if (!$form)) href="{{ $href }}" @else tabindex="0" @endif {{ $attributes->except(['href', 'title', 'icon']) }} role="menuitem">
        @isset($icon)
        <i class="{{ $icon }} mr-2 !w-4"></i>
        @endisset
        {{ isset($title) ? $title : $slot }}
    </a>
    @if ($form)
</form>
@endif
@else
<{{ $button ? 'button' : 'div' }} {{ $attributes->except(['title', 'icon'])->class('w-full text-left whitespace-nowrap') }} @if ($button) x-on:click="open = false" @endif>
    @isset($icon)
    <i class="{{ $icon }} mr-2 !w-4"></i>
    @endisset
    {{ $title ?? $slot }}
</{{ $button ? 'button' : 'div' }}>
@endisset
