@props([
    'backlink' => null
])

@isset ($backlink)
    <div class="text-gray-500 text-sm space-x-2 mb-3">
        &larr; <a {{ $backlink->attributes }}>{{ $backlink }}</a>
    </div>
@endif
<div {{ $attributes->class('mb-5 xl:mb-12') }}>
    <h1 class="text-2xl sm:text-3xl xl:text-4xl whitespace-nowrap overflow-hidden overflow-ellipsis">{{ $slot }}</h1>
    {{ $after ?? '' }}
</div>
