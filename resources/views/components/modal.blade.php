@props([
    'maxWidth' => 'xl',
    'title' => ''
])

@php
    $maxWidth = [
        'sm' => 'sm:max-w-sm',
        'md' => 'sm:max-w-md',
        'lg' => 'sm:max-w-lg',
        'xl' => 'sm:max-w-xl',
        '2xl' => 'sm:max-w-2xl',
        '3xl' => 'sm:max-w-3xl',
        '4xl' => 'sm:max-w-4xl',
        '5xl' => 'sm:max-w-5xl',
        '6xl' => 'sm:max-w-6xl',
        '7xl' => 'sm:max-w-7xl',
    ][$maxWidth];
@endphp

<div class="bg-white xs:rounded-t rounded-b shadow-xl overflow-hidden xs:mx-auto {{ $maxWidth }}" x-on:mousedown.away="hide($event)">

    {{-- Header --}}
    <div class="flex items-center justify-between px-6 pt-4 bg-opacity-20">
        <div class="flex items-center">
            <span class="font-bold text-lg">
                {{ $title }}
            </span>
        </div>
        <span wire:click="$emit('closeModal')" class="cursor-pointer text-3xl opacity-50 hover:opacity-100 transition duration-200">&times;</span>
    </div>

    {{-- Content --}}
    <div class="p-5 !pt-1 xs:p-6">
        {{ $slot }}
    </div>

    {{-- Actions --}}
    <div class="px-5 pb-6 xs:px-6 xs:pb-6 flex justify-end space-x-4">
        {{ $actions }}
    </div>
</div>
