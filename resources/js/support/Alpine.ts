type Alpine = {

    /**
     * References the root Alpine.js element
     */
    $root: HTMLElement,

     /**
      * Proxy for registered references
      */
    $refs: any,

    /**
     * A delegate function for the next tick
     */
    $nextTick: Function
}

export default Alpine;
