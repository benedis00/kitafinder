import { Modal } from "../alpine/Modal";
import { Message } from "../alpine/Notifications";

interface ApplicationWindow extends Window {
    livewire?: any
};

class Application {

    /**
     * The application locale
     */
    public readonly locale: string;

    /**
     * The current csrf token
     */
    public readonly csrfToken?: string;

    /**
     * The application window object
     */
    public readonly window: ApplicationWindow & typeof globalThis;

    /**
     * Instantiates the application
     */
    constructor () {
        this.locale = document.querySelector('html')?.getAttribute('lang') ?? 'de';
        this.csrfToken = document.head.querySelector<HTMLMetaElement>('meta[name="csrf-token"]')?.content ?? undefined;
        this.window = window;
    }

    /**
     * Registers a new application-ready handler
     * @param handler The function to execute when the application is ready
     * @returns The application
     */
    public ready (handler: Function) : Application {
        window.addEventListener('DOMContentLoaded', event => handler(event));

        return this;
    }

    /**
     * Binds a value to the window object
     * @param properties An object of properties to bind
     * @returns The application
     */
    public bind (properties: object) : Application {
        for (let property in properties) {
            (window as any)[property] = properties[property];
        }

        return this;
    }

    /**
     * Generates a uuid
     * @returns A uuid
     */
    public uuid () : string {
        // @ts-ignore
        return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
            (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
        );
    }

    /**
     * Generates a url
     * @param endpoint An endpoint to append
     * @returns The application url
     */
    public url (endpoint?: string) : string {
        const baseUrl = this.window.location.protocol + '//' + this.window.location.hostname;
        if (! endpoint) {
            return baseUrl;
        }

        return baseUrl + '/' + endpoint;
    }

    /**
     * Opens a state aware modal
     * @param path The path to the modal
     * @param params The parameters to pass to the modal
     */
    public modal (path?: string, params?: any) : void {
        Modal(path, params);
    }

    /**
     * Sends a notification
     * @param detail The detail object
     */
    public notify (detail: Message) : void {
        window.dispatchEvent(new CustomEvent('notify', { detail }));
    }
}

export const app = new Application();
