import Alpine from 'alpinejs';

import Expander from './alpine/Expander';
import { Modal } from './alpine/Modal';
import Notifications from './alpine/Notifications';
import { app } from './support/Application';

app.bind({ Alpine, Modal }).ready(() => {
    //
});

// Alpine.data('expander', Expander);
Alpine.data('notifications', Notifications);

Alpine.start();
