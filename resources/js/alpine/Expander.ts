import Alpine from "../support/Alpine";

export default () : any & Alpine => {
    return {

        /**
         * Whether the expandable entity is expanded
         */
        expanded: false,

        /**
         * Toggles the expanded state
         */
        toggle () {
            this.expanded = !this.expanded;
        }
    }
}
