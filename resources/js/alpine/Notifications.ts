import { app } from "../support/Application";

/**
 * Describes a message in a notification
 */
export type Message = {

    /**
     * The notification headline
     */
    headline: string,

    /**
     * The notification content
     */
    content: string,

    /**
     * Wether the notification indicates an error
     */
    isError: boolean,

    /**
     * The font awesome icon to display
     */
    icon: string,

    /**
     * The amount of milliseconds the notification should be visible
     * If set to 0 or a negative value, the notification disappears not automatically
     */
    hideAfter: number
}

/**
 * Describes a notification
 */
type Notification = {

    /**
     * The unique id of the notification
     */
    uuid: string,

    /**
     * The message of the notification
     */
    message: Message,

    /**
     * Whether the notification is currently displayed
     */
    visible: boolean
};

/**
 * Represents the notifications Alpine.js object
 */
type Notifications = {

    /**
     * List of notifications
     */
    notifications: Array<Notification>,

    /**
     * Collects a message object and wraps it in a notification
     */
    collect: (message: Message) => void,

    /**
     * Removes a notification form the stack
     */
    remove: (uuid: string) => void
}

export default () : Notifications => {
    return {

        /**
         * The currently visible notifications
         */
        notifications: Array<Notification>(),

        /**
         * Collects a message object and wraps it in a notification
         * @param message The message object to wrap in a notification
         */
        collect (message: Message) : void {
            const notification: Notification = {
                uuid: app.uuid(),
                message: message,
                visible: false
            };

            this.notifications.splice(0, 0, notification);

            // @ts-ignore
            this.$nextTick(() => {
                const notificationToShow = this.notifications.find(n => n.uuid === notification.uuid);
                if (notificationToShow) {
                    notificationToShow.visible = true;
                }
            });

            if (notification.message.hideAfter > 0) {
                setTimeout(() => this.remove(notification.uuid), notification.message.hideAfter);
            }
        },

        /**
         * Removes a notification form the stack
         * @param uuid The uuid of the notification to remove
         */
        remove (uuid: string) {
            const notification = this.notifications.find(n => n.uuid === uuid);
            if (! notification) {
                return;
            }

            notification.visible = false;

            //
            // Completely remove the notification after the animation has been played
            setTimeout(() => {
                let index = this.notifications.findIndex(message => message.uuid == notification.uuid);
                this.notifications.splice(index, 1);
            }, 200);
        }
    }
};
