L.mapbox.accessToken = api;

//
//map with startpoint and startzoom (, {zoomSnap: 0.25, zoomDelta: 0.5})
var map = L.map('map', {
    zoomControl: false
}).setView([52.520008, 13.404954], 11);

L.control.zoom({
    position: 'bottomright'
}).addTo(map);

//connection to the different kinds of maps
// var osm = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
//     maxZoom: 19,
//     attribution: '© <a href="www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
// });

// var cartodbAttribution = '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, &copy; <a href="https://carto.com/attribution">CARTO</a>';
// var positron = L.tileLayer('https://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png', {
//     attribution: cartodbAttribution
// });

var streets = L.tileLayer('https://api.mapbox.com/styles/v1/mapbox/outdoors-v11/tiles/{z}/{x}/{y}?access_token=' + api, {
       attribution: '© <a href="https://www.mapbox.com/feedback/">Mapbox</a> © <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors © Powered by <a href="https://www.geoapify.com/">Geoapify</a>',
       tileSize: 512,
       zoomOffset: -1
}).addTo(map);

var satellite = L.tileLayer('https://api.mapbox.com/styles/v1/mapbox/satellite-streets-v11/tiles/{z}/{x}/{y}?access_token=' + api, {
    attribution: '© <a href="https://www.mapbox.com/feedback/">Mapbox</a> © <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors © Powered by <a href="https://www.geoapify.com/">Geoapify</a>',
    tileSize: 512,
    zoomOffset: -1
})

//
//layer control
var baseMaps = {
    // "OpenStreetMap": osm,
    // "Carto": positron,
    "Streets": streets,
    "Satellite": satellite
};

var layerControl = L.control.layers(baseMaps).addTo(map);



//
//different colored Icons for the different facility types
var greenIcon = new L.Icon({
    iconUrl: 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-green.png',
    shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
});
var goldIcon = new L.Icon({
    iconUrl: 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-gold.png',
    shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
});
var blueIcon = new L.Icon({
    iconUrl: 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-blue.png',
    shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
});
var redIcon = new L.Icon({
    iconUrl: 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-red.png',
    shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
});
var greyIcon = new L.Icon({
    iconUrl: 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-grey.png',
    shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
});

//
//clusters for the different facility types
var greenKitas = L.markerClusterGroup({
    iconCreateFunction: function (cluster) {
        var childCount = cluster.getChildCount();

        return new L.DivIcon({ html: '<div><span>' + childCount + '</span></div>',
        className: 'green marker-cluster', iconSize: new L.Point(40, 40) });
    },
    showCoverageOnHover: false,
});
var redKitas = L.markerClusterGroup({
    iconCreateFunction: function (cluster) {
        var childCount = cluster.getChildCount();

        return new L.DivIcon({ html: '<div><span>' + childCount + '</span></div>',
        className: 'red marker-cluster', iconSize: new L.Point(40, 40) });
    },
    showCoverageOnHover: false,
});
var blueKitas = L.markerClusterGroup({
    iconCreateFunction: function (cluster) {
        var childCount = cluster.getChildCount();

        return new L.DivIcon({ html: '<div><span>' + childCount + '</span></div>',
        className: 'blue marker-cluster', iconSize: new L.Point(40, 40) });
    },
    showCoverageOnHover: false,
});
var goldKitas = L.markerClusterGroup({
    iconCreateFunction: function (cluster) {
        var childCount = cluster.getChildCount();

        return new L.DivIcon({ html: '<div><span>' + childCount + '</span></div>',
        className: 'gold marker-cluster', iconSize: new L.Point(40, 40) });
    },
    showCoverageOnHover: false,
});
var greyKitas = L.markerClusterGroup({
    iconCreateFunction: function (cluster) {
        var childCount = cluster.getChildCount();

        return new L.DivIcon({ html: '<div><span>' + childCount + '</span></div>',
        className: 'grey marker-cluster', iconSize: new L.Point(40, 40) });
    },
    showCoverageOnHover: false,
});

//
//markers for the different clusters as geojson
var blueMarkers = L.geoJSON(kitas, {
    filter: function(feature, layer) {
        return feature.properties.show_on_map && feature.properties.in_circle && (feature.properties.color === 'blue');
    },
    pointToLayer: function(feature, latlng) {
        return L.marker(latlng, {icon: blueIcon});
    },
    onEachFeature: onEachFeature
});

var greenMarkers = L.geoJSON(kitas, {
    filter: function(feature, layer) {
        return feature.properties.show_on_map && feature.properties.in_circle && (feature.properties.color === 'green');
    },
    pointToLayer: function(feature, latlng) {
        return L.marker(latlng, {icon: greenIcon});
    },
    onEachFeature: onEachFeature
});

var goldMarkers = L.geoJSON(kitas, {
    filter: function(feature, layer) {
        return feature.properties.show_on_map && feature.properties.in_circle && (feature.properties.color === 'gold');
    },
    pointToLayer: function(feature, latlng) {
        return L.marker(latlng, {icon: goldIcon});
    },
    onEachFeature: onEachFeature
});

var redMarkers = L.geoJSON(kitas, {
    filter: function(feature, layer) {
        return feature.properties.show_on_map && feature.properties.in_circle && (feature.properties.color === 'red');
    },
    pointToLayer: function(feature, latlng) {
        return L.marker(latlng, {icon: redIcon});
    },
    onEachFeature: onEachFeature
});

var greyMarkers = L.geoJSON(kitas, {
    filter: function(feature, layer) {
        return !feature.properties.show_on_map || !feature.properties.in_circle;
    },
    pointToLayer: function(feature, latlng) {
        return L.marker(latlng, {icon: greyIcon});
    },
    onEachFeature: onEachFeature
});

//
//add the markers to the related clusters
blueKitas.addLayer(blueMarkers).addTo(map);
greenKitas.addLayer(greenMarkers).addTo(map);
goldKitas.addLayer(goldMarkers).addTo(map);
redKitas.addLayer(redMarkers).addTo(map);
greyKitas.addLayer(greyMarkers);

//
//add the clusters as an overlay to the map
layerControl.addOverlay(blueKitas, '<span class="dot blue"> </span> ' + kitaTypes['blue']).addTo(map);
layerControl.addOverlay(greenKitas, '<span class="dot green"> </span> ' + kitaTypes['green']).addTo(map);
layerControl.addOverlay(goldKitas, '<span class="dot gold"> </span> ' + kitaTypes['gold']).addTo(map);
layerControl.addOverlay(redKitas, '<span class="dot red"> </span> ' + kitaTypes['red']).addTo(map);


//
//add legend
var legend = L.control({position: 'bottomleft'});
legend.onAdd = function (map) {
    var div = L.DomUtil.create('div', 'legend'),
        colors = ['blue', 'green', 'gold', 'red'],
        labels = [ kitaTypes['blue'], kitaTypes['green'], kitaTypes['gold'], kitaTypes['red']];

    // loop throught the facility types
    for (var i = 0; i < colors.length; i++) {
        div.innerHTML +=
            '<p> <i class="' + colors[i] + '"></i> ' + labels[i] + '</p>';
    }

    return div;
}
legend.addTo(map);

layerControl.setPosition('bottomleft');


var shownLayer, polygon;

//
//make sure, every time you hover a different cluster, the other polygon disapears and the cluster goes back to normal
function removePolygon() {
    if (shownLayer) {
        shownLayer.setOpacity(1);
        shownLayer = null;
    }
    if (polygon) {
        map.removeLayer(polygon);
        polygon = null;
    }
};

//
//add to every cluster a hover effect for a custom coverage
blueKitas.on('clustermouseover', function (a) {
    removePolygon();

    a.layer.setOpacity(0.2);
    shownLayer = a.layer;
    polygon = L.polygon(a.layer.getConvexHull(), {color: 'blue'});
    map.addLayer(polygon);
});
blueKitas.on('clustermouseout', removePolygon);

greenKitas.on('clustermouseover', function (a) {
    removePolygon();

    a.layer.setOpacity(0.2);
    shownLayer = a.layer;
    polygon = L.polygon(a.layer.getConvexHull(), {color: 'green'});
    map.addLayer(polygon);
});
greenKitas.on('clustermouseout', removePolygon);

goldKitas.on('clustermouseover', function (a) {
    removePolygon();

    a.layer.setOpacity(0.2);
    shownLayer = a.layer;
    polygon = L.polygon(a.layer.getConvexHull(), {color: 'gold'});
    map.addLayer(polygon);
});
goldKitas.on('clustermouseout', removePolygon);

redKitas.on('clustermouseover', function (a) {
    removePolygon();

    a.layer.setOpacity(0.2);
    shownLayer = a.layer;
    polygon = L.polygon(a.layer.getConvexHull(), {color: 'red'});
    map.addLayer(polygon);
});
redKitas.on('clustermouseout', removePolygon);

greyKitas.on('clustermouseover', function (a) {
    removePolygon();

    a.layer.setOpacity(0.2);
    shownLayer = a.layer;
    polygon = L.polygon(a.layer.getConvexHull(), {color: 'grey'});
    map.addLayer(polygon);
});
greyKitas.on('clustermouseout', removePolygon);

map.on('zoomend', removePolygon);

//
//black address marker for geocoding
var addressMarker;

var violetIcon = new L.Icon({
    iconUrl: 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-violet.png',
    shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
});

//
//Geocoder using mapbox
L.Control.geocoder({
    placeholder: 'Suche nach einer Adresse...',
    geocoder: L.Control.Geocoder.mapbox({
        apiKey: api,
        geocodingQueryParams: {country: 'de'},
        serviceUrl: 'https://api.mapbox.com/geocoding/v5/mapbox.places/' + encodeURIComponent('Berlin ')
      }),
      defaultMarkGeocode: false,
      collapsed: false,
      position: 'topleft'
}).on('markgeocode', function (e) {
    removeAddressMarker();
    addressMarker = L.marker([e.geocode.center.lat, e.geocode.center.lng], {icon: violetIcon});
    addressMarker.bindPopup(e.geocode.name);
    map.fitBounds(e.geocode.bbox);
    addressMarker.addTo(map);
}).addTo(map);

//
//delete address marker
function removeAddressMarker() {
    if (typeof addressMarker !== 'undefined') {
        addressMarker.remove(map);
    }
}

map.on('click', removeAddressMarker);


//
//open the popup and zoom to the marker, if you click on a kita in the list
function openPop(markers, coords) {
    Object.keys(markers).forEach(key => {
        if (markers[key]._latlng.lat == coords.lat && markers[key]._latlng.lng == coords.lng) {
            map.setView(coords, 18);
            setTimeout(() => {markers[key].openPopup();}, 50);
        }
    });
}

function openBluePopup(lng, lat) {
    var markers = blueMarkers._layers;
    var coords = L.latLng(lat,lng);
    openPop(markers, coords);
}

function openRedPopup(lng, lat) {
    var markers = redMarkers._layers;
    var coords = L.latLng(lat,lng);
    openPop(markers, coords);
}

function openGoldPopup(lng, lat) {
    var markers = goldMarkers._layers;
    var coords = L.latLng(lat,lng);
    openPop(markers, coords);
}

function openGreenPopup(lng, lat) {
    var markers = greenMarkers._layers;
    var coords = L.latLng(lat,lng);
    openPop(markers, coords);
}


//
//popup with GeoJSON
function onEachFeature(feature, layer) {
    // does this feature have a property named popupContent?
    if (feature.properties && feature.properties.popupContent) {
        let div = document.createElement('div');
        let text = document.createElement('text');
        text.innerHTML = feature.properties.popupContent;
        let button = document.createElement('button');
        button.innerHTML = '<button class="info-button" style="font-size: 13px; color: blue">Mehr Informationen</button>';
        button.onclick = function(e) {
            e.preventDefault();
            window.livewire.emit('details', feature.properties.kita.id);
            window.livewire.emit('changeContent', 'details');
            const event = new CustomEvent('details');
            window.dispatchEvent(event);
        }
        div.append(text);
        div.append(button);
        layer.bindPopup(div);
    }
}

function recalculateKitas() {
    blueKitas.clearLayers();
    greenKitas.clearLayers();
    goldKitas.clearLayers();
    redKitas.clearLayers();
    greyKitas.clearLayers();


    //
    //markers for the different clusters as geojson
    blueMarkers = L.geoJSON(kitas, {
        filter: function(feature, layer) {
            return feature.properties.show_on_map && feature.properties.in_circle && (feature.properties.color === 'blue');
        },
        pointToLayer: function(feature, latlng) {
            return L.marker(latlng, {icon: blueIcon});
        },
        onEachFeature: onEachFeature
    });

    greenMarkers = L.geoJSON(kitas, {
        filter: function(feature, layer) {
            return feature.properties.show_on_map && feature.properties.in_circle && (feature.properties.color === 'green');
        },
        pointToLayer: function(feature, latlng) {
            return L.marker(latlng, {icon: greenIcon});
        },
        onEachFeature: onEachFeature
    });

    goldMarkers = L.geoJSON(kitas, {
        filter: function(feature, layer) {
            return feature.properties.show_on_map && feature.properties.in_circle && (feature.properties.color === 'gold');
        },
        pointToLayer: function(feature, latlng) {
            return L.marker(latlng, {icon: goldIcon});
        },
        onEachFeature: onEachFeature
    });

    redMarkers = L.geoJSON(kitas, {
        filter: function(feature, layer) {
            return feature.properties.show_on_map && feature.properties.in_circle && (feature.properties.color === 'red');
        },
        pointToLayer: function(feature, latlng) {
            return L.marker(latlng, {icon: redIcon});
        },
        onEachFeature: onEachFeature
    });

    greyMarkers = L.geoJSON(kitas, {
        filter: function(feature, layer) {
            return !feature.properties.show_on_map || !feature.properties.in_circle;
        },
        pointToLayer: function(feature, latlng) {
            return L.marker(latlng, {icon: greyIcon});
        },
        onEachFeature: onEachFeature
    });

    //
    //add the markers to the related clusters
    blueKitas.addLayer(blueMarkers);
    greenKitas.addLayer(greenMarkers);
    goldKitas.addLayer(goldMarkers);
    redKitas.addLayer(redMarkers);
    greyKitas.addLayer(greyMarkers);
}

var circle;
var circleMarker;

//
//delete circle marker
function removeCircleMarker() {
    if (typeof circle !== 'undefined') {
        circle.remove(map);
    }
    if (typeof circleMarker !== 'undefined') {
        circleMarker.remove(map);
    }
}

function calculateCircle(lng, lat, radius, markerPopup) {
    removeCircleMarker();
    removeAddressMarker();
    circleMarker = L.marker([lat, lng], {icon: violetIcon});
    circleMarker.bindPopup(markerPopup)
    circleMarker.addTo(map);

    var coords = L.latLng(lat,lng);
    circle = L.circle(coords, {radius: radius, color: '#1d4ed8'});
    circle.addTo(map);

    var blueIds = removeOutCircle(coords, blueMarkers._layers, radius);
    var greenIds = removeOutCircle(coords, greenMarkers._layers, radius);
    var goldIds = removeOutCircle(coords, goldMarkers._layers, radius);
    var redIds = removeOutCircle(coords, redMarkers._layers, radius);
    var greyIds = removeOutCircle(coords, greyMarkers._layers, radius);
    recalculateKitas();
    map.fitBounds(circle.getBounds());

    var allIds = blueIds;
    allIds = allIds.concat(greenIds);
    allIds = allIds.concat(goldIds);
    allIds = allIds.concat(redIds);
    allIds = allIds.concat(greyIds);

    window.dispatchEvent(new Event('resize'));
    window.livewire.emit('circleListing', allIds);
}

function deleteCircle() {
    removeCircleMarker();
    addAllCircleToMap(blueMarkers._layers);
    addAllCircleToMap(greenMarkers._layers);
    addAllCircleToMap(goldMarkers._layers);
    addAllCircleToMap(redMarkers._layers);
    addAllCircleToMap(greyMarkers._layers);
    recalculateKitas();
}

function deleteFilter() {
    addAllFilterToMap(blueMarkers._layers);
    addAllFilterToMap(greenMarkers._layers);
    addAllFilterToMap(goldMarkers._layers);
    addAllFilterToMap(redMarkers._layers);
    addAllFilterToMap(greyMarkers._layers);
    recalculateKitas();
}

function addAllCircleToMap(markers) {
    Object.keys(markers).forEach(key => {
        markers[key].feature.properties.in_circle = true;
    });
}

function addAllFilterToMap(markers) {
    Object.keys(markers).forEach(key => {
        markers[key].feature.properties.show_on_map = true;
    })
}

function removeOutCircle(coords, markers, radius) {
    var circleIds = [];
    Object.keys(markers).forEach(key => {
        var point = L.latLng(markers[key]._latlng.lat, markers[key]._latlng.lng)
        if (point.distanceTo(coords) > radius) {
            markers[key].feature.properties.in_circle = false;
        } else {
            circleIds = circleIds.concat(markers[key].feature.properties.kita.id);
            markers[key].feature.properties.in_circle = true;
        }
    });
    return circleIds;
}

function removeOverlay(types) {
    types.forEach(element => {
        if (element.class === 'blue') {
            if (element.checked && !map.hasLayer(blueKitas)) {
                blueKitas.addTo(map);
            } else if (!element.checked && map.hasLayer(blueKitas)) {
                blueKitas.remove(map);
            }
        } else if (element.class === 'green') {
            if (element.checked && !map.hasLayer(greenKitas)) {
                greenKitas.addTo(map);
            } else if (!element.checked && map.hasLayer(greenKitas)) {
                greenKitas.remove(map);
            }
        } else if (element.class === 'gold') {
            if (element.checked && !map.hasLayer(goldKitas)) {
                goldKitas.addTo(map);
            } else if (!element.checked && map.hasLayer(goldKitas)) {
                goldKitas.remove(map);
            }
        } else if (element.class === 'red') {
            if (element.checked && !map.hasLayer(redKitas)) {
                redKitas.addTo(map);
            } else if (!element.checked && map.hasLayer(redKitas)) {
                redKitas.remove(map);
            }
        }
    });
}

function removeGreyKitas(show) {
    if (show) {
        greyKitas.addTo(map);
    } else {
        greyKitas.remove(map);
    }
}

map.on('overlayadd', function (event) {
    if (event.name === ('<span class="dot blue"> </span> ' + kitaTypes['blue'])) {
        window.livewire.emit('addType', 'blue', true);
    } else if (event.name === ('<span class="dot green"> </span> ' + kitaTypes['green'])) {
        window.livewire.emit('addType', 'green', true);
    } else if (event.name === ('<span class="dot gold"> </span> ' + kitaTypes['gold'])) {
        window.livewire.emit('addType', 'gold', true);
    } else if (event.name === ('<span class="dot red"> </span> ' + kitaTypes['red'])) {
        window.livewire.emit('addType', 'red', true);
    }
})

map.on('overlayremove', function (event) {
    if (event.name === ('<span class="dot blue"> </span> ' + kitaTypes['blue'])) {
        window.livewire.emit('addType', 'blue', false);
    } else if (event.name === ('<span class="dot green"> </span> ' + kitaTypes['green'])) {
        window.livewire.emit('addType', 'green', false);
    } else if (event.name === ('<span class="dot gold"> </span> ' + kitaTypes['gold'])) {
        window.livewire.emit('addType', 'gold', false);
    } else if (event.name === ('<span class="dot red"> </span> ' + kitaTypes['red'])) {
        window.livewire.emit('addType', 'red', false);
    }
})
