# Kitafinder

## Installation 
Make sure to have php and npm installed.

Run the following commands for initiation: <br />
npm install <br />
composer install

Connect your mysql database and set the .env file with your api keys

## Set Database
First migrate your database: <br />
php artisan migrate

Fill database with commands (make sure to have the right datasets at the right position): <br />
php artisan import:daycare-centers <br />
php artisan import:api-details

## Start Website
Start the database and run the following command in 2 seperate terminals: <br />
php artisan serve <br />
npm run watch
