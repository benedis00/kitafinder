<?php

use App\Http\Controllers\Auth\ConfirmPasswordController;
use App\Http\Controllers\Auth\EmailVerificationController;
use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\Auth\ResetPasswordController;
use App\Http\Controllers\Auth\SessionController;
use App\Http\Controllers\Auth\SignUpController;
use App\Http\Controllers\Auth\TwoFactorAuthenticationController;
use App\Http\Middleware\SetUserLocale;
use App\Http\Middleware\VerifyPasswordResetToken;
use App\Livewire\Pages\Dashboard;
use App\Livewire\Pages\Settings;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * The following routes should be localized.
 * These are usually public routes where there is no authenticated user
 * to get their preferred locale from. The browser locale will be used as a default
 * but can be manually overridden by altering the URL
 *
 * Authenticated routes do not need to be localized since the users preferred locale
 * will be used to select the locale
 */
Route::prefix(/* uses guest localization? LaravelLocalization::setLocale())->middleware('localizationRedirect'*/'')->group(function() {

    //
    // Unauthenticated public product routes
    // Route::view('/', 'pages.product.landing')->name('landing');

    /**
     * The guest-middleware group redirects all requests to the dashboard if the user
     * trying to access the route is already authenticated.
     *
     * Use this group for pages that are only supposed to be accessible for guest
     * such as sign-in, forgot-password, etc.
     */
    Route::middleware('guest')->group(function () {

        Route::prefix('signin')->controller(SessionController::class)->group(function () {
            Route::get('/', 'create')->name('signin.create');
            Route::post('/', 'store')->name('signin.store');

            //
            // Challenge view and post request for two factor authentication
            Route::prefix('challenge')->controller(TwoFactorAuthenticationController::class)->group(function () {
                Route::get('/', 'create')->name('two-factor-authentication.create');
                Route::post('/', 'challenge')->name('two-factor-authentication.challenge');
            });
        });

        Route::prefix('signup')->controller(SignUpController::class)->group(function () {
            Route::get('/', 'create')->name('signup.create');
            Route::post('/', 'store')->name('signup.store');
        });

        Route::prefix('forgot-password')->controller(ForgotPasswordController::class)->group(function () {
            Route::get('/', 'create')->name('password.forgot.create');
            Route::post('/', 'store')->name('password.forgot.store');
        });

        Route::prefix('reset-password')->controller(ResetPasswordController::class)->middleware(VerifyPasswordResetToken::class)->group(function () {
            Route::get('/', 'create')->name('password.reset.create');
            Route::post('/', 'store')->name('password.reset.store');
        });
    });
});

/**
 * The email verification routes must be accessible for unauthenticated users and may not have a language prefix.
 * The corresponding resend email route is located within the auth section
 */
Route::get('verify/{user}', [ EmailVerificationController::class, 'create' ])->name('verify.create')->middleware('signed');

/**
 * The auth-middleware group redirects all requests to the sign-in page
 * if the user trying to access the route is not authenticated.
 *
 * After successful authentication the user is being redirected to the
 * intended destination
 */
Route::middleware([ 'auth', SetUserLocale::class ])->group(function () {
    Route::post('/signout', [ SessionController::class, 'destroy' ])->name('signout');

    //
    // Routes for the password confirmation flow
    Route::prefix('confirm-password')->controller(ConfirmPasswordController::class)->group(function () {
        Route::get('/', 'create')->name('password.confirm.create');
        Route::post('/', 'store')->name('password.confirm.store');
    });

    Route::get('settings', Settings::class)->name('settings');
});

Route::get('/', Dashboard::class)->name('dashboard');


/**
 * The following routes are only available on local environments
 * and should never be exposed to the production environment.
 *
 * Add routes to test functionality if necessary.
 * Use the /playground route and the playground-view to test views.
 */
if (app()->isLocal()) {
    Route::view('playground', 'playground');
    Route::get('/abort/{status}', fn (string $status) => abort($status));
}
