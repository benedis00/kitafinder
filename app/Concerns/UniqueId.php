<?php

namespace App\Concerns;

/**
 * @property string $uid
 * @property string $resourceIdentifier
 */
trait UniqueId
{
    /**
     * Boots the unique id trait and registers setting
     * the unique id when creating an instance of the model
     *
     * @return void
     */
    protected static function bootUniqueId () : void
    {
        static::creating(function (self $model) {

            $uid = null;
            do {
                $uid = collect([ $model->getResourceIdentifier(), str()->random(24) ])->filter()->join('_');
            } while ($model::where('uid', $uid)->exists());

            $model->uid = $uid;
        });
    }

    /**
     * Retrieve the model by the passed unique uid
     *
     * @param string|null $uid
     * @return self|null
     */
    public static function uid (?string $uid = null) : ?self
    {
        return static::firstWhere('uid', $uid);
    }

    /**
     * Gets the unique resource identifier for the model
     *
     * @return string
     */
    public function getResourceIdentifier () : string
    {
        return $this->resourceIdentifier;
    }

    /**
     * Gets the resource identifier statically
     *
     * @return string
     */
    public static function resourceIdentifier () : string
    {
        return (new self)->getResourceIdentifier();
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName () : string
    {
        return 'uid';
    }
}
