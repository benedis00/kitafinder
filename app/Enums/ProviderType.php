<?php

namespace App\Enums;

enum ProviderType: string
{
    /**
     * The identifier for the 'private Kita ohne Abschluss der Rahmenvereinbarung'
     */
    case privateCenter = 'privateCenter';

    /**
     * The identifier for the 'Arbeiterwohlfahrt'
     */
    case workersWelfare = 'workersWelfare';

    /**
     * The identifier for the 'Deutscher Caritasverband'
     */
    case caritas = 'caritas';

    /**
     * The identifier for the 'EKT'
     */
    case ekt = 'ekt';

    /**
     * The identifier for the 'Diakonisches Werk'
     */
    case deaconry = 'deaconry';

    /**
     * The identifier for the 'Eigenbetrieb'
     */
    case ownOperation = 'ownOperation';

    /**
     * The identifier for the 'Betriebskita'
     */
    case companyCenter = 'companyCenter';

    /**
     * The identifier for the 'Sonstiger freier Träger'
     */
    case other = 'other';

    /**
     * Get the provider type by  case
     *
     * @param string $case
     * @return ProviderType
     */
    public static function get (string $case) : ProviderType
    {
        //
        //search for provider type
        switch ($case) {
            case 'private Kita ohne Abschluss der Rahmenvereinbarung':
                return ProviderType::privateCenter;
            case 'Arbeiterwohlfahrt':
                return ProviderType::workersWelfare;
            case 'Deutscher Caritasverband':
                return ProviderType::caritas;
            case 'EKT':
                return ProviderType::ekt;
            case 'Diakonisches Werk':
                return ProviderType::deaconry;
            case 'Eigenbetrieb':
                return ProviderType::ownOperation;
            case 'Betriebskita':
                return ProviderType::companyCenter;
            default:
                //
                //nothing found, musst be 'Sonstiger freier Träger'
                return ProviderType::other;
        }
    }

    /**
     * Gets the readable name of the provider type
     *
     * @return string
     */
    public function name() : string
    {
        return trans('providers.' . $this->value);
    }
}
