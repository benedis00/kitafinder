<?php

namespace App\Enums;

enum FacilityType: string
{
    /**
     * The identifier for the 'Waldkindergarten'
     */
    case forest = 'forest';

    /**
     * The identifier for the 'Eltern-Kind-Gruppe'
     */
    case parentChild = 'parentChild';

    /**
     * The identifier for the 'Eltern-Initiativ-Kindertagesstätte'
     */
    case parentInitiative = 'parentInitiative';

    /**
     * The identifier for the 'Kindertagesstätte'
     */
    case dayCare = 'dayCare';

    /**
     * Get the facility type by case
     *
     * @param string $case
     * @return FacilityType
     */
    public static function get (string $case) : FacilityType
    {
        //
        //search for facility type
        switch ($case) {
            case 'Waldkindergarten':
                return FacilityType::forest;
            case 'Eltern-Kind-Gruppe':
                return FacilityType::parentChild;
            case 'Eltern-Initiativ-Kindertagesstätte':
                return FacilityType::parentInitiative;
            default:
                //
                //nothing found, musst be 'Kindertagesstätte'
                return FacilityType::dayCare;
        }
    }

    /**
     * Gets the readable name of the facility type
     *
     * @return string
     */
    public function name() : string
    {
        return trans('facilities.' . $this->value);
    }
}
