<?php

namespace App\Livewire\Modals;

use App\Livewire\Extensions\StateAwareModal;
use Illuminate\View\View;

class IntroModal extends StateAwareModal
{

    /**
     * Renders the component
     *
     * @return \Illuminate\View\View
     */
    public function render() : View
    {
        return view('modals.intro-modal');
    }
}
