<?php

namespace App\Livewire;

use App\Enums\FacilityType;
use App\Enums\ProviderType;
use App\Models\DaycareCenter;
use App\Models\SupervisoryDistrict;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Http;
use Illuminate\Validation\Rule;
use Illuminate\View\View;
use Livewire\Component;
use Livewire\WithPagination;

class SidebarContent extends Component
{
    use WithPagination;

    /**
     * defines if the sidebar is on the left side
     *
     * @var boolean
     */
    public bool $left;

    /**
     * content Type for changing content of sidebar
     *
     * @var string
     */
    public string $contentType;

    /**
     * defines if the kitas should be shown as grey markers instead of leaving them out
     *
     * @var boolean
     */
    public bool $greyKitas = false;

    /**
     * input of the postal code
     *
     * @var string
     */
    public string $postalCode = '';

    /**
     * input of the district
     *
     * @var string
     */
    public string $district = 'all';

    /**
     * all possible facility types with their color
     *
     * @var array
     */
    public array $facilityTypes;

    /**
     * input when the kita needs to be open
     *
     * @var string
     */
    public string $opens = 'all';

    /**
     * list of possible opening hours
     *
     * @var array
     */
    public array $openingHours;

    /**
     * input when the kita needs to stay open
     *
     * @var string
     */
    public string $closes = 'all';

    /**
     * list of possible closing hours
     *
     * @var array
     */
    public array $closingHours;

    /**
     * input of the educational focus
     *
     * @var string
     */
    public string $educationalFocus = '';

    /**
     * input of the educational features
     *
     * @var string
     */
    public string $educationalFeatures = '';

    /**
     * input of the provider name
     *
     * @var string
     */
    public string $providerName = '';

    /**
     * input of the provider type
     *
     * @var string
     */
    public string $providerType = 'all';

    /**
     * input of the minimum number of places
     *
     * @var string
     */
    public string $minPlaces = '';

    /**
     * input of the maximum number of places
     *
     * @var string
     */
    public string $maxPlaces = '';

    /**
     * input of the minimum age of care
     *
     * @var string
     */
    public string $minAge = '';

    /**
     * input of the maximum age of care
     *
     * @var string
     */
    public string $maxAge = '';

    /**
     * checks if email should be provided
     *
     * @var boolean
     */
    public bool $email = false;

    /**
     * checks if website should be provided
     *
     * @var boolean
     */
    public bool $website = false;

    /**
     * check if mobile number should be provided
     *
     * @var boolean
     */
    public bool $mobile = false;

    /**
     * checks if fax number should be provided
     *
     * @var boolean
     */
    public bool $fax = false;

    /**
     * kita, which details should be shown
     *
     * @var App\Models\DaycareCenter
     */
    public DaycareCenter $kita;

    /**
     * the address used for the circumcircle geocoding
     *
     * @var string
     */
    public string $address;

    /**
     * the range of the circle for the geocoding
     *
     * @var string
     */
    public string $circle;

    /**
     * all filtered Kita Ids for the list
     *
     */
    public array $filterList;

    /**
     * all Kita Ids in the circle
     *
     * @var array
     */
    public array $circleList;

    /**
     * searchinput for name of kita
     *
     * @var string
     */
    public string $kitaName;

    /**
     * contains the timestrings for the opening hours
     *
     * @var array
     */
    public array $times;

    /**
     * specifies if the sidebar is open
     *
     * @var boolean
     */
    public bool $sidebarOpen;

    /**
     * The event listeners
     *
     * @var array
     */
    protected $listeners = [
        'changeContent' => 'changeContent',
        'details' => 'details',
        'filter' => 'filter',
        'addType' => 'addType',
        'removeType' => 'removeType',
        'listing' => 'listing',
        'circleListing' => 'circleListing',
        'resetFilter' => 'resetFilter',
        'updateSidebar' => 'updateSidebar'
    ];

    /**
     * Mounts the component
     *
     * @param boolean $prize
     * @return void
     */
    public function mount(bool $left) : void
    {
        //
        //defines whether the sidebar is located at the left side
        $this->left = $left;

        //
        //the sidebar is initially always closed
        $this->sidebarOpen = false;

        //
        //first content type should be filter
        $this->contentType = 'filter';

        //
        //definig every facility type with its color and its checked property
        $this->facilityTypes = [
            [
                'class' => 'blue',
                'name' => FacilityType::dayCare->name(),
                'type' => FacilityType::dayCare->value,
                'checked' => true,
            ],
            [
                'class' => 'green',
                'name' => FacilityType::forest->name(),
                'type' => FacilityType::forest->value,
                'checked' => true,
            ],
            [
                'class' => 'gold',
                'name' => FacilityType::parentChild->name(),
                'type' => FacilityType::parentChild->value,
                'checked' => true,
            ],
            [
                'class' => 'red',
                'name' => FacilityType::parentInitiative->name(),
                'type' => FacilityType::parentInitiative->value,
                'checked' => true,
            ],
        ];

        //
        //getting the opening and the closing hours
        $open = [];
        $close = [];
        foreach (DaycareCenter::whereNot('opening_hours', null)->select('opening_hours')->distinct()->get() as $kita) {
            $openingHours = explode(';', $kita->opening_hours);
            foreach ($openingHours as $hours) {
                preg_match('/([^\d]+)\s?(.+)(-)(.+)/i', $hours, $split);
                if (!in_array($split[2], $open)) {
                    $open = array_merge($open, [$split[2]]);
                }
                if (!in_array($split[4], $close)) {
                    $close = array_merge($close, [$split[4]]);
                }
            }
        }
        sort($open);
        sort($close);
        $this->openingHours = $open;
        $this->closingHours = $close;

        //
        //provider name and type that include every kita
        $this->providerName = '';
        $this->providerType = 'all';

        //
        //setting the number of places
        $this->minPlaces = '';
        $this->maxPlaces = '';


        //
        //ages that include every kita
        $this->minAge = '';
        $this->maxAge = '';

        //
        //none of the checkboxes for the information that should be provided should be checked
        $this->email = false;
        $this->website = false;
        $this->mobile = false;
        $this->fax = false;


        //
        //the addressinput is empty
        $this->address = '';

        //
        //the initial circle radius is set to 500 meters
        $this->circle = '0.5';


        //
        //the initial list consists of all kitas
        $this->filterList = ['all'];

        //
        //the initial circle doesnt exist
        $this->circleList = ['all'];

        //
        //nameinput for kita is empty
        $this->kitaName = '';
    }


    /**
     * Updates the fields when the content changes
     *
     * @return void
     */
    public function updated(string $property) : void
    {
        if (str_contains($property, 'facilityType')) {
            $this->emit('filter', $property, $this->facilityTypes, $this->left);

            $this->validate();

            $this->dispatchBrowserEvent('filterTypes', ['types' => $this->facilityTypes]);
        } else if ($property === 'greyKitas') {
            $this->emit('filter', $property, $this->$property, $this->left);

            $this->validate();

            $this->dispatchBrowserEvent('greyKitas', ['show' => $this->greyKitas]);
        } else if ($this->contentType === 'filter') {
            $this->emit('filter', $property, $this->$property, $this->left);

            $this->validate();

            $properties = [
                'district' => $this->district,
                'postalCode' => $this->postalCode,
                'facilityTypes' => $this->facilityTypes,
                'opens' => $this->opens,
                'closes' => $this->closes,
                'educationalFocus' => $this->educationalFocus,
                'educationalFeatures' => $this->educationalFeatures,
                'providerName' => $this->providerName,
                'providerType' => $this->providerType,
                'minPlaces' => $this->minPlaces,
                'maxPlaces' => $this->maxPlaces,
                'minAge' => $this->minAge,
                'maxAge' => $this->maxAge,
                'email' => $this->email,
                'website' => $this->website,
                'mobile' => $this->mobile,
                'fax' => $this->fax
            ];
            $this->emit('filtering', $properties);
        } else if ($this->contentType === 'umkreis') {
            $this->emit('filter', $property, $this->$property, $this->left);

            $this->validateOnly($property);
        } else {
            $this->emit('filter', $property, $this->$property, $this->left);

            $this->validateOnly($property);
        }
    }

    /**
     * Updates the page when content changes
     *
     * @return void
     */
    public function updatedKitaName() : void
    {
        $this->resetPage();
    }

    /**
     * content of sidebar got changed
     *
     * @param string $contentType
     * @return void
     */
    public function changeContent(string $contentType) : void
    {
        $this->contentType = $contentType;
    }

    /**
     * checks if the day is contained in the opening hours
     *
     * @param string $openingHour
     * @param string $day
     * @return boolean
     */
    public function containsDay(string $openingHour, string $day) : bool
    {
        if (str_contains($openingHour, $day)) {
            return true;
        }

        if ($day === 'Tu') {
            if (preg_match('/(Mo-Th|Mo-Fr|Mo-We|Mo-Su)/i', $openingHour)) {
                return true;
            }
        }

        if ($day === 'We') {
            if (preg_match('/(Mo-Th|Mo-Fr|Tu-Fr|Mo-Su)/i', $openingHour)) {
                return true;
            }
        }

        if ($day === 'Th') {
            if (preg_match('/(Mo-Fr|Tu-Fr|Mo-Su|We-Sa)/i', $openingHour)) {
                return true;
            }
        }

        if ($day === 'Fr') {
            if (preg_match('/(Mo-Su|We-Sa)/i', $openingHour)) {
                return true;
            }
        }

        if ($day === 'Sa') {
            if (preg_match('/(Mo-Su|Fr-Su)/i', $openingHour)) {
                return true;
            }
        }

        return false;
    }

    /**
     * details of a kita should be shown
     *
     * @param int $id
     * @return void
     */
    public function details(int $id) : void
    {
        $kita = DaycareCenter::find($id);
        $this->kita = $kita;
        $this->times = [];
        if (isset($kita->opening_hours)) {
            $openingHours = explode(';', $kita->opening_hours);
            $this->times['Mo'] = 'geschlossen';
            $this->times['Tu'] = 'geschlossen';
            $this->times['We'] = 'geschlossen';
            $this->times['Th'] = 'geschlossen';
            $this->times['Fr'] = 'geschlossen';
            $this->times['Sa'] = 'geschlossen';
            $this->times['Su'] = 'geschlossen';
            foreach ($openingHours as $openings) {
                preg_match('/([^\d]+)\s?(.+)/i', $openings, $split);
                $time = $split[2];

                if ($this->containsDay($split[1], 'Mo')) {
                    $this->times['Mo'] = $time;
                }
                if ($this->containsDay($split[1], 'Tu')) {
                    $this->times['Tu'] = $time;
                }
                if ($this->containsDay($split[1], 'We')) {
                    $this->times['We'] = $time;
                }
                if ($this->containsDay($split[1], 'Th')) {
                    $this->times['Th'] = $time;
                }
                if ($this->containsDay($split[1], 'Fr')) {
                    $this->times['Fr'] = $time;
                }
                if ($this->containsDay($split[1], 'Sa')) {
                    $this->times['Sa'] = $time;
                }
                if ($this->containsDay($split[1], 'Su')) {
                    $this->times['Su'] = $time;
                }
            }
        }
    }

    /**
     * listens if the other bar changes
     *
     * @param $property
     * @param $filter
     * @return void
     */
    public function filter($property, $filter, $left) : void
    {
        if (str_contains($property, 'facilityType') && $this->left !== $left) {
            $this->facilityTypes[0]['checked'] = $filter[0]['checked'];
            $this->facilityTypes[1]['checked'] = $filter[1]['checked'];
            $this->facilityTypes[2]['checked'] = $filter[2]['checked'];
            $this->facilityTypes[3]['checked'] = $filter[3]['checked'];
        }
        else if($this->left !== $left) {
            $this->$property = $filter;
        }

        $this->validate();
    }

    /**
     * change checkbox of type if overlay removed/added
     *
     * @param string $color
     * @param boolean $add
     * @return void
     */
    public function addType(string $color, bool $add) : void
    {
        if ($add) {
            for ($i = 0; $i < count($this->facilityTypes); $i++) {
                if ($color === $this->facilityTypes[$i]['class']) {
                    $this->facilityTypes[$i]['checked'] = true;
                }
            }
        } else {
            for ($i = 0; $i < count($this->facilityTypes); $i++) {
                if ($color === $this->facilityTypes[$i]['class']) {
                    $this->facilityTypes[$i]['checked'] = false;
                }
            }
        }
    }

    /**
     * when showed kitas are filtered change the one in the list
     *
     * @param array $filterList
     * @return void
     */
    public function listing(array $filterList) : void
    {
        $this->filterList = $filterList;
        $this->resetPage();
    }

    /**
     * when kitas in the circle change, change the list
     *
     * @param array $circleList
     * @return void
     */
    public function circleListing(array $circleList) : void
    {
        $this->circleList = $circleList;
        $this->resetPage();
    }

    /**
     * returns the collection of kitas for the list
     *
     * @return Illuminate\Pagination\LengthAwarePaginator
     */
    public function getList() : LengthAwarePaginator
    {
        $kitaNames = explode(' ', $this->kitaName);
        $kitaList = DaycareCenter::where('name', 'like', '%' . array_shift($kitaNames) . '%');

        foreach ($kitaNames as $name) {
            $kitaList = $kitaList->where('name', 'like', '%' . $name . '%');
        }

        if ($this->filterList !== ['all']) {
            $kitaList = $kitaList->whereIn('id', $this->filterList);
        }

        if ($this->circleList !== ['all']) {
            $kitaList = $kitaList->whereIn('id', $this->circleList);
        }

        if (!$this->facilityTypes[0]['checked']) {
            $kitaList = $kitaList->whereNot('facility_type', $this->facilityTypes[0]['type']);
        }

        if (!$this->facilityTypes[1]['checked']) {
            $kitaList = $kitaList->whereNot('facility_type', $this->facilityTypes[1]['type']);
        }

        if (!$this->facilityTypes[2]['checked']) {
            $kitaList = $kitaList->whereNot('facility_type', $this->facilityTypes[2]['type']);
        }

        if (!$this->facilityTypes[3]['checked']) {
            $kitaList = $kitaList->whereNot('facility_type', $this->facilityTypes[3]['type']);
        }

        return $kitaList->orderBy('name')->paginate();
    }

    /**
     * Triggering JavaScript to show the clicked kita
     *
     * @param App\Models\DaycareCenter $popupKita
     * @return void
     */
    public function showPopup(DaycareCenter $popupKita) : void
    {
        switch ($popupKita->facility_type) {
            case FacilityType::dayCare:
                $this->dispatchBrowserEvent('listClick', ['color' => 'blue', 'lng' => $popupKita->longitude, 'lat' => $popupKita->latitude]);
                break;
            case FacilityType::forest:
                $this->dispatchBrowserEvent('listClick', ['color' => 'green', 'lng' => $popupKita->longitude, 'lat' => $popupKita->latitude]);
                break;
            case FacilityType::parentChild:
                $this->dispatchBrowserEvent('listClick', ['color' => 'gold', 'lng' => $popupKita->longitude, 'lat' => $popupKita->latitude]);
                break;
            default:
                $this->dispatchBrowserEvent('listClick', ['color' => 'red', 'lng' => $popupKita->longitude, 'lat' => $popupKita->latitude]);
                break;
        }
    }

    /**
     * Triggering JavaScript to show only the kitas inside the circle
     *
     * @return void
     */
    public function searchCircle() : void
    {
        $this->validateOnly('address');
        $this->validateOnly('circle');

        $url = 'https://api.mapbox.com/geocoding/v5/mapbox.places/' . urlencode($this->address) . '.json?country=de&access_token=' . config('app.mapbox_token');
        $radius = $this->circle * 1000;

        $response = Http::get($url);
        $center = $response->object()->features[0]->center ?? null;

        if ($center === null) {
            $this->addError('address', 'Die Adresse konnte nicht gefunden werden');
            return;
        }

        $markerPopup = '<p>' . $response->object()->features[0]->place_name . '</p>';

        $this->dispatchBrowserEvent('circleClick', ['lng' => $center[0], 'lat' => $center[1], 'radius' => $radius, 'markerPopup' => $markerPopup]);
    }

    /**
     * Triggering JavaScript to delete the circle
     *
     * @return void
     */
    public function deleteCircle() : void
    {
        $this->dispatchBrowserEvent('circleDelete');
        $this->emit('circleListing', ['all']);
        $this->circleList = ['all'];
        $this->circle = '';
    }

    /**
     * reset the filter to initial values
     *
     * @return void
     */
    public function resetFilter() : void
    {
        $this->reset([
            'postalCode',
            'district',
            'opens',
            'closes',
            'educationalFeatures',
            'educationalFocus',
            'providerName',
            'providerType',
            'minPlaces',
            'maxPlaces',
            'minAge',
            'maxAge',
            'email',
            'website',
            'mobile',
            'fax',
        ]);
    }

    /**
     * reset the kitas on the map
     *
     * @return void
     */
    public function resetting() : void
    {
        $this->emit('resetFilter');
        $this->filterList = ['all'];
        $this->dispatchBrowserEvent('deleteFilter');
    }

    /**
     * sets the updated bool if the sidebar is open
     *
     * @param boolean $sidebarOpen
     * @return void
     */
    public function updateSidebar(bool $sidebarOpen) : void
    {
        $this->sidebarOpen = $sidebarOpen;
    }

    /**
     * The validation rules
     *
     * @return array
     */
    public function rules() : array
    {
        //
        //possible inputs for districts
        $validationDistrict = SupervisoryDistrict::select('id')->get()->toArray();
        $validationDistrictArray = [];

        foreach ($validationDistrict as $district) {
            $validationDistrictArray[] = (string) $district['id'];
        }
        $validationDistrictArray[] = 'all';

        //
        //possible inputs for opening hours
        $validationOpeningHours = $this->openingHours;
        $validationOpeningHours[] = 'all';

        //
        //possible inputs for closing hours
        $validationClosingHours = $this->closingHours;
        $validationClosingHours[] = 'all';

        //
        //possible inputs for provider types
        $validationProviderTypes = [];

        foreach (ProviderType::cases() as $type) {
            $validationProviderTypes[] = $type->value;
        }
        $validationProviderTypes[] = 'all';

        //
        //validation rules
        switch ($this->contentType) {
            case 'filter':
                $rules = [
                    'greyKitas' => [
                        'nullable',
                        'boolean'
                    ],
                    'postalCode' => [
                        'nullable',
                        'digits:5'
                    ],
                    'district' => [
                        'required',
                        Rule::in($validationDistrictArray)
                    ],
                    'facilityTypes.*.checked' => [
                        'nullable',
                        'boolean'
                    ],
                    'opens' => [
                        'required',
                        Rule::in($validationOpeningHours)
                    ],
                    'closes' => [
                        'required',
                        Rule::in($validationClosingHours)
                    ],
                    'educationalFocus' => [
                        'nullable',
                        'string',
                        'max:255'
                    ],
                    'educationalFeatures' => [
                        'nullable',
                        'string',
                        'max:255'
                    ],
                    'providerName' => [
                        'nullable',
                        'string',
                        'max:255'
                    ],
                    'providerType' => [
                        'required',
                        Rule::in($validationProviderTypes)
                    ],
                    'minPlaces' => [
                        'nullable',
                        'integer',
                        'gte:0'
                    ],
                    'maxPlaces' => [
                        'nullable',
                        'integer',
                        'gte:0',
                        'min:' . $this->minPlaces
                    ],
                    'minAge' => [
                        'nullable',
                        'integer',
                        'gte:0'
                    ],
                    'maxAge' => [
                        'nullable',
                        'integer',
                        'gte:0',
                        'min:' . $this->minAge
                    ],
                    'email' => [
                        'nullable',
                        'boolean'
                    ],
                    'website' => [
                        'nullable',
                        'boolean'
                    ],
                    'mobile' => [
                        'nullable',
                        'boolean'
                    ],
                    'fax' => [
                        'nullable',
                        'boolean'
                    ],
                ];
                break;
            case 'umkreis':
                $rules = [
                    'address' => [
                        'string',
                        'max:255',
                        'required'
                    ],
                    'circle' => [
                        'numeric',
                        'gt:0',
                        'required'
                    ],
                ];
                break;
            case 'liste':
                $rules = [
                    'kitaName' => [
                        'string',
                        'max:255'
                    ],
                ];
                break;
            default:
                $rules = [];
                break;
        }

        return $rules;
    }

    /**
     * The validation error messages
     *
     * @return array
     */
    public function messages() : array
    {
        $rules = [
            'postalCode.*' => 'Eine gültige Postleitzahl hat 5 Stellen',
            'district.*' => 'Eine echte Option muss ausgewählt werden',
            'opens.*' => 'Eine echte Option muss ausgewählt werden',
            'closes.*' => 'Eine echte Option muss ausgewählt werden',
            'educationalFocus.*' => 'Die Zeichenkette darf maximal 255 Charaktere enthalten',
            'educationalFeatures.*' => 'Die Zeichenkette darf maximal 255 Charaktere enthalten',
            'providerName.*' => 'Die Zeichenkette darf maximal 255 Charaktere enthalten',
            'providerType.*' => 'Eine echte Option muss ausgewählt werden',
            'minPlaces.*' => 'Es dürfen nur positive Interger eingegeben werden',
            'maxPlaces.min' => 'Das Maximum darf nicht kleiner sein, als das Minimum',
            'maxPlaces.*' => 'Es dürfen nur positive Interger eingegeben werden',
            'minAge.*' => 'Es dürfen nur positive Interger eingegeben werden',
            'maxAge.min' => 'Das Maximum darf nicht kleiner sein, als das Minimum',
            'maxAge.*' => 'Es dürfen nur positive Interger eingegeben werden',

            'kitaName.*' => 'Die Zeichenkette darf maximal 255 Charaktere enthalten',

            'address.max' => 'Die Zeichenkette darf maximal 255 Charaktere enthalten',
            'address.required' => 'Es muss eine Adresse angegeben werden',
            'circle.gt' => 'Der Umkreis muss größer als 0 sein',
            'circle.*' => 'Der Umkreis wird als Zahl benötigt'
        ];

        return $rules;
    }

    /**
     * Renders the component
     *
     * @return \Illuminate\View\View
     */
    public function render() : View
    {
        return view('components.sidebar.content');
    }
}
