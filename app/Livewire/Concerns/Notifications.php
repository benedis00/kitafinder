<?php

namespace App\Livewire\Concerns;

/**
 * This trait on a livewire component allows the component to trigger in app notifications
 * Use the "$this->notify()" or "$this->notifyError()" function to show a notification.
 */
trait Notifications
{
    /**
     * Dispatches a notification event
     *
     * @param string|array<string, mixed> $options
     * @param boolean $flash Use the flash option when the notification should be displayed in the next request
     * @return void
     */
    public function notify (string|array $options, bool $flash = false) : void
    {
        if (! is_array($options)) {
            $options = [ 'content' => $options ];
        }

        $payload = [
            'headline' => $options['headline'] ?? trans('app.action.success'),
            'content' => $options['content'] ?? '',
            'isError' => $options['isError'] ?? false,
            'icon' => $options['icon'] ?? 'fas fa-check-double',
            'hideAfter' => $options['hideAfter'] ?? 6000
        ];

        if ($flash || ! method_exists($this, 'dispatchBrowserEvent')) {
            session()->flash('notification', $payload);
        } else {
            $this->dispatchBrowserEvent('notify', $payload);
        }
    }

    /**
     * Dispatches a notification event with the error flag
     *
     * @param string|array<string, mixed> $options
     * @param boolean $flash Use the flash option when the notification should be displayed in the next request
     * @return void
     */
    public function notifyError (string|array $options, bool $flash = false) : void
    {
        if (! is_array($options)) {
            $options = [ 'content' => $options ];
        }

        $payload = [
            'headline' => $options['headline'] ?? trans('app.action.failed'),
            'content' => $options['content'] ?? '',
            'isError' => true,
            'icon' => $options['icon'] ?? 'fas fa-exclamation-circle',
            'hideAfter' => $options['hideAfter'] ?? 6000
        ];

        if ($flash || ! method_exists($this, 'dispatchBrowserEvent')) {
            session()->flash('notification', $payload);
        } else {
            $this->dispatchBrowserEvent('notify', $payload);
        }
    }
}
