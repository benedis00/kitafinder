<?php

namespace App\Livewire\Concerns;

/**
 * This trait on a livewire component allows the component to be lazy loaded.
 * Use the attribute >> wire:init="setInitialContentLoadCompleted" << on the root element of the component.
 * Once the component has been loaded it triggers the command.
 * Use "$this->initialContentLoaded()" and "$this->initialContentLoading()" to check whether the content has already been lazy loaded or not
 */
trait LazyLoadsContent
{
    /**
     * Whether the initial content load has been completed
     *
     * @var boolean
     */
    public bool $initialContentLoadCompleted = false;

    /**
     * Sets the initial content load as completed
     *
     * @return void
     */
    public function setInitialContentLoadCompleted () : void
    {
        $this->initialContentLoadCompleted = true;
    }

    /**
     * Checks whether the content has already been lazy loaded
     *
     * @return boolean
     */
    public function initialContentLoaded () : bool
    {
        return $this->initialContentLoadCompleted;
    }

    /**
     * Checks whether the component is currently loading the initial content
     *
     * @return boolean
     */
    public function initialContentLoading () : bool {
        return ! $this->initialContentLoadCompleted;
    }
}
