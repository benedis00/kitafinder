<?php

namespace App\Livewire\Concerns;

use Livewire\WithPagination;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

trait LengthAwarePagination
{
    use WithPagination;

    /**
     * The visible items per page
     *
     * @var integer
     */
    public int $itemsPerPage = 50;

    /**
     * The possible items per page options
     *
     * @var array<integer>
     */
    protected $itemsPerPageOptions = [ 25, 50, 100, 200 ];

    /**
     * Initializes the items per page value
     *
     * @return void
     */
    public function initializeLengthAwarePagination () : void
    {
        $this->itemsPerPage = session()->get('dataTable:itemsPerPage', $this->itemsPerPage);
    }

    /**
     * Updates the selected items per page in the session
     *
     * @param int $value
     * @return void
     */
    public function updatedItemsPerPage (int $value) : void
    {
        if (in_array($value, $this->itemsPerPageOptions)) {
            session()->put('dataTable:itemsPerPage', $value);
        }
    }

    /**
     * Gets the items per page options
     *
     * @return array<integer>
     */
    public function getItemsPerPageOptionsProperty () : array
    {
        return $this->itemsPerPageOptions;
    }

    /**
     * Applies per page pagination to a query
     *
     * @param mixed $query
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public function applyPagination (mixed $query) : LengthAwarePaginator
    {
        return $query->paginate($this->itemsPerPage);
    }

    /**
     * Gets an empty length aware paginator
     *
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public function emptyPaginator () : LengthAwarePaginator
    {
        return new LengthAwarePaginator(new Collection, 0, 1);
    }

    /**
     * Gets the name of the pagination view
     *
     * @return string
     */
    public function paginationView () : string
    {
        return 'pagination';
    }
}
