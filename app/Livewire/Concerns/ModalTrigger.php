<?php

namespace App\Livewire\Concerns;

/**
 * This trait on a livewire component allows the component to open modals.
 * Use the "openModal" function in the component and pass either a dot-notation path to the modal-view
 * or simply put the class name of the modal class. Attributes are passed into the mount() method.
 *
 * $this->openModal('users.create', [ ... ]);
 * $this->openModal(\App\Livewire\Models\Users\Create::class, [ ... ]);
 */
trait ModalTrigger
{
    /**
     * Emits the open modal event
     *
     * @param string $component The component name or class
     * @param array<mixed> $attributes Component attributes to pass
     * @return void
     */
    public function openModal (string $component, array $attributes = []) : void
    {
        if (class_exists($component)) {
            $component = (string) str($component)
                ->replaceFirst(config('livewire.class_namespace').'\\Modals\\', '')
                ->explode('\\')
                ->map(fn ($part) => (string) str($part)->kebab())
                ->join('.');
        }

        $this->emit('openModal', 'modals.'.$component, $attributes);
    }
}
