<?php

namespace App\Livewire\Pages\Settings;

use Illuminate\Support\Collection;
use Illuminate\View\View;
use Livewire\Component;
use PragmaRX\Google2FA\Google2FA;

class TwoFactorAuthentication extends Component
{
    /**
     * Activates the two factor authentication
     *
     * @return void
     */
    public function activate () : void
    {
        //
        // Abort if the user has already two factor authentication enabled
        if (auth()->user()->twoFactorEnabled()) {
            return;
        }

        //
        // Generate a new set of recovery codes
        $recoveryCodes = Collection::times(8, fn () => auth()->user()->generateRecoveryCode())->all();

        //
        // Set the encrypted secret and recovery codes
        auth()->user()->forceFill([
            'two_factor_secret' => encrypt(app(Google2FA::class)->generateSecretKey()),
            'two_factor_recovery_codes' => encrypt(json_encode($recoveryCodes))
        ])->save();
    }

    /**
     * Deactivates the two factor authentication
     *
     * @return void
     */
    public function deactivate () : void
    {
        //
        // Abort if the user has no two factor authentication enabled
        if (! auth()->user()->twoFactorEnabled()) {
            return;
        }

        //
        // Unset the secret and recovery codes
        auth()->user()->forceFill([
            'two_factor_secret' => null,
            'two_factor_recovery_codes' => null
        ])->save();
    }

    /**
     * Regenerates the recovery codes for an active two factor authentication
     *
     * @return void
     */
    public function regenerate () : void
    {
        //
        // Abort if the user has no two factor authentication enabled
        if (! auth()->user()->twoFactorEnabled()) {
            return;
        }

        //
        // Generate a new set of recovery codes
        $recoveryCodes = Collection::times(8, fn () =>  auth()->user()->generateRecoveryCode())->all();

        auth()->user()->forceFill([
            'two_factor_recovery_codes' => encrypt(json_encode($recoveryCodes)),
        ])->save();
    }

    /**
     * Renders the component
     *
     * @return \Illuminate\View\View
     */
    public function render () : View
    {
        return view('pages.settings.two-factor-authentication');
    }
}
