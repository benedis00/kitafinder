<?php

namespace App\Livewire\Pages;

use App\Enums\FacilityType;
use App\Enums\ProviderType;
use App\Models\DaycareCenter;
use Illuminate\View\View;
use Livewire\Component;

class Dashboard extends Component
{
    /**
     * The listed kitas in a geojson format
     *
     * @var string
     */
    public string $geojsonKitas;

    /**
     * A list of every kita type with an color as index
     *
     * @var array
     */
    public array $kitaTypes;

    /**
     * A list of every kita Id what is in the cicle
     *
     * @var array
     */
    public array $circleIds;

    /**
     * The event listeners
     *
     * @var array
     */
    protected $listeners = [
        'filtering' => 'filtering',
        'circleListing' => 'circleListing',
    ];

    /**
     * Mounts the component
     *
     * @return void
     */
    public function mount() : void
    {

        $this->kitaTypes = [
            'blue' => FacilityType::dayCare->name(),
            'green' => FacilityType::forest->name(),
            'gold' => FacilityType::parentChild->name(),
            'red' => FacilityType::parentInitiative->name()
        ];

        $this->circleIds = ['all'];
    }

    /**
     * Fire browser window resize events after rerender.
     *
     * @return void
     */
    public function dehydrate() : void
    {
        $this->dispatchBrowserEvent('resize');
    }

    /**
     * Creates the html-element for the popup
     *
     * @param object $kita
     * @return string
     */
    public function createPopupHtml(object $kita) : string
    {
        $providerType = ProviderType::get($kita->provider_type)->name();

        $html =
            '<div style="max-width: 208px; overflow: hidden">
                <div style="font-weight: 600">' . $kita->name . '</div>
                <p>' . $kita->address_street . ' ' . $kita->address_house_number . $kita->address_house_number_addition . ', ' . $kita->address_postal_code . ' ' . $kita->address_place . ' ' . $kita->supervisory_district_name . '</p>
                <p>' . $kita->opening_hours . '</p>
                <a href="' . $kita->website . '">' . $kita->website . '</a>
                <p>' . (isset($kita->min_age) ? "Betreuungsanfang: " . $kita->min_age : "") . '</p>
                <p>' . (isset($kita->max_age) ? "Betreuungsende: " . $kita->max_age : "") . '</p>
                <p> Trägerart: ' . $providerType . '</p>
            </div>';

        return $html;
    }

    /**
     * Convert an amount of kitas to a geojson format
     *
     * @return object
     */
    public function getJsonKitas() : object
    {
        return DaycareCenter::toBase()->join('providers', 'daycare_centers.provider_id', '=', 'providers.id')
            ->join('supervisory_districts', 'daycare_centers.supervisory_district_id', '=', 'supervisory_districts.id')->selectRaw('daycare_centers.*, providers.name as provider_name, providers.type as provider_type, supervisory_districts.name as supervisory_district_name')->get()
            ->map(function ($kita) {
                $color = 'blue';
                if ($kita->facility_type === FacilityType::forest->value) {
                    $color = 'green';
                }
                elseif ($kita->facility_type === FacilityType::parentChild->value) {
                    $color = 'gold';
                }
                elseif ($kita->facility_type === FacilityType::parentInitiative->value) {
                    $color = 'red';
                }
                return [
                    'type' => 'Feature',
                    'properties' => [
                        'kita' => $kita,
                        'show_on_map' => true,
                        'in_circle' => true,
                        'popupContent' => $this->createPopupHtml($kita),
                        'color' => $color
                    ],
                    'geometry' => [
                        'type' => 'Point',
                        'coordinates' => [$kita->longitude, $kita->latitude]
                    ]
                ];
            });
    }

    /**
     * Kita Ids who are in the circle
     *
     * @param array $circleIds
     * @return void
     */
    public function circleListing(array $circleIds)
    {
        $this->circleIds = $circleIds;
    }

    /**
     * Listening, if the kitas should be reloaded
     *
     * @param array $properties
     * @return void
     */
    public function filtering(array $properties)
    {
        $geojsonKitas = [];
        $kitas = [];

        foreach ($this->getJsonKitas() as $kita) {
            if (($properties['district'] !== 'all') && ((string)($kita['properties']['kita']->supervisory_district_id) !== $properties['district'])) {
                $kita['properties']['show_on_map'] = false;
            }

            if (!(empty($properties['postalCode'])) && ($kita['properties']['kita']->address_postal_code !== $properties['postalCode'])) {
                $kita['properties']['show_on_map'] = false;
            }

            if (($properties['opens'] !== 'all') || ($properties['closes'] !== 'all')) {
                if ($kita['properties']['kita']->opening_hours === null) {
                    $kita['properties']['show_on_map'] = false;
                }
                else {
                    $openingHours = explode(';', $kita['properties']['kita']->opening_hours);
                    $opensEarlier = false;
                    $closesLater = false;
                    foreach ($openingHours as $hours) {
                        preg_match('/([^\d]+)\s?(.+)(-)(.+)/i', $hours, $split);
                        if (($properties['opens'] !== 'all') && (strtotime($split[2]) <= strtotime($properties['opens']))) {
                            $opensEarlier = true;
                        }
                        if (($properties['closes'] !== 'all') && (strtotime($split[4]) >= strtotime($properties['closes']))) {
                            $closesLater = true;
                        }
                    }

                    if (!$opensEarlier && ($properties['opens'] !== 'all')) {
                        $kita['properties']['show_on_map'] = false;
                    }
                    if (!$closesLater && ($properties['closes'] !== 'all')) {
                        $kita['properties']['show_on_map'] = false;
                    }
                }
            }

            if ((!empty($properties['educationalFocus'])) && (!str_contains(strtolower($kita['properties']['kita']->educational_focus), strtolower($properties['educationalFocus'])))) {
                $kita['properties']['show_on_map'] = false;
            }

            if ((!empty($properties['educationalFeatures'])) && (!str_contains(strtolower($kita['properties']['kita']->educational_features), strtolower($properties['educationalFeatures'])))) {
                $kita['properties']['show_on_map'] = false;
            }

            if ((!empty($properties['providerName'])) && (!str_contains(strtolower($kita['properties']['kita']->provider_name), strtolower($properties['providerName'])))) {
                $kita['properties']['show_on_map'] = false;
            }

            if (($properties['providerType'] !== 'all') && ($kita['properties']['kita']->provider_type !== $properties['providerType'])) {
                $kita['properties']['show_on_map'] = false;
            }

            if (!(empty($properties['minPlaces']) && ($properties['minPlaces'] !== '0')) && (($kita['properties']['kita']->number_of_places < $properties['minPlaces']) || ($kita['properties']['kita']->number_of_places === null))) {
                $kita['properties']['show_on_map'] = false;
            }

            if (!(empty($properties['maxPlaces']) && ($properties['maxPlaces'] !== '0')) && (($kita['properties']['kita']->number_of_places > $properties['maxPlaces']) || ($kita['properties']['kita']->number_of_places === null))) {
                $kita['properties']['show_on_map'] = false;
            }

            if (!(empty($properties['minAge']) && ($properties['minAge'] !== '0')) && (($kita['properties']['kita']->min_age > $properties['minAge']) || ($kita['properties']['kita']->min_age === null))) {
                $kita['properties']['show_on_map'] = false;
            }

            if (!(empty($properties['maxAge']) && ($properties['maxAge'] !== '0')) && (($kita['properties']['kita']->max_age < $properties['maxAge']) || ($kita['properties']['kita']->max_age === null))) {
                $kita['properties']['show_on_map'] = false;
            }

            if (($properties['email']) && ($kita['properties']['kita']->email === null)) {
                $kita['properties']['show_on_map'] = false;
            }

            if (($properties['website']) && ($kita['properties']['kita']->website === null)) {
                $kita['properties']['show_on_map'] = false;
            }

            if (($properties['mobile']) && ($kita['properties']['kita']->mobile === null)) {
                $kita['properties']['show_on_map'] = false;
            }

            if (($properties['fax']) && ($kita['properties']['kita']->fax === null)) {
                $kita['properties']['show_on_map'] = false;
            }

            if ($kita['properties']['show_on_map']) {
                $kitas[] = $kita['properties']['kita']->id;
            }

            if ($this->circleIds !== ['all']) {
                if (!in_array($kita['properties']['kita']->id, $this->circleIds)) {
                    $kita['properties']['in_circle'] = false;
                }
            }

            $geojsonKitas[] = $kita;
        }

        $this->dispatchBrowserEvent('updated', ['kitas' => $geojsonKitas]);
        $this->emit('listing', $kitas);

    }

    /**
     * Renders the component
     *
     * @return \Illuminate\View\View
     */
    public function render() : View
    {
        return view('pages.dashboard');
    }
}
