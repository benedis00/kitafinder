<?php

namespace App\Livewire\Pages;

use Illuminate\View\View;
use Livewire\Component;

class Settings extends Component
{
    /**
     * Renders the component
     *
     * @return \Illuminate\View\View
     */
    public function render () : View
    {
        return view('pages.settings');
    }
}
