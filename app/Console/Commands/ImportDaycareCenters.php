<?php

namespace App\Console\Commands;

use App\Enums\FacilityType;
use App\Enums\ProviderType;
use App\Models\DaycareCenter;
use App\Models\Provider;
use App\Models\SupervisoryDistrict;
use Illuminate\Console\Command;

class ImportDaycareCenters extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:daycare-centers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import the daycare centers of the 2 csv files.';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        //
        //patterns for preg_match
        $streetpattern = '/([^\d]+)\s?(.+)/i';
        $houseNumberPattern = '/([\d]+)([^\d])?(.+)?/i';

        //
        //import Kitas from csv
        $open = fopen(public_path('storage/kitas/kitaliste_aktuell.csv'), 'r');
        if ($open !== false) {
            while (($row = fgetcsv($open, null, ';')) !== false) {
                $kita = DaycareCenter::where('facility_id', $row[2])->first();
                if (!isset($kita)) {
                    $kita = new DaycareCenter();

                    $district = SupervisoryDistrict::where('supervisory_district_id', $row[0])->first();
                    if (!isset($district)) {
                        $district = SupervisoryDistrict::create([
                            'supervisory_district_id' => $row[0],
                            'name' => $row[1],
                            'created_by' => 1,
                            'updated_by' => 1
                        ]);
                    }
                    $kita->supervisory_district_id = $district->id;

                    $kita->facility_id = $row[2];
                    $kita->name = $row[3];

                    preg_match($streetpattern, $row[4], $street);
                    $kita->address_street = trim($street[1]);

                    preg_match($houseNumberPattern, $street[2], $houseNumber);
                    $kita->address_house_number = (int)$houseNumber[1];

                    if (isset($houseNumber[2]) && isset($houseNumber[3])) {
                        $kita->address_house_number_addition = $houseNumber[2] . $houseNumber[3];
                    }
                    else if(isset($houseNumber[2])) {
                        $kita->address_house_number_addition = $houseNumber[2];
                    }

                    $kita->address_postal_code = $row[5];

                    if (!empty($row[6])) {
                        $kita->mobile = $row[6];
                    }

                    if (!empty($row[7])) {
                        $kita->number_of_places = $row[7];
                    }

                    $kita->facility_type = FacilityType::get($row[8])->value;

                    $provider = Provider::where('provider_id', $row[9])->first();
                    if (!isset($provider)) {
                        $provider = Provider::create([
                            'provider_id' => $row[9],
                            'name' => $row[10],
                            'type' => ProviderType::get($row[11])->value,
                            'created_by' => 1,
                            'updated_by' => 1
                        ]);
                    }
                    $kita->provider_id = $provider->id;

                    $kita->created_by = 1;
                    $kita->updated_by = 1;
                    $kita->address_place = 'Berlin';
                    $kita->save();
                }
            }
            fclose($open);
        }

        //
        //import more Information from csv
        $open2 = fopen(public_path('storage/kitas/kindertagesstätten_wfs.csv'), 'r');
        if ($open2 !== false) {
            while (($row = fgetcsv($open2, null, ';')) !== false ) {
                $kita = DaycareCenter::where('facility_id', $row[1])->first();
                if (isset($kita)) {
                    if ($row[7] === 'Straße 43' || $row[7] === 'Straße 264' || $row[7] === 'Straße 614') {
                        $kita->address_street = trim($row[7]);
                        $kita->address_house_number = $row[8];

                        if (!empty($row[9]) && ($row[9] !== '-')) {
                            $kita->address_house_number_addition = $row[9];
                        } else {
                            $kita->address_house_number_addition = null;
                        }
                    }

                    if (!isset($kita->address_house_number_addition)) {
                        $kita->address_house_number = $row[8];

                        if (!empty($row[9]) && ($row[9] !== '-')) {
                            $kita->address_house_number_addition = $row[9];
                        }
                    }

                    if (!isset($kita->educational_focus) && !empty($row[14]) && ($row[14] !== '-')) {
                        $kita->educational_focus = $row[14];
                    }

                    if (!isset($kita->educational_features) && !empty($row[15]) && ($row[15] !== '-')) {
                        $kita->educational_features = $row[15];
                    }

                    if (str_contains($row[11], '@')) {
                        if (!isset($kita->email) && $row[11] !== $row[19]) {
                            $kita->email = $row[11];
                        }
                    } else if (!empty($row[11]) && ($row[11] !== '-')) {
                        if (!isset($kita->website) && $row[11] !== $row[19]) {
                            $kita->website = $row[11];
                        }
                    }

                    if (!isset($kita->provider->email) && str_contains($row[19], '@')) {
                        $kita->provider->email = $row[19];
                        $kita->provider->save();
                    } else if (!isset($kita->provider->website) && !empty($row[19]) && ($row[19] !== '-')) {
                        $kita->provider->website = $row[19];
                        $kita->provider->save();
                    }

                    if (!isset($kita->mobile) && !empty($row[10]) && ($row[10] !== '-')) {
                        $kita->mobile = $row[10];
                    }

                    if (!isset($kita->number_of_places) && !empty($row[12]) && ($row[12] !== '-')) {
                        $kita->number_of_places = $row[12];
                    }
                } else {
                    $kita = new DaycareCenter();

                    $district = SupervisoryDistrict::where('supervisory_district_id', $row[3])->first();
                    if (!isset($district)) {
                        $district = SupervisoryDistrict::create([
                            'supervisory_district_id' => $row[3],
                            'name' => $row[4],
                            'created_by' => 1,
                            'updated_by' => 1
                        ]);
                    }
                    $kita->supervisory_district_id = $district->id;

                    $kita->facility_id = $row[1];
                    $kita->name = $row[2];
                    $kita->address_street = trim($row[7]);
                    $kita->address_house_number = $row[8];

                    if (!empty($row[9]) && ($row[9] !== '-')) {
                        $kita->address_house_number_addition = $row[9];
                    }

                    $kita->address_postal_code = $row[5];

                    if (!empty($row[10]) && ($row[10] !== '-')) {
                        $kita->mobile = $row[10];
                    }

                    if (!empty($row[12]) && ($row[12] !== '-')) {
                        $kita->number_of_places = $row[12];
                    }

                    $kita->facility_type = FacilityType::dayCare->value;

                    $provider = Provider::where('provider_id', $row[16])->first();
                    if (!isset($provider)) {
                        $provider = Provider::create([
                            'provider_id' => $row[16],
                            'name' => $row[17],
                            'type' => ProviderType::get($row[18])->value,
                            'created_by' => 1,
                            'updated_by' => 1
                        ]);
                    }
                    if (!isset($provider->email) && str_contains($row[19], '@')) {
                        $provider->email = $row[19];
                    } else if (!isset($provider->website) && !empty($row[19]) && ($row[19] !== '-')) {
                        $provider->website = $row[19];
                    }
                    $provider->save();
                    $kita->provider_id = $provider->id;

                    $kita->created_by = 1;
                    $kita->updated_by = 1;
                    $kita->address_place = 'Berlin';

                    if (!empty($row[14]) && ($row[14] !== '-')) {
                        $kita->educational_focus = $row[14];
                    }

                    if (!empty($row[15]) && ($row[15] !== '-')) {
                        $kita->educational_features = $row[15];
                    }

                    if (str_contains($row[11], '@')) {
                        if ($row[11] !== $row[19]) {
                            $kita->facility_email = $row[11];
                        }
                    } else if (!empty($row[11]) && ($row[11] !== '-')) {
                        if ($row[11] !== $row[19]) {
                            $kita->facility_website = $row[11];
                        }
                    }
                }
                $kita->save();
            }
            fclose($open2);
        }
    }
}
