<?php

namespace App\Console\Commands;

use App\Models\DaycareCenter;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

class ImportApiDetails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:api-details';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get additional information with the Api queries';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {


        $kitas = DaycareCenter::whereNull('longitude')->orWhereNull('latitude')->get();

        $batchInput = [];
        $searchKitas = [];
        for ($max = 0; $max < min(1000, count($kitas)); $max++) {
            $kitaAddress = $kitas[$max]->address_street . ' ' . $kitas[$max]->address_house_number . ', ' . $kitas[$max]->address_postal_code . ' ' . $kitas[$max]->address_place . ' ' . $kitas[$max]->supervisoryDistrict->name;
            $batchInput = array_merge($batchInput,[$kitaAddress]);
            $searchKitas = array_merge($searchKitas, [['kita' => $kitas[$max], 'address' => $kitaAddress]]);
        }

        $batchPostUrl = 'https://api.geoapify.com/v1/batch/geocode/search?&apiKey=' . config('app.geoapify_key') . '&country=Germany';
        $batchPostResponse = Http::post($batchPostUrl, $batchInput);

        if (isset($batchPostResponse->object()->error)) {
            $batchPostResponse->throw();
        }

        $batchGetUrl = 'https://api.geoapify.com/v1/batch/geocode/search?id=' . $batchPostResponse->object()->id . '&apiKey=' . config('app.geoapify_key') . '&format=json';
        sleep(30);
        $batchGetResponse = Http::get($batchGetUrl);

        if (isset($batchGetResponse->object()->error)) {
            $batchGetResponse->throw();
        }

        while (isset($batchGetResponse->object()->status)) {
            if ($batchGetResponse->object()->status === 'pending') {
                sleep(60);
                $batchGetResponse = Http::get($batchGetUrl);
            } else {
                $batchGetResponse->throw();
            }
        }

        $kitaObjects = $batchGetResponse->object();

        $findPlaceUrl1 = 'https://api.geoapify.com/v2/places?categories=childcare&filter=circle:';
        $findPlaceUrl2 = ',100&limit=20&apiKey=' . config('app.geoapify_key');
        $placeDetailsUrl1 = 'https://api.geoapify.com/v2/place-details?id=';
        $placeDetailsUrl2 = '&apiKey=' . config('app.geoapify_key');

        foreach ($kitaObjects as $kitaObject) {
            $search = array_search($kitaObject->query->text, array_column($searchKitas, 'address'));
            if ($search !== false) {
                $searchKita = $searchKitas[$search]['kita'];

                $findPlaceResponse = Http::get($findPlaceUrl1 . $kitaObject->lon . ',' . $kitaObject->lat . $findPlaceUrl2);

                if (isset($findPlaceResponse->object()->error)) {
                    $findPlaceResponse->throw();
                }

                if (count($findPlaceResponse->object()->features) > 1) {
                    foreach ($findPlaceResponse->object()->features as $place) {
                        $equalHouseNumber = false;
                        if (isset($place->properties->housenumber)) {
                            $equalHouseNumber = (((int) $place->properties->housenumber) === $searchKita->address_house_number);
                        }

                        $equalName = false;
                        if (isset($place->properties->name)) {
                            $equalName = $place->properties->name === $searchKita->name;
                        }

                        $equalStreet = false;
                        if (isset($place->properties->street) && isset($kitaObject->street)) {
                            $equalStreet = $place->properties->street === $kitaObject->street;
                        }

                        if ($equalName || ($equalStreet && $equalHouseNumber)) {
                            $searchKita = array_merge(['kita' => $searchKita], ['placeID' => $place->properties->place_id]);
                            break;
                        }
                    }
                } else if (count($findPlaceResponse->object()->features) === 1) {
                    if (count(DaycareCenter::where('address_street', $searchKita->address_street)->get()) > 1) {
                        $equalHouseNumber = false;
                        if (isset($findPlaceResponse->object()->features[0]->properties->housenumber)) {
                            $equalHouseNumber = (((int) $findPlaceResponse->object()->features[0]->properties->housenumber) === $searchKita->address_house_number);
                        }

                        $equalName = false;
                        if (isset($findPlaceResponse->object()->features[0]->properties->name)) {
                            $equalName = $findPlaceResponse->object()->features[0]->properties->name === $searchKita->name;
                        }

                        $equalStreet = false;
                        if (isset($findPlaceResponse->object()->features[0]->properties->street) && isset($kitaObject->street)) {
                            $equalStreet = $findPlaceResponse->object()->features[0]->properties->street === $kitaObject->street;
                        }

                        if ($equalName || ($equalStreet && $equalHouseNumber)) {
                            $searchKita = array_merge(['kita' => $searchKita], ['placeID' => $findPlaceResponse->object()->features[0]->properties->place_id]);
                        }
                    } else {
                        $equalName = false;
                        if (isset($findPlaceResponse->object()->features[0]->properties->name)) {
                            $equalName = $findPlaceResponse->object()->features[0]->properties->name === $searchKita->name;
                        }

                        $equalStreet = false;
                        if (isset($findPlaceResponse->object()->features[0]->properties->street) && isset($kitaObject->street)) {
                            $equalStreet = $findPlaceResponse->object()->features[0]->properties->street === $kitaObject->street;
                        }

                        if ($equalName || $equalStreet) {
                            $searchKita = array_merge(['kita' => $searchKita], ['placeID' => $findPlaceResponse->object()->features[0]->properties->place_id]);
                        } else {
                            if (count(DaycareCenter::where('address_street', $findPlaceResponse->object()->features[0]->properties->street)->get()) === 0
                                && (isset($findPlaceResponse->object()->features[0]->properties->name) ?
                                (count(DaycareCenter::where('name', $findPlaceResponse->object()->features[0]->properties->name)->get()) === 0)
                                : (false))) {
                                $searchKita = array_merge(['kita' => $searchKita], ['placeID' => $findPlaceResponse->object()->features[0]->properties->place_id]);
                            }
                        }
                    }
                }
                if (isset($searchKita['placeID'])) {
                    $placeDetailsResponse = Http::get($placeDetailsUrl1 . $searchKita['placeID'] . $placeDetailsUrl2);

                    if (isset($placeDetailsResponse->object()->error)) {
                        $placeDetailsResponse->throw();
                    }

                    $placeKita = $placeDetailsResponse->object()->features[0]->properties;
                    $csvKita = $searchKita['kita'];

                    $csvKita->website = $placeKita->website ?? null;
                    $csvKita->opening_hours = $placeKita->opening_hours ?? null;
                    $csvKita->mobile = $placeKita->contact->phone ?? null;
                    $csvKita->email = $placeKita->contact->email ?? null;
                    $csvKita->fax = $placeKita->contact->fax ?? null;
                    $csvKita->min_age = $placeKita->restrictions->min_age ?? null;
                    $csvKita->max_age = $placeKita->restrictions->max_age ?? null;
                    $csvKita->longitude = $placeKita->lon;
                    $csvKita->latitude = $placeKita->lat;
                    $csvKita->save();
                } else {
                    $searchKita->longitude = $kitaObject->lon;
                    $searchKita->latitude = $kitaObject->lat;
                    $searchKita->save();
                }
            }
        }
    }
}
