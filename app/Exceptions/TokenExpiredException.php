<?php

namespace App\Exceptions;

use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Thrown when the request expects a token for some sort of verification and said token is expired.
 */
class TokenExpiredException extends HttpException
{
    /**
     * Create a new exception instance.
     *
     * @return void
     */
    public function __construct ()
    {
        parent::__construct(403, 'Verification token expired.');
    }
}
