<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Validator;

class Password implements Rule
{
    /**
     * The min amount of characters
     *
     * @var integer
     */
    public int $minCharacters = 1;

    /**
     * The max amount of characters
     *
     * @var integer
     */
    public int $maxCharacters = 100;

    /**
     * Whether mixed case is required
     *
     * @var boolean
     */
    public bool $mixedCase = false;

    /**
     * Whether digits are required
     *
     * @var boolean
     */
    public bool $digits = false;

    /**
     * Whether special characters are required
     *
     * @var boolean
     */
    public bool $specialCharacters = false;

    /**
     * The list of error messages
     *
     * @var array<string>
     */
    public array $messages = [];

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes ($attribute, $value) : bool {
        $validator = Validator::make(
            [ $attribute => $value ],
            [ $attribute => 'string' ]
        );

        if ($validator->fails()) {
            $this->appendMessage(trans('auth.validation.password.string'));
            return false;
        }

        $results = (object) $this->results($value);

        if (! $results->min) {
            $this->appendMessage(trans('auth.validation.password.min', [ 'length' => $this->minCharacters ]));
        }

        if (! $results->max) {
            $this->appendMessage(trans('auth.validation.password.max', [ 'length' => $this->maxCharacters ]));
        }

        if (! $results->mixed) {
            $this->appendMessage(trans('auth.validation.password.mixed'));
        }

        if (! $results->digits) {
            $this->appendMessage(trans('auth.validation.password.digits'));
        }

        if (! $results->special) {
            $this->appendMessage(trans('auth.validation.password.special'));
        }

        if (! empty($this->messages)) {
            return false;
        }

        return true;
    }

    /**
     * Gets a validation results summary
     *
     * @param string $value
     * @return array<string, mixed>
     */
    public function results (string $value) : array {
        return [
            'min' => strlen($value) >= $this->minCharacters,
            'max' => strlen($value) <= $this->maxCharacters,
            'mixed' => !$this->mixedCase || (preg_match('/[a-z]/', $value) !== 0 && preg_match('/[A-Z]/', $value) !== 0),
            'digits' => !$this->digits || (preg_match('/[0-9]/', $value) !== 0),
            'special' => !$this->specialCharacters || (preg_match('/[#?!@()$%^&*=_{}[\]:;"\'|\\<>,.\/~`±§+-]/', $value) !== 0)
        ];
    }

    /**
     * Appends an error message
     *
     * @param string $message
     * @return void
     */
    private function appendMessage (string $message) : void {
        $this->messages[] = $message;
    }

    /**
     * Get the validation error message.
     *
     * @return array<string>
     */
    public function message () : array {
        return $this->messages;
    }
}
