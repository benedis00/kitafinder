<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class SupervisoryDistrict extends Model
{
    use HasFactory;
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'supervisory_districts';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array<string>
     */
    protected $guarded = [];

    /*
     |--------------------------------------------------------------------------
     | Relationships
     |--------------------------------------------------------------------------
    */

    /**
     * In a supervisory district are multiple daycare centers located
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function daycareCenters() : HasMany
    {
        return $this->hasMany(DaycareCenter::class);
    }
}
