<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Session extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sessions';

    /**
     * Indicates if the model's ID is auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The data type of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that should be cast.
     *
     * @var array<string>
     */
    protected $casts = [
        'last_activity' => 'datetime',
    ];

    /*
    |--------------------------------------------------------------------------
    | Relationships
    |--------------------------------------------------------------------------
    */

    /**
     * The relationship to the session's user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo<\App\Models\User, \App\Models\Session>
     */
    public function user () : BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /*
    |--------------------------------------------------------------------------
    | Methods
    |--------------------------------------------------------------------------
    */

    /**
     * Invalidates the session
     *
     * @return void
     */
    public function invalidate () : void
    {
        $this->forceDelete();
    }

    /**
     * Gets the deserialized session payload
     *
     * @param string|null $get Access a specific property using dot-notation
     * @return mixed
     */
    public function payload (?string $get = null) : mixed
    {
        $payload = unserialize(base64_decode($this->payload));

        if (isset($get)) {
            return data_get($payload, $get);
        }

        return $payload;
    }
}
