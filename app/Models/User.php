<?php

namespace App\Models;

use App\Concerns\TwoFactorAuthentication;
use Illuminate\Contracts\Translation\HasLocalePreference;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Storage;

class User extends Authenticatable implements HasLocalePreference
{
    use HasFactory;
    use SoftDeletes;
    use TwoFactorAuthentication;
    use Notifiable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array<string>
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array<string>
     */
    protected $hidden = [
        'password',
        'remember_token'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime'
    ];

    /*
    |--------------------------------------------------------------------------
    | Relationships
    |--------------------------------------------------------------------------
    */

    /**
     * The relationship to the user's session sessions
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany<\App\Models\Session>
     */
    public function sessions () : HasMany
    {
        return $this->hasMany(Session::class);
    }

    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    */

    /**
     * Gets the avatar url image url for this user
     *
     * @return \Illuminate\Database\Eloquent\Casts\Attribute
     */
    protected function avatarUrl () : Attribute
    {
        return Attribute::make(function () {
            return isset($this->avatar)
                ? Storage::disk('avatars')->url($this->avatar)
                : 'https://unavatar.vercel.app/'.$this->email.'?fallback='.urlencode('https://eu.ui-avatars.com/api/?size=128&name='.$this->name.'&background=ffffff&color=3b82f6');
        });
    }

    /*
    |--------------------------------------------------------------------------
    | Methods
    |--------------------------------------------------------------------------
    */

    /**
     * Checks whether the user has verified its email address
     *
     * @return boolean
     */
    public function isVerified () : bool
    {
        return isset($this->email_verified_at);
    }

    /**
     * Sets a new pending email change address
     *
     * @param string|null $email
     * @return void
     */
    public function setPendingEmailChange (string|null $email = null) : void
    {
        //
        // Remove the change email request when a null email was passed
        if (! isset($email)) {
            UserEmailChange::where('user_id', $this->id)->delete();
            return;
        }

        //
        // Tries to insert the first param array.
        // When there is an entry uniquely identifiable by the user_id (second array)
        // Update the email (third array)
        UserEmailChange::upsert(
            [ 'user_id' => $this->id, 'email' => $email ],
            [ 'user_id' ],
            [ 'email' ]
        );
    }

    /**
     * Gets the currently pending email
     *
     * @return string|null
     */
    public function getPendingEmailChange () : string|null
    {
        return UserEmailChange::firstWhere('user_id', $this->id)?->email;
    }

    /**
     * Sends the email with the verification link for the pending email change
     *
     * @return void
     */
    public function sendPendingEmailVerification () : void
    {
        if ($pendingEmail = $this->getPendingEmailChange()) {
            // Mail::to($pendingEmail)->send(new \App\Mail\Users\VerifyEmail($this, $pendingEmail));
            logger()->info('Verification url valid until: '.now()->addMinutes(30).' => '.url()->temporarySignedRoute('verify.create', now()->addMinutes(30), [
                'user'  => $this,
                'email' => $pendingEmail
            ]));
        } else if (! $this->isVerified()) {
            // Mail::to($this)->send(new \App\Mail\Users\VerifyEmail($this, $this->email));
            logger()->info('Verification url valid until: '.now()->addMinutes(30).' => '.url()->temporarySignedRoute('verify.create', now()->addMinutes(30), [
                'user'  => $this,
                'email' => $this->email
            ]));
        }
    }

    /*
    |--------------------------------------------------------------------------
    | Locale Preference
    |--------------------------------------------------------------------------
    */

    /**
     * Get the user's preferred locale.
     *
     * @return string
     */
    public function preferredLocale () : string
    {
        return app()->getLocale();
    }
}
