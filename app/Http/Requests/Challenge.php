<?php

namespace App\Http\Requests;

use PragmaRX\Google2FA\Google2FA;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class Challenge extends FormRequest {

    /**
     * The user attempting the two factor challenge.
     *
     * @var \App\Models\User|null
     */
    protected ?User $challengedUser = null;

    /**
     * Indicates if the user wished to be remembered after login.
     *
     * @var bool|null
     */
    protected ?bool $remember = null;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize () : bool {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, array<mixed>>
     */
    public function rules () : array {
        return [
            'code' => [ 'nullable', 'string' ]
        ];
    }

    /**
     * Determine if the request has a valid two factor code.
     *
     * @return bool
     */
    public function hasValidCode () : bool {
        return $this->code && app(Google2FA::class)->verifyKey(
            decrypt($this->challengedUser()->two_factor_secret), $this->code
        );
    }

    /**
     * Get the valid recovery code if one exists on the request.
     *
     * @return string|null
     */
    public function validRecoveryCode () : ?string {
        if (! $this->code) {
            return null;
        }

        return collect($this->challengedUser()->recoveryCodes())->first(function ($code) {
            return hash_equals($this->code, $code) ? $code : null;
        });
    }

    /**
     * Determine if there is a challenged user in the current session.
     *
     * @return bool
     */
    public function hasChallengedUser () : bool {
        return $this->session()->has('attempt.id') && User::find($this->session()->get('attempt.id'));
    }

    /**
     * Get the user that is attempting the two factor challenge.
     *
     * @return \App\Models\User
     * @throws \Illuminate\Http\Exceptions\HttpResponseException
     */
    public function challengedUser () : User {

        //
        // If the challenged user was already retrieved
        if (isset($this->challengedUser)) {
            return $this->challengedUser;
        }

        if ($this->session()->has('attempt.id')) {

            /** @var \App\Models\User|null */
            $user = User::find($this->session()->get('attempt.id'));
            if ($user) {

                //
                // Setting and returning the challenged user
                return $this->challengedUser = $user;
            }
        }

        //
        // If the challenged user cannot be retrieved redirect back to the sign in view
        throw new HttpResponseException(
            redirect()->route('signin.create')
        );
    }

    /**
     * Determine if the user wanted to be remembered after login.
     *
     * @return bool
     */
    public function remember() : bool {
        if (! isset($this->remember)) {
            $this->remember = $this->session()->pull('attempt.remember', false);
        }

        return $this->remember;
    }
}
