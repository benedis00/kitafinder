<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Cache\RateLimiter;
use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Cache\RateLimiting\Unlimited;
use Illuminate\Http\Exceptions\ThrottleRequestsException;

class RateLimitedFormRequest extends FormRequest
{
    /**
     * Gets the limit rules for the form request rate limiter
     *
     * @return \Illuminate\Cache\RateLimiting\Limit The limit rules for the rate limiter
     */
    public function limit () : Limit
    {
        return new Unlimited;
    }

    /**
     * Gets the rate limiter for the form request
     *
     * @return \Illuminate\Cache\RateLimiter The rate limiter
     */
    public function rateLimiter (Limit $limit) : RateLimiter
    {
        return app(RateLimiter::class)->for(self::class, fn () => $limit);
    }

    /**
	 * Configure the validator instance.
	 *
	 * @param  \Illuminate\Validation\Validator  $validator
	 * @return void
	 */
	public function withValidator ($validator) : void
    {
        if ($validator->passes()) {
            $limit          = $this->limit();
            $rateLimiter    = $this->rateLimiter($limit);

            //
            // Throw an exception if the form request was tried too many times
            if ($rateLimiter->tooManyAttempts($limit->key, $limit->maxAttempts)) {
                throw new ThrottleRequestsException;
            }

            //
            // If the validation passes hit the rate limiter
            $rateLimiter->hit($limit->key, $limit->decayMinutes * 60);
        }
	}
}
