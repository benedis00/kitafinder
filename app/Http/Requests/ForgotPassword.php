<?php

namespace App\Http\Requests;

use App\Http\Requests\RateLimitedFormRequest;
use Illuminate\Cache\RateLimiting\Limit;

class ForgotPassword extends RateLimitedFormRequest
{
    /**
     * Gets the limit rules for the form request rate limiter
     *
     * @return \Illuminate\Cache\RateLimiting\Limit The limit rules for the rate limiter
     */
    public function limit () : Limit
    {
        return Limit::perMinute(2)->by(request()->ip());
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize () : bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, array<mixed>>
     */
    public function rules () : array
    {
        return [
            'email' => [
                'required',
                'email',
                'exists:users'
            ]
        ];
    }

    /**
     * Get the validation messages
     *
     * @return array<string, string>
     */
    public function messages () : array
    {
        return [
            'email.email' => trans('auth.forgot-password.validation.email.email'),
            'email.exists' => trans('auth.forgot-password.validation.email.exists'),
            'email.*' => trans('auth.forgot-password.validation.email.required')
        ];
    }
}
