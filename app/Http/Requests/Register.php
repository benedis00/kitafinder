<?php

namespace App\Http\Requests;

use App\Http\Requests\RateLimitedFormRequest;
use App\Rules\Password;
use Illuminate\Cache\RateLimiting\Limit;

class Register extends RateLimitedFormRequest
{
    /**
     * Gets the limit rules for the form request rate limiter
     *
     * @return \Illuminate\Cache\RateLimiting\Limit The limit rules for the rate limiter
     */
    public function limit () : Limit
    {
        //
        // Any ip address may sign up a new user 2 times every 5 minutes (every 2.5 minutes)
        return Limit::perMinutes(5, 2)->by(request()->ip() ?? '');
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize () : bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, array<mixed>>
     */
    public function rules () : array
    {
        return [

            'name' => [
                'required',
                'string',
                'min:2',
                'max:100'
            ],

            'email' => [
                'required',
                'email',
                'unique:users',
                'max:255'
            ],

            'password' => [
                'required',
                new Password
            ],

            'accept' => [
                'accepted'
            ]
        ];
    }

    /**
     * Get the validation messages
     *
     * @return array<string, string>
     */
    public function messages () : array
    {
        return [
            'name.min' => trans('auth.signup.validation.name.min'),
            'name.max' => trans('auth.signup.validation.name.max'),
            'name.*' => trans('auth.signup.validation.name.required'),

            'email.email' => trans('auth.signup.validation.email.email'),
            'email.unique' => trans('auth.signup.validation.email.unique'),
            'email.*' => trans('auth.signup.validation.email.required'),

            'password.required' => trans('auth.signup.validation.password.required')
        ];
    }
}
