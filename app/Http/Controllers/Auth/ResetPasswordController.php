<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\UserPasswordReset;
use App\Rules\Password;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class ResetPasswordController extends Controller
{
    /**
     * Show the reset password view
     *
     * @return \Illuminate\View\View
     */
    public function create () : View
    {
        return view('auth.password-reset');
    }

    /**
     * Requests an email to reset the users password
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store (Request $request) : RedirectResponse
    {
        //
        // Validates the response
        request()->validate([
            'token'     => [ 'required', 'string' ],
            'password'  => [ 'required', new Password ]
        ], [
            'password.required' => trans('auth.reset-password.validation.password')
        ]);

        //
        // Retrieve the password reset entry for the given token
        $passwordReset = UserPasswordReset::firstWhere('token', $request->input('token'));

        //
        // Set the new password
        User::where('email', $passwordReset->email)->update([
            'password' => Hash::make($request->password)
        ]);

        //
        // Remove the database entry for the password reset
        UserPasswordReset::where('email', $passwordReset->email)->delete();

        return redirect()->route('signin.create')
            ->with('message', trans('auth.reset-password.info'));
    }
}
