<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Hash;
use Illuminate\View\View;

class ConfirmPasswordController extends Controller
{
    /**
     * Show the password confirmationv iew
     *
     * @return \Illuminate\View\View
     */
    public function create () : View
    {
        return view('auth.password-confirm');
    }

    /**
     * Confirm the entered password for the user
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store () : RedirectResponse
    {
        //
        // Check if the entered password was correct
        if (! Hash::check(request()->password, auth()->user()->password)) {
            return back()->withErrors([
                'password' => trans('auth.confirm-password.validation')
            ]);
        }

        request()->session()->passwordConfirmed();

        return redirect()->intended();
    }
}
