<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Http\Requests\Register;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class SignUpController extends Controller
{
    /**
     * Show the sign up view
     *
     * @return \Illuminate\View\View
     */
    public function create () : View
    {
        return view('auth.signup');
    }

    /**
     * Creates a new user
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store (Register $request) : RedirectResponse
    {
        //
        // Validates the response
        $validated = (object) $request->validated();

        //
        // Create a new user
        $user = User::create([
            'name' => $validated->name,
            'email' => $validated->email,
            'password' => Hash::make($validated->password),
            'created_by' => 1,
            'updated_by' => 1
        ]);

        //
        // Update the created_by and updated_by fields accordingly
        $user->update([
            'created_by' => $user->id,
            'updated_by' => $user->id,
        ], [ 'timestamps' => false ]);

        logger()->info('User with id '.$user->id.' created ('.$user->email.')');
        logger()->info('Verification url valid until: '.now()->addMinutes(30).' => '.url()->temporarySignedRoute('verify.create', now()->addMinutes(30), [
            'user'  => $user,
            'email' => $user->email
        ]));

        //
        // Signing in the user and regenerate its session
        auth()->login($user);
        $request->session()->regenerate();

        //
        // Redirect to the applications dashboard
        return redirect()->route('dashboard');
    }
}
