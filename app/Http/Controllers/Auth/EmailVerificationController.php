<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Exceptions\InvalidSignatureException;
use Symfony\Component\HttpKernel\Exception\HttpException;

class EmailVerificationController extends Controller
{
    /**
     * Attempts to verify a users email address
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function create (User $user) : RedirectResponse
    {
        $verifiedEmail = request('email');
        if (! isset($verifiedEmail)) {
            throw new InvalidSignatureException;
        }

        //
        // If the verified email address is the one thats stored
        // the user has verified his initial email address
        if (! $user->isVerified() && $verifiedEmail === $user->email) {
            $user->email_verified_at = now();
        }

        //
        // If the user has a pending email address and its the verified one
        // he has verified the pending email
        $pendingEmail = $user->getPendingEmailChange();
        if (isset($pendingEmail) && $pendingEmail === $verifiedEmail) {
            $user->setPendingEmailChange(null);

            //
            // Check if the email is still free to use
            if (! User::where('email', $verifiedEmail)->exists()) {
                $user->email = $verifiedEmail;
                $user->email_verified_at = now();
            } else {
                throw new HttpException(403, 'This email address is already verified.');
            }
        }

        $user->save();
        return redirect()->route('dashboard');
    }
}
