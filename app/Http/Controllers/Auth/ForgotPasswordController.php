<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Models\User;
use App\Http\Requests\ForgotPassword;
use App\Models\UserPasswordReset;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class ForgotPasswordController extends Controller
{
    /**
     * Show the forgot password view
     *
     * @return \Illuminate\View\View
     */
    public function create () : View
    {
        return view('auth.password-forgot');
    }

    /**
     * Requests an email to reset the users password
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store (ForgotPassword $request) : RedirectResponse
    {
        //
        // Generate a unique token
        $token = UserPasswordReset::uniqueToken();
        $user = User::firstWhere('email', $request->email);

        //
        // Insert or update the password resets entry
        UserPasswordReset::upsert([
            'email'         => $user->email,
            'token'         => $token,
            'expires_at'    => now()->addMinutes(config('auth.passwords.users.expire'))
        ], ['email']);

        logger()->info('Password reset requested for user '.$user->id.' ('.$user->email.')');
        logger()->info(route('password.reset.create', [ 'token' => $token ]));

        return redirect()->route('signin.create')
            ->with('message', trans('auth.forgot-password.info', [ 'email' => $user->email ]));
    }
}
