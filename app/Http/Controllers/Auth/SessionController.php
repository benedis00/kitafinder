<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Http\Requests\Authenticate;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class SessionController extends Controller
{
    /**
     * Show the sign in view
     *
     * @return \Illuminate\View\View
     */
    public function create () : View
    {
        return view('auth.signin');
    }

    /**
     * Signs the user in
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store (Authenticate $request) : RedirectResponse
    {
        //
        // Error message for the response in case of a failed authentication attempt
        $failedAttempt = [
            'email' => trans('auth.signin.validation.failed')
        ];

        //
        // Validate the attempting user credentials
        $attemptingUser = User::firstWhere('email', $request->email);
        if (! $attemptingUser || ! Hash::check($request->password, $attemptingUser->password)) {
            return back()->withErrors($failedAttempt);
        }

        //
        // If the attempting user has two factor authentication enabled
        // we redirect the user to the challenge before authenticating
        if ($attemptingUser->twoFactorEnabled()) {
            $request->session()->put([
                'attempt.id' => $attemptingUser->getKey(),
                'attempt.remember' => $request->filled('remember')
            ]);

            return redirect()->route('two-factor-authentication.create');
        }

        //
        // Trying to authenticate the user with the given credentials
        // when two factor authentication is disabled
        $authenticationAttempt = auth()->attempt(
            $request->only('email', 'password'),
            $request->filled('remember')
        );

        //
        // Regenerate the session and redirect the user to its intended destination
        if ($authenticationAttempt) {
            $request->session()->regenerate();

            return redirect()->intended(RouteServiceProvider::HOME);
        }

        //
        // Redirect back when the authentication attempt failed
        return back()->withErrors($failedAttempt);
    }

    /**
     * Signs out the currently authenticated user
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy () : RedirectResponse
    {
        //
        // Signing out the authenticated user
        auth()->logout();

        //
        // Invalidate session data
        request()->session()->invalidate();
        request()->session()->regenerateToken();

        //
        // Redirect to the applications root
        return redirect()->route('signin.create');
    }
}
