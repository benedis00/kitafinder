<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Challenge;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class TwoFactorAuthenticationController extends Controller
{
    /**
     * Show the two factor authentication challenge view.
     *
     * @return \Illuminate\View\View|\Illuminate\Http\RedirectResponse
     */
    public function create (Challenge $request) : View|RedirectResponse
    {
        //
        // Check if the current session has a user to challenge
        // If not redirect the user back to the login screen
        if (! $request->hasChallengedUser()) {
            return redirect()->route('signin.create');
        }

        return view('auth.challenge');
    }

    /**
     * Attempt to authenticate a new session using the two factor authentication code.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function challenge (Challenge $request) : RedirectResponse
    {
        //
        // Retrieve the challenged user
        $user = $request->challengedUser();

        //
        // First check if the user tries to authenticate using a recovery code
        // If so, the code must be valid and will be replaced afterwards
        if ($code = $request->validRecoveryCode()) {
            $user->replaceRecoveryCode($code);
        } elseif (! $request->hasValidCode()) {

            //
            // If there was no recovery code set and the given
            // two factor authentication code was invalid, abort
            return back()->withErrors([
                'code' => trans('auth.challenge.validation')
            ])->with('message-danger', trans('auth.challenge.alert'));
        }

        //
        // Sign in the challenged user, regenerate the session
        auth()->login($user, $request->remember());
        $request->session()->regenerate();

        return redirect()->intended(RouteServiceProvider::HOME);
    }
}
