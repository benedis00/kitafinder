<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;

class SetUserLocale {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function handle ($request, Closure $next) : Response|RedirectResponse|JsonResponse {
        if (auth()->guest()) {
            return $next($request);
        }

        $locale = auth()->user()->preferredLocale();
        $this->applyLocale($locale);

        return $next($request);
    }

    /**
     * Applies the locale for the application
     *
     * @param string $locale
     * @return void
     */
    public function applyLocale (string $locale) : void {
        app()->setLocale($locale);
        setlocale(LC_TIME, $locale);
        setlocale(LC_MONETARY, $locale);
    }
}
