<?php

namespace App\Http\Middleware;

use App\Exceptions\TokenExpiredException;
use Closure;
use Illuminate\Http\Request;
use App\Models\UserPasswordReset;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;

class VerifyPasswordResetToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle (Request $request, Closure $next) : Response|RedirectResponse
    {
        //
        // Check if a token is given
        $token = $request->input('token');
        if (! isset($token)) {
            abort(404);
        }

        //
        // Check if there is a password reset entry for the given token
        $passwordReset = UserPasswordReset::firstWhere('token', $token);
        if (! isset($passwordReset)) {
            abort(404);
        }

        //
        // Check if the reset token is expired
        if (now()->greaterThan($passwordReset->expires_at)) {
            UserPasswordReset::where('token', $token)->delete();
            throw new TokenExpiredException;
        }

        //
        // Token is given, valid and not expired
        return $next($request);
    }
}
