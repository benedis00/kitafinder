<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() : void
    {
        //
        // Create users table
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->text('two_factor_secret')->nullable();
            $table->text('two_factor_recovery_codes')->nullable();
            $table->rememberToken();

            $table->foreignId('created_by')
                ->constrained('users')
                ->restrictOnDelete()
                ->restrictOnUpdate();
            $table->foreignId('updated_by')
                ->constrained('users')
                ->restrictOnDelete()
                ->restrictOnUpdate();
            $table->timestamps();
            $table->softDeletes();
        });

        //
        // Create email cahnges table
        Schema::create('users_email_changes', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->unique()
                ->constrained('users')
                ->restrictOnDelete()
                ->restrictOnUpdate();
            $table->string('email');
        });

        //
        // Create password resets table
        Schema::create('users_password_resets', function (Blueprint $table) {
            $table->id();
            $table->string('email')->unique();
            $table->string('token')->unique();
            $table->timestamp('expires_at');
        });

        //
        // Create the sessions table with the link to the users
        Schema::create('sessions', function (Blueprint $table) {
            $table->string('id')->primary();

            $table->foreignId('user_id')->nullable()
                ->constrained('users')
                ->restrictOnDelete()
                ->restrictOnUpdate();

            $table->string('ip_address', 45)->nullable();
            $table->text('user_agent')->nullable();
            $table->text('payload');
            $table->integer('last_activity')->index();
        });
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function down() : void
    {
        Schema::drop('sessions');
        Schema::drop('users_password_resets');
        Schema::drop('users_email_changes');
        Schema::drop('users');
    }
};
