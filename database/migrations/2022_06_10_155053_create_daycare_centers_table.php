<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('daycare_centers', function (Blueprint $table) {
            $table->id();
            $table->foreignId('supervisory_district_id')
                ->constrained('supervisory_districts')
                ->restrictOnDelete()
                ->restrictOnUpdate();
            $table->integer('facility_id');
            $table->string('name');
            $table->string('address_street');
            $table->integer('address_house_number');
            $table->string('address_house_number_addition')->nullable();
            $table->string('address_postal_code');
            $table->string('address_place');
            $table->string('mobile')->nullable();
            $table->integer('number_of_places')->nullable();
            $table->string('facility_type');
            $table->foreignId('provider_id')
                ->constrained('providers')
                ->restrictOnDelete()
                ->restrictOnUpdate();
            $table->double('longitude')->nullable();
            $table->double('latitude')->nullable();
            $table->text('opening_hours')->nullable();
            $table->string('website')->nullable();
            $table->string('fax')->nullable();
            $table->string('email')->nullable();
            $table->string('educational_focus')->nullable();
            $table->text('educational_features')->nullable();

            $table->foreignId('created_by')
                ->constrained('users')
                ->onDelete('restrict')
                ->onUpdate('restrict');
            $table->foreignId('updated_by')
                ->constrained('users')
                ->onDelete('restrict')
                ->onUpdate('restrict');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('daycare_centers');
    }
};
