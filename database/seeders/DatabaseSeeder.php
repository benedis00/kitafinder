<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run () : void {
        \App\Models\User::factory()->create([
            'email' => 'admin@example.com',
            'password' => bcrypt('admin')
        ]);

        \App\Models\User::factory(10)->create();
    }
}
